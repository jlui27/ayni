import React, { Component } from 'react'
import './Page404.scss'

class Page404 extends Component{
    render(){
        return(
            <div className='page404'>
                Page not found <br/>
                404
            </div>
        )
    }
}

export default Page404;