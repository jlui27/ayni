import React from 'react'
import './OurUniverse.scss'
import { api } from '../../core/helpers/api';
import NavMain from '../../components/menu/nav-main/NavMain';
import NavSmall from '../../components/menu/nav-small/NavSmall';
import $ from 'jquery'

class OurUniverse extends React.Component{
    constructor(){
        super()
        this.our_descrip_primary = React.createRef();
        this.our_descrip_secondary = React.createRef()
        this.state = {
            portrait: [],
            content: [],
        };
    }

    getPortrait(){
        api.get('/portraits?code=OUR_UNIVERSE', [], (res) => {
            //console.log(res.data)
            if(res.data){
                this.setState({ portrait: res.data.data })
            } else {
                console.log(res)
            }
        })
    }
    getContent(){
        api.get('/footer?code=OUR_UNIVERSE', [], (res) => {
            if(res.data){
                let ouniverse = res.data.data.content.indexOf("<p><strong>FOUNDERS</strong></p>")
                this.setState({ 
                    content: res.data.data,
                })              
                this.our_descrip_primary.current.innerHTML=res.data.data.content.substring(0,ouniverse);
                this.our_descrip_secondary.current.innerHTML=res.data.data.content.substring(ouniverse);

            } else {
                console.log(res)
            }
        })
    }
    componentDidMount(){
        this.getPortrait()
        this.getContent()
    }
    render(){
        const { portrait, content } = this.state
        //const testCover = 'https://www.incimages.com/uploaded_files/image/970x450/getty_509107562_2000133320009280346_351827.jpg'
        const options = [
            { text: 'Home', route: '/' },
            { text: 'Our Universe', route: '/our-universe' }
        ]
        var oUniverseElement = $('p').find('strong:contains("OUR UNIVERSE")').text()
        var foundersElement = $('p strong:contains("FOUNDERS")').text()
        
        return(
            <div className='our-universe'>
                <div className='our-universe_nav-small'><NavSmall options={options} /></div>
                <div className='our-universe_title'>{content.name}</div>
                <div className='our-universe_portrait' style={{backgroundImage: `url(${portrait.path})`}} />  {/* Original: portrait.path */}
                {/* <img src={portrait.path} alt=""/> */}
                {/* <div className='our-universe_body' id='our_content'></div> */}
                <div className='our-universe_body' ref = { this.our_descrip_primary } id="nav_1"></div>
                <div className='our-universe_body' ref = { this.our_descrip_secondary } id="nav_2"></div>
                <div className='our-universe_navbar'></div>
            </div>
        )
    }
}

export default OurUniverse