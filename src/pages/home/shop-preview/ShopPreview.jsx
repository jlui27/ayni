import React from 'react'
import './ShopPreview.scss'
import { api } from '../../../core/helpers/api';
import ProdPreview from '../../../components/prod-preview/ProdPreview';

class ShopPreview extends React.Component{
    constructor(){
        super()
        this.state={
            previiewData: []
        }
        this.getProductPrev=this.getProductPrev.bind(this)
    }
    getProductPrev(){
        api.get('/home/slider', [], (res) => {
            if(res.data){
                const data = res.data.data
                    if(data)
                        this.setState({ previiewData: data })
            }
        })
    }
    componentDidMount(){
        this.getProductPrev()
    }
    render(){
       const { previiewData } = this.state
        return(
            <div className='shop-preview'>
                <div className='shop-preview_container'>
                    <ProdPreview products={previiewData} width='275px' limit={4} />
                </div>
               {/* <span> ↑↑ PENDIENTE - EN DESARROLLO ↑↑</span> */}
            </div>
        )
    }
}

export default ShopPreview;