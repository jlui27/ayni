import React, { Component } from 'react'
import './Home.scss'
import HomeCoverPage from './cover-page/HomeCoverPage';
import NavMain from '../../components/menu/nav-main/NavMain';
import NavTop from '../../components/menu/nav-top/NavTop';
import ShopPreview from './shop-preview/ShopPreview';

class Home extends Component{
    render(){
        return(
            <div className='home'>
                <HomeCoverPage/>
                <NavMain/>
                <ShopPreview/>
            </div>
        )
    }
}

export default Home;