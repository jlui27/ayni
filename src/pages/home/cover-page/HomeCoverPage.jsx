import React, { Component } from 'react'
import './HomeCoverPage.scss'
import { api } from '../../../core/helpers/api';
import { withRouter } from 'react-router-dom';

class HomeCoverPage extends Component{
    constructor(){
        super()
        this.state={
            collPortrait: [],
            shopPortrait: [],
            universePortrait: [],
            lastCollection: 0
        }
        this.getCoverData=this.getCoverData.bind(this)
    }
    getCoverData(code, state){
        api.get('/portraits?code=', code, (res) => {
            if(res.data){
                const data = res.data.data
                if(data)
                    this.setState({ ...this.state, [state]: data })
            }else{
                console.log(`Error at getting ${code}`, res)
            }   
        })
    }
    getCollectionList(){
        api.get('/collections', [], (res) => {
            if(res){
                if(res.data){
                    const collections = res.data.data
                    /*
                    var collectionsIDList = []
                    for (let i = 0; i < collections.length; i++) {
                        collectionsIDList.push(parseInt(collections[i].id))
                    }
                    let lastCollection = Math.max.apply(null, collectionsIDList)
                    this.setState({ lastCollection: lastCollection })
                    */
                   this.setState({ lastCollection: collections[0].id })
                }
            }
        })
    }
    componentDidMount(){
        var codes = ['HOME_PORTRAIT_COLLECTION_IMAGE', 'HOME_PORTRAIT_SHOP','HOME_PORTRAIT_OUR_UNIVERSE']
        var states = ['collPortrait', 'shopPortrait', 'universePortrait']
        var i = 0
        while(i < codes.length){
            this.getCoverData(codes[i], states[i])
            i++
        }
        this.getCollectionList()
    }
    go(route){
        this.props.history.push(route)
    }
    render(){
        const { collPortrait, shopPortrait, universePortrait, lastCollection } = this.state
        /*
        const content = {
            cov1: 'https://i.imgur.com/DNWECCN.jpg',
            cov2: 'https://i.imgur.com/GUWlN9Y.jpg',
            cov3: 'https://i.imgur.com/CLRWjTx.jpg'
        }
        */
        return(
            <div className='home-cover-page'>
                <div className='home-cover-page_title'>TODAY FOR YOU</div>
                <div className='home-cover-page_title'>TOMORROW</div>
                <div className='home-cover-page_title'>FOR ME</div>
                <div className='home-cover-page_content'>
                    <div className='home-cover-page_content_cov1' style={{backgroundImage: `url(${collPortrait.path})`}} onClick={()=>this.go(`/collection/${lastCollection}`)}></div>
                    <div className='home-cover-page_content_cov2' style={{backgroundImage: `url(${shopPortrait.path})`}} onClick={()=>this.go('/shop')}></div>
                    <div className='home-cover-page_content_cov3' style={{backgroundImage: `url(${universePortrait.path})`}} onClick={()=>this.go('/our-universe')}></div>
                </div>
            </div>
        )
    }
}

export default withRouter(HomeCoverPage)