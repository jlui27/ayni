import React, { Component } from 'react'
import './PublicPayment.scss'
import { connect } from 'react-redux';
import { changeCheckoutStep } from '../../../../../core/store/actions/checkout';
import { api } from '../../../../../core/helpers/api';
import $ from 'jquery'

class PublicPayment extends Component{
    constructor(){
        super()
        this.state = { cardData: null }
    }
    getCard(){
        api.get('/payments/customers/me/card', '', (res) => {
            if(res.data)
                if(res.data.status === 'success')
                    this.setState({ cardData: res.data.data })
        })
    }
    componentDidMount(){
        this.getCard()
        $('html, body').animate({ scrollTop: 140 }, 'slow' );
    }
    render(){
        const { onChangeCheckoutStep } = this.props
        const { cardData } = this.state
        const cardNumber = cardData?cardData.cardNumber?cardData.cardNumber.replace(/X/g,'*'):'???':'???'
        const expirationDate = cardData?cardData.expirationDate?cardData.expirationDate:'???':'???'
        return(
            <div className='public-payment'>
                <div className='public-payment_container'>
                    <div className='public-payment_container_title'>Credit or debit card</div>
                    <div className='public-payment_container_card-number'><i className='material-icons'>credit_card</i> VISA {cardNumber}</div>
                    <div className='public-payment_container_card-expires'>Expiration: {expirationDate}</div>
                    <button onClick={()=>window.location.href = '/profile/my-card'}>CHANGE</button>
                </div>
                <div className='public-payment_buttons'>
                    <button className='public-payment_buttons_go-my-purchases' onClick={()=>onChangeCheckoutStep(0)}><i className='material-icons'>keyboard_backspace</i> BACK</button>
                    <button className='public-payment_buttons_next' onClick={()=>onChangeCheckoutStep(2)}>NEXT</button>
                </div>
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return{
        onChangeCheckoutStep(step){
            dispatch(changeCheckoutStep(step))
        }
    }
}

export default connect(null, mapDispatchToProps)(PublicPayment)