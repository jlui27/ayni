import React, { Component } from 'react'
import './UserPayment.scss'

class UserPayment extends Component{
    constructor(props){
        super(props)
        this.state = {
            yearList: [],
            monthList: ['January','February','March','April','May','June','July','August','September','October','November','December'],
            saveCard: false
        }
        this.onSaveCard=this.onSaveCard.bind(this)
    }
    setYearList(){
        const  { yearList } = this.state
        let list = yearList
        const date = new Date()
        const currentYear = date.getFullYear()
        for (let i = currentYear; i < currentYear + 20; i++) {
            list.push(i)
        }
        this.setState({ yearList: list })
    }
    onSaveCard(){
        this.setState({ saveCard: !this.state.saveCard })
    }
    componentDidMount(){
        this.setYearList()
    }
    render(){
        const { monthList, yearList, saveCard } = this.state
        return(
            <div className='user-payment_container'>
                <div className='user-payment_container_body'>
                    <div className='user-payment_container_body_card-number'>
                        <label htmlFor="">Credit or debit card</label>
                        <div className='user-payment_container_body_card-number_input'>
                            <input type="text" placeholder='1111 2222 3333 4444' />
                            <i className='material-icons'>lock_outlined</i>
                            {/* <div className='user-payment_container_body_card-number_input_icon'></div> */}
                        </div>
                    </div>
                    <div className='user-payment_container_body_card-ccv'>
                        <label htmlFor="">Security code</label>
                        <div className='user-payment_container_body_card-ccv_input'>
                            <input type="text" placeholder='ccv' maxLength={3}/>
                            <i className='material-icons'>help_outline</i>
                            {/* <div className='user-payment_container_body_card-ccv_input_icon'></div> */}
                        </div>
                    </div>
                    <div className='user-payment_container_body_card-expiration'>
                        <label htmlFor="">Expiration date</label>
                        <div className='user-payment_container_body_card-expiration_selects'>
                            <select>
                                <option value="0" defaultChecked hidden>Month</option>
                                {monthList.length>0?monthList.map((month, i) => {
                                    return(
                                        <option key={i} value={month.toLowerCase()} >{month}</option>
                                    )
                                }):false}
                            </select>
                            <select style={{marginLeft: '34px'}}>
                                <option value="0" defaultChecked hidden>Year</option>
                                {yearList.length>0?yearList.map((year, i) => {
                                    return(
                                        <option key={i} value={year}>{year}</option>
                                    )
                                }):false}
                            </select>
                        </div>
                    </div>
                    <div className='user-payment_container_body_card-ccv-mobile'>
                        <label htmlFor="">Security code</label>
                        <div className='user-payment_container_body_card-ccv-mobile_input'>
                            <input type="text" placeholder='ccv' maxLength={3}/>
                            <i className='material-icons'>help_outline</i>
                            {/* <div className='user-payment_container_body_card-ccv_input_icon'></div> */}
                        </div>
                    </div>
                    <div className='user-payment_container_body_save-card'>
                        <label htmlFor="">&nbsp;</label>
                        <div className='user-payment_container_body_save-card_container' onClick={this.onSaveCard} >
                            <i className='material-icons'>{saveCard?'toggle_on':'toggle_off'}</i>
                            <span>Save card data</span>
                        </div>
                    </div>
                </div>
                <span>There will be a charge for the first order, then the refund will be made S / 5.00</span>
                <div className='user-payment_container_buttons'>
                    <button className='user-payment_container_buttons_go-my-purchases'><i className='material-icons'>keyboard_backspace</i> BACK</button>
                    <button className='user-payment_container_buttons_next'>NEXT</button>
                </div>
            </div>
        )
    }
}

export default UserPayment