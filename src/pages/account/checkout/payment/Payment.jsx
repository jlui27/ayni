import React, { Component } from 'react'
import './Payment.scss'
import UserPayment from './user/UserPayment';
import PublicPayment from './public/PublicPayment';
import { opSelMobile, opNoSelMobile, opSelDesktop, opNoSelDesktop } from '../section';

class CheckoutPayment extends  Component{
    constructor(props){
        super(props)
        this.state = {
            optionSelected: 0, optionSelectedStyle: null, optionNoSelectedStyle: null
        }
        this.mediaQExecute=this.mediaQExecute.bind(this)
        this.changeOption=this.changeOption.bind(this)
        this.options = ['Credit or debit card', /*'Payment upon delivery: Cash'*/]
    }
    mediaQExecute(x) {
        if (x.matches) { // If media query matches
            this.setState({ optionSelectedStyle: opSelMobile, optionNoSelectedStyle: opNoSelMobile })
        } else {
          this.setState({ optionSelectedStyle: opSelDesktop, optionNoSelectedStyle: opNoSelDesktop })
        }
    }
    mediaQChecker(){
        var x = window.matchMedia("(max-width: 800px)")
        this.mediaQExecute(x)
        x.addListener(this.mediaQExecute) 
    }
    changeOption(i){
        const { optionSelected } = this.state
        if(optionSelected !== i)
            this.setState({ optionSelected: i })
    }
    componentDidMount(){
        this.mediaQChecker()
    }
    render(){
        const { optionSelected, optionNoSelectedStyle, optionSelectedStyle } = this.state
        return(
            <div className='checkout-payment'>
                <div className='checkout-payment_container'>
                    <div className='checkout-payment_container_title'>
                        <div className='checkout-payment_container_title_icon'><i className='material-icons'>credit_card</i></div>
                        <div className='checkout-payment_container_title_text'>2. PAYMENT METHOD</div>
                    </div>
                    <div className='checkout-payment_container_options'>
                        {this.options.map((text, i) => {
                            return(
                                <div className='checkout-payment_container_options_to' key={i} 
                                    style={optionSelected===i?optionSelectedStyle:optionNoSelectedStyle} onClick={()=>this.changeOption(i)}>
                                    <div className='checkout-payment_container_options_to_option'>{text}</div>
                                    <div className='checkout-payment_container_options_to_icon' style={optionSelected!==i?{display:'none'}:null}>▼</div>
                                </div>
                            )
                        })}
                    </div>
                    <img className='checkout-payment_container_card-icons' src="/images/fixed/credit-cards.png" alt=""/>
                    <div className='checkout-payment_container_warning'>We accept credit cards (Visa, MasterCard and American Express) and Debit (Visa).</div>
                    {/*  */}
                    {localStorage.getItem(process.env.REACT_APP_LOCAL_TOKEN)?<PublicPayment />:<UserPayment /> 
                    /* Las vistas estan invertidas por error, public como User y User como public ↑↑↑ */}
                    {/*  */}
                </div>
            </div>
        )
    }
}

export default CheckoutPayment