import React, { Component } from 'react'
import './Confirmation.scss'
import { toMoney } from '../../../../core/helpers/formats';
import CheckoutSuccess from '../success/CheckoutSuccess';
import { connect } from 'react-redux';
import { api } from '../../../../core/helpers/api';
import $ from 'jquery'
import { updateCheckoutSuccess } from '../../../../core/store/actions/checkout';
import { updateCartCounter, setSessionCart } from '../../../../core/store/actions/cart';
import { alertError } from '../../../../core/constants/alert';

class CheckoutConfirmation extends  Component{
    constructor(props){
        super(props)
        this.state = {
            cartData: [], finalPrices: {subtotal: 0, deliveryPrice: 0, total: 0},
            userData: null, cardData: null
        }

        this.createTransaction=this.createTransaction.bind(this)
    }
    getCart(){
        var uid = localStorage.getItem(process.env.REACT_APP_LOCAL_UID)
        var localCart = localStorage.getItem(process.env.REACT_APP_LOCAL_CART)
        var cart = uid ? this.props.sesCartData : localCart ? JSON.parse(localCart) : []
        var subtotal = 0
        var delivery = 0
        var total = 0
        for (let i = 0; i < cart.length; i++) {
            const iSubTotal = cart[i].total;
            subtotal = subtotal + iSubTotal
            total = subtotal + delivery
        }
        this.setState({  cartData: cart, finalPrices: { subtotal: subtotal, deliveryPrice: delivery, total: total } })
    }
    getUserData(){
        api.get('/users/me', '', (res) => {
            console.log(res)
            if(res.data)
                if(res.data.status === 'success')
                    this.setState({ userData: res.data.data })
                else
                    alertError('Error: User data not found, please try again')
        })
    }
    getCard(){
        api.get('/payments/customers/me/card', '', (res) => {
            if(res.data)
                if(res.data.status === 'success')
                    this.setState({ cardData: res.data.data })
                else
                    alertError('Credit card data not found, please try again')
        })
    }
    createTransaction(){
        const { sesCartData, setCheckoutSuccess, updateCartCounter, updateSessionCart } = this.props
        const uid = localStorage.getItem(process.env.REACT_APP_LOCAL_UID)
        if(sesCartData.length>0){
            var amount = 0
            var cart = []
            var i=0
            while(i<sesCartData.length){
                const { id, name, description, quantity, price, total } = sesCartData[i]
                cart.push({id, name, description, quantity, unitPrice: price})
                amount = amount + total
                i++
            }
            var params = { amount, cart }
        }
        api.post('/payments/transactions', params, (res) => {
            //console.log('transaction res', res)
            if(res.data){
                if(res.data.status === 'success'){
                    api.post(`/users/${uid}/shoppingCart`, { shoppingCartJson: [] }, ()=>{})
                    setCheckoutSuccess(true)
                    updateCartCounter(0)
                    updateSessionCart([])
                } else {
                    alertError('ERROR: Has ocurred an error, please try again')
                }
            } else {
                alertError('ERROR: Credit card error')
            }
        })
    }
   
    componentDidMount(){
        this.getCart()
        this.getUserData()
        this.getCard()
        $('html, body').animate({ scrollTop: 140 }, 'slow' );
    }
    componentWillUnmount(){
        this.props.setCheckoutSuccess(false)
    }
    render(){
        const { checkSuccess } = this.props
        const { cartData, finalPrices, userData, cardData } = this.state
        const firstName = userData?userData.firstName?userData.firstName:null:null
        const lastName = userData?userData.lastName?userData.lastName:null:null
        const address = userData?userData.address?userData.address:null:null
        const phone = userData?userData.phone?userData.phone:null:null
        const email = userData?userData.email?userData.email:null:null

        const cardNumber = cardData?cardData.cardNumber?cardData.cardNumber.replace(/X/g,'*'):null:null
        const expirationDate = cardData?cardData.expirationDate?cardData.expirationDate:null:null
        //console.log('Current cart: ', this.state)
        if(checkSuccess === false){
            return(
                <div className='checkout-confirmation'>
                    <div className='checkout-confirmation_container'>
                        <div className='checkout-delivery_container_title'>
                            <div className='checkout-delivery_container_title_icon'><i className='material-icons'>check_circle_outline</i></div>
                            <div className='checkout-delivery_container_title_text'>3. CONFIRM PURCHASE</div>
                        </div>
                        {/* Continua aqui */}
                        <div className='checkout-confirmation_container_body'>
                            <div className='checkout-confirmation_container_body_delivery'>
                                <div className='checkout-confirmation_container_body_delivery_info'>
                                    <div className='checkout-confirmation_container_body_delivery_info_title'>DELIVERY</div>
                                    <div className='checkout-confirmation_container_body_delivery_info_name'>
                                        <div className='checkout-confirmation_container_body_delivery_info_name_icon'></div>
                                        <div className='checkout-confirmation_container_body_delivery_info_name_text'>{firstName} {lastName}</div>
                                    </div>
                                    <div className='checkout-confirmation_container_body_delivery_info_address'>
                                        <div className='checkout-confirmation_container_body_delivery_info_address_icon'></div>
                                        <div className='checkout-confirmation_container_body_delivery_info_address_text'>
                                            {address==='DATANULL'?'-':address} <br/> 
                                            {phone==='DATANULL'?'-':phone} <br/> 
                                            {email==='DATANULL'?'-':email}
                                            {/* San Isidro <br/> Avenida Javier Prado 7721 <br/> Dpto 201 
                                            <br/> Tel:  0344 332 5931 <br/> carmenceccarelli@gmail.com */}
                                        </div>
                                    </div>
                                </div>
                                <div className='checkout-confirmation_container_body_delivery_map'>
                                    <div className='user-delivery_body_map_container'></div>
                                </div>
                            </div>
                            {/* Divission */}
                            <div className='checkout-confirmation_container_body_payment-method'>
                                <div className='checkout-confirmation_container_body_payment-method_title'>PAYMENT METHOD</div>
                                <div className='checkout-confirmation_container_body_payment-method_content'>
                                    <i className='material-icons'>credit_card</i>
                                    <div className='checkout-confirmation_container_body_payment-method_content_info'>
                                        <div className='checkout-confirmation_container_body_payment-method_content_info_card'>VISA {cardNumber}</div>
                                        <div className='checkout-confirmation_container_body_payment-method_content_info_expiration'>Expiration: {expirationDate}</div>
                                    </div>
                                </div>
                            </div>
                            {/* Divission */}
                            <div className='checkout-confirmation_container_body_product-list'>
                                <div className='checkout-confirmation_container_body_product-list_title'>PURCHASES ({cartData.length})</div>
                                <div className='checkout-confirmation_container_body_product-list_container'>
                                    {cartData.length>0?cartData.map((data, i) => {
                                        return(
                                            <div className='checkout-confirmation_container_body_product-list_container_content' key={i}>
                                                <div className='checkout-confirmation_container_body_product-list_container_content_image' style={{backgroundImage: `url(${data.image})`}}></div>
                                                <div className='checkout-confirmation_container_body_product-list_container_content_info'>
                                                    <div className='checkout-confirmation_container_body_product-list_container_content_info_name'>{data.name}</div>
                                                    <div className='checkout-confirmation_container_body_product-list_container_content_info_color-size-unds'>{data.color} - {data.size} - {data.quantity} Uds</div>
                                                </div>
                                                <div className='checkout-confirmation_container_body_product-list_container_content_price'>$ {toMoney(data.total)}</div>
                                            </div>
                                        )
                                    }):false}
                                </div>
                                <div className='checkout-confirmation_container_body_product-list_subtotal'>
                                    <label>Subtotal (2):</label>
                                    <span>$ {toMoney(finalPrices.subtotal)}</span>
                                </div>
                                {/* <div className='checkout-confirmation_container_body_product-list_delivery-price'>
                                    <label>Envío:</label>
                                    <span>$ {toMoney(finalPrices.deliveryPrice)}</span>
                                </div> */}
                                <div className='checkout-confirmation_container_body_product-list_total'>
                                    <label>Total:</label>
                                    <span>$ {toMoney(finalPrices.total)}</span>
                                </div>
                            </div>
                        </div>
                        <div className='checkout-confirmation_container_buttons'>
                            <button className='checkout-confirmation_container_buttons_go-my-purchases' onClick={()=>window.location.href='/profile/order-records'}><i className='material-icons'>keyboard_backspace</i> Back to My purchases</button>
                            <button className='checkout-confirmation_container_buttons_next' onClick={this.createTransaction}>CONFIRM PURCHASE</button>
                        </div>
                    </div>
                </div>
            )
        } else {
            return(
                <div className='checkout-confirmation'><CheckoutSuccess/></div>
            )
        }
    }
}

const mapStateToProps = state => {
    return{
        sesCartData: state.cart.data,
        checkSuccess: state.checkout.success
    }
}
const mapDispatchToProps = dispatch => {
    return {
        setCheckoutSuccess(bool){dispatch(updateCheckoutSuccess(bool))},
        updateCartCounter(count){dispatch(updateCartCounter(count))},
        updateSessionCart(data){dispatch(setSessionCart(data))},
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckoutConfirmation)