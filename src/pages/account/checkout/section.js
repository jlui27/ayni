// Step styles
export const selectedStepDesk = { backgroundColor: '#a3a3a2', color: 'white' }
export const selectedLinkerDesk = { backgroundColor: '#a3a3a2' }
export const selectedStepTextDesk = { color: '#a3a3a2' }
export const selectedStepMob = { backgroundColor: '#1C1C1A', color: 'white' }
export const selectedLinkerMob = { backgroundColor: '#1C1C1A' }
export const selectedStepTextMob = { color: '#1C1C1A' }

// Form options
export const opSelDesktop = { backgroundColor: '#1c1c1a', color: 'white', borderRadius: '5px' }
export const opNoSelDesktop = { border: '1px #a3a3a2 solid' }
export const opSelMobile = { backgroundColor: '#1c1c1a', color: 'white', borderRadius: '6px', border: '#393939 1px solid' }
export const opNoSelMobile = { border: '#1C1C1A 1px solid', borderRadius: '6px', color: '#1C1C1A' }