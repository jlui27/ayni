import React, { Component } from 'react'
import './Delivery.scss'
import PublicDelivery from './public/PublicDelivery';
import UserDelivery from './user/UserDelivery';
import { opSelMobile, opNoSelMobile, opSelDesktop, opNoSelDesktop } from '../section';

class CheckoutDelivery extends  Component{
    constructor(props){
        super(props)
        this.state = {
            optionSelected: 0, optionSelectedStyle: null, optionNoSelectedStyle: null
        }
        this.mediaQExecute = this.mediaQExecute.bind(this)
        this.changeOption=this.changeOption.bind(this)
        this.options = ['Delivery', 'Store pickup']
    }
    mediaQExecute(x) {
        if (x.matches) { // If media query matches
            this.setState({ optionSelectedStyle: opSelMobile, optionNoSelectedStyle: opNoSelMobile })
        } else {
          this.setState({ optionSelectedStyle: opSelDesktop, optionNoSelectedStyle: opNoSelDesktop })
        }
    }
    mediaQChecker(){
        var x = window.matchMedia("(max-width: 800px)")
        this.mediaQExecute(x)
        x.addListener(this.mediaQExecute) 
    }
    changeOption(i){
        const { optionSelected} = this.state
        if(optionSelected !== i)
            this.setState({ optionSelected: i })
    }
    componentDidMount(){
        this.mediaQChecker()
    }
    render(){
        const { optionSelected, optionSelectedStyle, optionNoSelectedStyle } = this.state
        return(
            <div className='checkout-delivery'>
                <div className='checkout-delivery_container'>
                    <div className='checkout-delivery_container_title'>
                        <div className='checkout-delivery_container_title_icon'><i className='material-icons'>location_on</i></div>
                        <div className='checkout-delivery_container_title_text'>1. DELIVERY</div>
                    </div>
                    <div className='checkout-delivery_container_options'>
                        {this.options.map((text, i) => {
                            return(
                                <div className='checkout-delivery_container_options_to' key={i} 
                                style={optionSelected===i?optionSelectedStyle:optionNoSelectedStyle} onClick={()=>this.changeOption(i)}>
                                    <div className='checkout-delivery_container_options_to_option'>{text}</div>
                                    <div className='checkout-delivery_container_options_to_icon' style={optionSelected!==i?{display:'none'}:null}>▼</div>
                                </div>
                            )
                        })}
                    </div>
                    {/*  */}´
                    {localStorage.getItem(process.env.REACT_APP_LOCAL_TOKEN)?<UserDelivery />:<PublicDelivery />}
                </div>
            </div>
        )
    }
}

export default CheckoutDelivery