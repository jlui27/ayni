import React, { Component } from 'react'
import './PublicDelivery.scss'

class PublicDelivery extends Component{
    constructor(props){
        super(props)

        this.groups = [
            { id: 'fullname', label: 'Your names*', placeholder: '' },
            { id: 'phone', label: 'Cell phone number*', placeholder: '' },
            { id: 'address', label: 'Address*', placeholder: 'Enter shipping address' },
            { id: 'inside', label: 'Interiors', placeholder: 'Example: Department 201' },
            { id: 'instructions', label: 'Instructions', placeholder: '' },
            { id: 'email', label: 'Mail', placeholder: '' }
        ]
    }
    render(){
        return(
            <div className='public-delivery'>
                <div className='public-delivery_delivery-time-mobile'>Monday deliveries - Friday 8am - 8pm and Saturdays 8am - 2pm<br/>We only ship within the U.S. territory</div>
                <div className='public-delivery_top-label'>Enter your shipping information:</div>
                <div className='public-delivery_form'>
                    {this.groups.map((text, i) => {
                        return(
                            <div className='public-delivery_form_group' key={i}>
                                <label htmlFor={text.id}>{text.label}</label>
                                <input type="text" id={text.id} placeholder={text.placeholder} />
                            </div>
                        )
                    })}
                </div>
                <div className='public-delivery_keep-data-client'>
                    <div></div>
                    <span>Save my information and consult quickly next time.</span>
                </div>
                <div className='public-delivery_delivery-time'>Monday deliveries - Friday 8am - 8pm and Saturdays 8am - 2pm<br/>We only ship within the U.S. territory</div>
                <div className='public-delivery_buttons'>
                    <button className='public-delivery_buttons_go-my-purchases'><b>←</b> BACK</button>
                    <button className='public-delivery_buttons_next'>NEXT</button>
                </div>
            </div>
        )
    }
}

export default PublicDelivery