import React, { Component } from 'react'
import './UserDelivery.scss'
import { connect } from 'react-redux';
import { changeCheckoutStep } from '../../../../../core/store/actions/checkout';
import { withRouter } from 'react-router-dom';
import { api } from '../../../../../core/helpers/api';
import $ from 'jquery'
import { alertConfirm } from '../../../../../core/constants/alert';

class UserDelivery extends Component{
    constructor(){
        super()
        this.state = {
            userData: null
        }
    }
    getUserData(){
        api.get('/users/me', '', (res) => {
            console.log(res)
            if(res.data)
                if(res.data.status === 'success')
                    this.setState({ userData: res.data.data })
        })
    }
    alertNoAddress(){
        alertConfirm('Please add your address to continue<br>Do you want to add now?', (rescall) => {
            if(rescall)
                window.location.href = '/profile/my-address'
        })
    }
    componentDidMount(){
        this.getUserData()
        $('html, body').animate({ scrollTop: 140 }, 'slow' );
    }
    render(){
        const { onChangeCheckoutStep } = this.props
        const { userData } = this.state
        const firstName = userData?userData.firstName?userData.firstName:null:null
        const lastName = userData?userData.lastName?userData.lastName:null:null
        const address = userData?userData.address?userData.address:null:null
        const phone = userData?userData.phone?userData.phone:null:null
        const email = userData?userData.email?userData.email:null:null
        const toVerifyAddress = userData?userData.address!=='DATANULL'?userData.address:null:null
        return(
            <div className='user-delivery'>
                <div className='user-delivery_delivery-time-mobile'>Monday deliveries - Friday 8am - 8pm and Saturdays 8am - 2pm<br/>We only ship within the U.S. territory</div>
                <div className='user-delivery_title'>My Address</div>
                <div className='user-delivery_body'>
                    <div className='user-delivery_body_info'>
                        <div className='user-delivery_body_info_name'>
                            <div className='user-delivery_body_info_name_icon'></div>
                            <div className='user-delivery_body_info_name_text'>
                                {firstName!=='undefined'?firstName:'-'} {lastName!=='undefined'?firstName:'-'}
                            </div>
                        </div>
                        <div className='user-delivery_body_info_address'>
                            <div className='user-delivery_body_info_address_icon'></div>
                            <div className='user-delivery_body_info_address_text'>
                                {address==='DATANULL'?'-':address?address.replace(/\|/g, '-'):'-'} <br/> 
                                {phone==='DATANULL'?'-':phone} <br/> 
                                {email==='DATANULL'?'-':email}
                                {/* San Isidro <br/> Avenida Javier Prado 7721 <br/> Dpto 201 
                                <br/> Tel:  0344 332 5931 <br/> carmenceccarelli@gmail.com */}
                            </div>
                        </div>
                        <button onClick={()=>window.location.href = '/profile/edit'} >CHANGE</button>
                    </div>
                    <div className='user-delivery_body_map'>
                        <div className='user-delivery_body_map_container'></div>
                    </div>
                </div>
                <div className='user-delivery_delivery-time'>Monday deliveries - Friday 8am - 8pm and Saturdays 8am - 2pm<br/>We only ship within the U.S. territory</div>
                <div className='user-delivery_buttons'>
                    <button className='user-delivery_buttons_go-my-purchases' onClick={()=>this.props.history.goBack()}><b>←</b> BACK</button>
                    <button className='user-delivery_buttons_next' onClick={()=>toVerifyAddress?onChangeCheckoutStep(1):this.alertNoAddress()}>NEXT</button>
                </div>
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return{
        onChangeCheckoutStep(step){
            dispatch(changeCheckoutStep(step))
        }
    }
}

export default withRouter(connect(null, mapDispatchToProps)(UserDelivery))