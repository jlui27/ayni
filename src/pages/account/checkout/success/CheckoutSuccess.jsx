import React, { Component } from 'react'
import './CheckoutSuccess.scss'
import { updateCheckoutSuccess } from '../../../../core/store/actions/checkout';
import { withRouter } from 'react-router-dom';
import $ from 'jquery'
import { connect } from 'react-redux';

class CheckoutSuccess extends Component{
    componentDidMount(){
        $('html, body').animate({ scrollTop: 140 }, 'slow' );
    }
    render(){
        return(
            <div className='checkout-success'>
                <div className='checkout-success_title'>¡Thank You!</div>
                <div className='checkout-success_text'>Your purchase was made successfully.</div>
                <button onClick={()=>this.props.history.push('/')}>GO TO HOME</button>
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setCheckoutSuccess(bool){dispatch(updateCheckoutSuccess(bool))},
    }
}

export default withRouter(connect(null, mapDispatchToProps)(CheckoutSuccess))