import React, { Component } from 'react'
import './Checkout.scss'
import CheckoutPayment from './payment/Payment';
import CheckoutDelivery from './delivery/Delivery';
import NavSmall from '../../../components/menu/nav-small/NavSmall';
import CheckoutConfirmation from './confirmation/Confirmation';
import { connect } from 'react-redux';
import { changeCheckoutStep } from '../../../core/store/actions/checkout';
import { selectedStepMob, selectedLinkerMob, selectedStepTextMob, selectedStepDesk, selectedLinkerDesk, selectedStepTextDesk } from './section';

class Checkout extends  Component{
    constructor(props){
        super(props)
        this.state = { selStep: null, selLinker: null, selStepText: null }
        this.mediaQExecute=this.mediaQExecute.bind(this)
        //this.steps = ['ENTREGA', 'PAGO', 'CONFIRMACIÓN']
        this.steps = [
            {name: 'DELIVERY', icon: 'location_on'}, {name: 'PAYMENT', icon: 'credit_card'}, {name: 'CONFIRMATION', icon: 'check_circle_outline'}
        ]
        this.smallMenu = [
            {text: 'My orders', route: '/profile/order-records' }, 
            {text: 'Checkout', route: '/checkout'}
        ]
    }
    mediaQExecute(x) {
        if (x.matches) { // If media query matches
            this.setState({ selStep: selectedStepMob, selLinker: selectedLinkerMob, selStepText: selectedStepTextMob })
        } else {
          this.setState({ selStep: selectedStepDesk, selLinker: selectedLinkerDesk, selStepText: selectedStepTextDesk })
        }
    }
    mediaQChecker(){
        var x = window.matchMedia("(max-width: 800px)")
        this.mediaQExecute(x)
        x.addListener(this.mediaQExecute) 
    }
    sectionSetter(){
        const { currentStep } = this.props
        switch(currentStep){
            case 0: return <CheckoutDelivery />; break;
            case 1: return <CheckoutPayment />; break;
            case 2: return <CheckoutConfirmation />; break;
            default: break;
        }
    }
    productsChecker(){
        const { cartCounter } = this.props
        if(!cartCounter)
            window.history.back()
    }
    sessionChecker(){
        const uid = localStorage.getItem(process.env.REACT_APP_LOCAL_UID)
        if(!uid)
            window.location.href = '/auth'
    }

    componentDidMount(){
        this.mediaQChecker()
        this.productsChecker()
        this.sessionChecker()
    }
    componentWillUnmount(){
        this.props.onChangeCheckoutStep(0)
    }
    render(){
        const { selStep, selLinker, selStepText } = this.state
        return(
            <div className='checkout'>
                <div className='checkout_header'>
                    <div className='checkout_header_sm-menu'>
                        <NavSmall options={this.smallMenu} />
                    </div>
                    <div className='checkout_header_steps'>
                        {this.steps.map((step, i) => {
                            const { currentStep, onChangeCheckoutStep } = this.props
                            const selectedStep = currentStep >= i ? selStep : null
                            const selectedLinker = currentStep >= i + 1 ? selLinker : null
                            const selectedStepText = currentStep >= i ? selStepText : null
                            return(
                                <div className={`checkout_header_steps_container${i}`} key={i} >  {/* onClick={i<currentStep?()=>onChangeCheckoutStep(i):false} */}
                                    <div className='checkout_header_steps_container_icon' style={selectedStep} onClick={/*()=>onChangeCheckoutStep(i)*/null}>
                                        <i className='material-icons'>{step.icon}</i>
                                    </div>
                                    <span style={selectedStepText}>{step.name}</span>
                                    {i < 2 ? <button className='checkout_header_steps_container_linker' style={selectedLinker}></button> : false }
                                </div>  
                            )
                        })}
                        
                    </div>
                </div>
                <div className='checkout_container'>
                    <div className='checkout_container_form'>
                        {/* <CheckoutDelivery /> */}
                        {this.sectionSetter()}
                        {/* <CheckoutConfirmation /> */}
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return{
        currentStep: state.checkout.currentStep,
        cartCounter: state.cart.counter
    }
}

const mapDispatchToProps = dispatch => {
    return{
        onChangeCheckoutStep(step){
            dispatch(changeCheckoutStep(step))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Checkout)