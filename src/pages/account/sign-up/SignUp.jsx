import React from 'react'
import './SignUp.scss'
import { api } from '../../../core/helpers/api';
import $ from 'jquery'
import FacebookLogin from 'react-facebook-login';
import GoogleLogin from 'react-google-login';

class SignUp extends React.Component{
    constructor(props){
        super(props)
        this.state = { 
            signupData: {email: '', password: ''},
            message: { text: '', color: '' }
        }

        this.onRegister=this.onRegister.bind(this)
        this.fbLogin=this.fbLogin.bind(this)
        this.gLogin=this.gLogin.bind(this)
        this.sendAccessToken=this.sendAccessToken.bind(this)
        this.sessionCreator=this.sessionCreator.bind(this)
    }
    sessionChecker(){
        var userOn = localStorage.getItem(process.env.REACT_APP_LOCAL_TOKEN)
        if(userOn)
            this.props.history.push('/profile')
    }
    onType(e){
        var id = e.target.id
        var value = e.target.value
        this.setState({ signupData: { ...this.state.signupData, [id]: value }, message: { text:'',color:'' } })
    }
    onRegister(){
        const { signupData } = this.state
        if(signupData.email === '' || signupData === ''){
            this.setState({ message: { text: 'Complete fields correctly', color: 'red' } })
        } else {
            const data = {
                email: signupData.email,
                password: signupData.password,
            }
            api.post('/signup', data, (res) => {
                console.log(res)
                if(res){
                    if(res.data){
                        this.setState({ message: { text: 'Registered successfully', color: 'green' } })
                        this.onLogin(data)
                        //setTimeout(() => { window.location.href = '/auth' }, 1500)
                    } else {
                        this.setState({ message: { text: 'Error try again', color: 'red' } })
                    }
                }
            })
        }
    }
    onLogin(data){
        const params = data
        api.post('/login', params, (res) => {
            console.log('res: ', res)
            //alert(`Res: ${JSON.stringify(res)}`)
            if(res.data){
                this.setState({ message: { text: 'Correct successfulll', color: 'green' } })
                const data = res.data.data
                this.sessionCreator(data)
            }else {
                this.setState({ message: { text: 'Incorrect user or password', color: 'red' }, btnLoginDisabled: false })
            }
            //this.setState({ btnLoginDisabled: false })
        })
    }
    sessionCreator(data){
        const token = { 
            "X-Auth-Token": data.authToken ? data.authToken : data.token,
            "X-User-Id": data.userId
        }
        const localToken = JSON.stringify(token)
        localStorage.setItem(process.env.REACT_APP_LOCAL_TOKEN, localToken)
        localStorage.setItem(process.env.REACT_APP_LOCAL_UID, data.userId)
        setTimeout(() => {
            window.location.href = '/profile'
        }, 1000);
    }
    fbLogin(fbres){
        if(fbres){
            const accessToken = fbres.accessToken
            if(accessToken)
                this.sendAccessToken(accessToken, 'facebook')
        }
    }
    gLogin(gres){
        if(gres){
            const accessToken = gres.accessToken
            if(accessToken)
                this.sendAccessToken(accessToken, 'google')
        }
    }
    sendAccessToken(accessToken, type){
        var endpoint = type === 'facebook' ? '/users/signin-facebook' : '/users/signin-google'
        api.post(endpoint, {accessToken}, (res) => {
            console.log(res)
            if(res.data){
                if(res.data.status === 'success'){
                    const data = res.data.data
                    this.sessionCreator(data)
                } else { alert('ERROR: Login error, try again') }
            }
        })
    }
    sessionCreator(data){
        const token = { 
            "X-Auth-Token": data.authToken ? data.authToken : data.token,
            "X-User-Id": data.userId
        }
        const localToken = JSON.stringify(token)
        localStorage.setItem(process.env.REACT_APP_LOCAL_TOKEN, localToken)
        localStorage.setItem(process.env.REACT_APP_LOCAL_UID, data.userId)
        setTimeout(() => {
            window.location.href = '/profile'
        }, 1000);
    }

    componentDidMount(){
        this.sessionChecker()
        $('html, body').animate({ scrollTop: 1 }, );
    }
    render(){
        const { message } = this.state
        console.log(this.state)
        return(
            <div className='signup'>
                <div className='signup_container'>
                    <div className='signup_container_left'>
                        <div className='signup_container_left_form'>
                            <h2>REGISTER</h2>
                            <span style={{color: message.color}} >{message.text}</span>
                            <label>EMAIL</label>
                            <input type="text" id="email" onChange={(e)=>this.onType(e)} />
                            <label>PASSWORD</label>
                            <input type="password" id='password' onChange={(e)=>this.onType(e)} />
                            <div><button onClick={this.onRegister} >SIGN UP</button></div>
                        </div>
                    </div>
                    <div className='signup_container_center'>
                        <div className='signup_container_center_up'/>
                        <span>OR</span>
                        <div className='signup_container_center_down'/>
                    </div>
                    <div className='signup_container_right'>
                        <div className='signup_container_right_form'>
                            <label>CONNECT WITH</label>
                            <div className='signup_container_right_form_buttons'>
                                <FacebookLogin
                                    appId='1284225565049629'
                                    autoLoad={false}
                                    fields={'name,email,picture'}
                                    callback={this.fbLogin}
                                    cssClass='signup_container_right_form_buttons_facebook'
                                    textButton=''
                                />
                                <GoogleLogin
                                    clientId="340779959209-012bva4ia2j5r0q6ia376np814bervg8.apps.googleusercontent.com"
                                    buttonText="Login"
                                    onSuccess={this.gLogin}
                                    onFailure={this.gLogin}
                                    cookiePolicy={'single_host_origin'}
                                    render={renderProps => (
                                        <button className='signup_container_right_form_buttons_google'
                                            onClick={renderProps.onClick} disabled={renderProps.disabled}>
                                        </button>
                                    )}
                                />
                                {/* <button className='signup_container_right_form_buttons_facebook'/> */}
                                {/* <button className='signup_container_right_form_buttons_google'/> */}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default SignUp