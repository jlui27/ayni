import React from 'react'
import './SectionsMenu.scss'
import { withRouter } from 'react-router-dom'
import ProfileGeneralPreview from '../sections/general-preview/GeneralPreview';
import { changeSection } from '../../../../core/store/actions/account';
import { connect } from 'react-redux';
import { sections } from './sections';
import MobileSectionsMenu from './mobile-selector/MobileSectionsMenu';
import { alertConfirm } from '../../../../core/constants/alert';

class ProfileMenu extends React.Component{
    constructor(){
        super()
        this.onSectionSelected=this.onSectionSelected.bind(this)
    }
    onSectionSelected(i, route){
        const { profileSection, changeSection } = this.props
        if(i !== profileSection){
            changeSection(i)
            this.props.history.push(route)
        }
    }
    logout(){
        //const confirm = window.confirm('Your session will be closed')
        alertConfirm('Your session will be closed', (rescall) => {
            if(rescall){
                localStorage.removeItem(process.env.REACT_APP_LOCAL_TOKEN)
                localStorage.removeItem(process.env.REACT_APP_LOCAL_UID)
                // Limpiar reduz si la pagina no es refrescada
                window.location.href = '/auth'
            }
        })
    }
    componentDidMount(){
        const currentSession = localStorage.getItem(process.env.REACT_APP_LOCAL_TOKEN)
        if(currentSession){
            const route = this.props.match.params.section
            const { changeSection } = this.props
            switch(route){
                case 'general-preview': changeSection(0); break;
                case 'edit': changeSection(1); break;
                case 'my-address': changeSection(2); break;
                case 'my-card': changeSection(3); break;
                case 'order-records': changeSection(4); break;
                case 'my-coupons': changeSection(5); break;
                default: changeSection(0); break;
            }
        } else {
            this.props.history.push('/auth')
        }
    }
    render(){
        return(
            <div className='profile-menu'>
                <div className='profile-menu_sections'>
                    <div className='profile-menu_sections_header'>
                        <div className='profile-menu_sections_header_user-icon'/>
                        <div className='profile-menu_sections_header_text'>My Account</div>
                        <button className='profile-menu_sections_header_logout' onClick={this.logout}>Logout</button>
                    </div>
                    <div className='profile-menu_sections_options'>
                        {sections.map((val, i) => {
                            const { profileSection } = this.props
                            const selected = profileSection===i?{ backgroundColor: '#a3a3a2' }:null
                            const backIcon = { backgroundImage: `url(${process.env.REACT_APP_URL_BASE+val.icon})`}
                            if(i !== 6){ // position 6 belongs to Logout
                                return(
                                    <div className='profile-menu_sections_options_content' key={i} style={selected} onClick={()=>this.onSectionSelected(i, val.route)}>
                                        <div className='profile-menu_sections_options_content_icon' style={backIcon} />
                                        <div className='profile-menu_sections_options_content_text'>{val.name}</div>
                                    </div>
                                )
                            }
                            
                        })}
                    </div>
                    <MobileSectionsMenu/>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        profileSection: state.account.sectionSelected
    }
}
const mapDispatchToProps = dispatch => {
    return {
        changeSection(section){
            dispatch(changeSection(section))
        }
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(withRouter(ProfileMenu))