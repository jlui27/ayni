export const sections=[
    {icon: '/images/icons/house.svg', name: 'General preview', route: '/profile/general-preview'},
    {icon: '/images/icons/pencil.svg', name: 'Edit profile', route: '/profile/edit'},
    {icon: '/images/icons/pin.svg', name: 'My address', route: '/profile/my-address'},
    {icon: '/images/icons/tarjeta.svg', name: 'My card', route: '/profile/my-card'},
    {icon: '/images/icons/note.svg', name: 'Orders history (1)', route: '/profile/order-records'},
    {icon: '/images/icons/ticket.svg', name: 'My coupons  (1)', route: '/profile/my-coupons'},
    {icon: '', name: 'Logout', route: ''}
]