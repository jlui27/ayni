import React, { Component } from 'react'
import './MobileSectionsMenu.scss'
import { connect } from 'react-redux';
import { changeSection } from '../../../../../core/store/actions/account';
import { sections } from '../sections';
import { withRouter } from 'react-router-dom';
import { alertConfirm } from '../../../../../core/constants/alert';

class MobileSectionMenu extends Component{
    constructor(props){
        super(props)
        this.state = {
            showSections: false
        }
        this.onSectionSelected=this.onSectionSelected.bind(this)
    }
    onSectionSelected(i, route){
        const { profileSection, changeSection } = this.props
        if(i !== 6){
            if(i !== profileSection){
                changeSection(i)
                this.props.history.push(route)
            }
        } else{ this.logout() }
        this.setState({ showSections: false })
    }
    logout(){
        alertConfirm('Your session will be closed', (rescall) => {
            if(rescall){
                localStorage.removeItem(process.env.REACT_APP_LOCAL_TOKEN)
                localStorage.removeItem(process.env.REACT_APP_LOCAL_UID)
                window.location.href = '/auth'
            }
        })
    }
    closeMenuOutside(){
        document.addEventListener("click", (evt) => {
            const flyoutElement = document.getElementById("mobile-sections-menu_container");
            let targetElement = evt.target; // clicked element
            do {
                if (targetElement == flyoutElement) {
                    return;
                }
                targetElement = targetElement.parentNode;
            } while (targetElement);
            const { showSections } = this.state
            if(showSections)
                this.setState({ showSections: false })
        });
    }
    componentDidMount(){
        this.closeMenuOutside()
    }
    render(){
        const { profileSection } = this.props
        const { showSections } = this.state
        const displatOptions = !showSections ? { display: 'none' } : null
        const currentPath = window.location.pathname
        return(
            <div className='mobile-sections-menu'>
                <div className='mobile-sections-menu_container' id="mobile-sections-menu_container">
                    <div className='mobile-sections-menu_container_selector' onClick={()=>this.setState({ showSections: !showSections })}>{sections[profileSection].name}</div>
                    <div className='mobile-sections-menu_container_options' style={displatOptions}>
                        {sections.map((data, i) => {
                            const selected = profileSection === i ? { backgroundColor: '#F6F6F6' } : null
                            return(
                                <div className={`mobile-sections-menu_container_options_content-${i}`} style={selected} onClick={()=>this.onSectionSelected(i, data.route)} key={i} >{data.name}</div>
                            )
                        })}
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        profileSection: state.account.sectionSelected
    }
}
const mapDispatchToProps = dispatch => {
    return {
        changeSection(section){
            dispatch(changeSection(section))
        }
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MobileSectionMenu))