import React from 'react'
import './Profile.scss'
import ProfileMenu from './sections-menu/SectionsMenu';
import ProfileGeneralPreview from './sections/general-preview/GeneralPreview';
import ProfileEdit from './sections/profile-edit/ProfileEdit';
import { connect } from 'react-redux';
import MyAddress from './sections/address/MyAddress';
import MyCard from './sections/my-card/MyCard';
import MyOrderRecords from './sections/order-records/MyOrderRecords';
import MyCoupons from './sections/my-coupons/MyCoupons';

class Profile extends React.Component{
    sectionSetter(){
        const { profileSection } = this.props
        switch(profileSection){
            case 0: return <ProfileGeneralPreview />;
            case 1: return <ProfileEdit />;
            case 2: return <MyAddress />;
            case 3: return <MyCard />;
            case 4: return <MyOrderRecords />;
            case 5: return <MyCoupons />;
            default: break;
        }
    }
    render(){
        return(
            <div className='profile'>
                <ProfileMenu />
                {/* <ProfileGeneralPreview/> */}
                {this.sectionSetter()}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        profileSection: state.account.sectionSelected
    }
}


export default connect(mapStateToProps)(Profile)