import React from 'react'
import './ProfileEdit.scss'
import { api } from '../../../../../core/helpers/api';
import $ from 'jquery'

class ProfileEdit extends React.Component{
    constructor(){
        super()
        this.state = {
            userData: null,
            selectedSex: 0,
            statesList: [],
            messageServer: { text: '', color: '' }
        }
        this.changeSex=this.changeSex.bind(this)
        this.sexoOptions = ['Male', 'Female', 'Others']
        this.onSave=this.onSave.bind(this)
    }
    getUserData(){
        api.get('/users/me', [], (res) => {
            if(res.data){
                const data = res.data.data
                this.setState({ userData: data, selectedSex: parseInt(data.id_gender) })
            }
        })
    }
    getStates(){
        api.get('/states', '', (res) => {
            if(res.data)
                if(res.data.status === 'success')
                    this.setState({ statesList: res.data.data })
        })
    }
    onType(e){
        var id = e.target.id
        var value = e.target.value
        this.setState({ userData: { ...this.state.userData, [id]: value } })
    }
    changeSex(i){
        const { selectedSex, userData } = this.state
        if(i !== selectedSex)
            this.setState({ selectedSex: i, userData: { ...userData, id_gender: i } })
    }
    onSave(){
        const { userData } = this.state
        const { firstName, lastName, birthday, id_gender, phone, email, id_state, postcode, reference, address, city, countryId, addressId } = userData
        if(firstName && lastName && birthday && id_gender && phone && email && id_state && postcode){
            var params = {
                firstName,
                lastName,
                birthday,
                id_gender,
                phone,
                email,
                state: id_state,
                postcode,
                reference: !reference?'DATANULL':reference,
                address: !address?'DATANULL':address,
                city: !city?'DATANULL':city,
                countryId: !countryId?21:countryId,
                addressId: !addressId?0:addressId
            }
            //console.log('params ', params)
            
            api.put('/users/me', params, (res) => {
                console.log(res)
                if(res.data){
                    if(res.data.status === 'success')
                        this.setState({ messageServer: { text: 'User data saved', color: 'green' } })
                    else
                        this.setState({ messageServer: { text: 'Error when saving data', color: 'red' } })
                }
            })
            
        } else {
            this.setState({ messageServer: { text: 'Complete all fields', color: 'orange' } })   
        }
        $('html, body').animate({ scrollTop: 20 }, 'fast' );
        setTimeout(() => { this.setState({ messageServer: { text: '', color: '' } }) }, 3000);
    }
    
    componentDidMount(){
        this.getUserData()
        this.getStates()
    }
    render(){
        const { userData, selectedSex, statesList, messageServer } = this.state
        console.log('user data got: ', userData)
        return(
            <div className='profile-edit'>
                <h2>Edit profile</h2>
                <span style={{color: messageServer.color}}>{messageServer.text}</span>
                <div className='profile-edit_container'>
                    <div className='profile-edit_container_photo'>
                        <div className='profile-edit_container_photo_image'/>
                        <label>
                            <input type="file"/>
                            Upload picture
                        </label>
                    </div>
                    <div className='profile-edit_container_form'>
                        <div className='profile-edit_container_form_fullname'>
                            <div className='profile-edit_container_form_fullname_name'>
                                <label htmlFor="firstName">Names</label>
                                <input type="text" id="firstName" defaultValue={userData?userData.firstName!=='undefined'?userData.firstName:null:null} onChange={(e)=>this.onType(e)} />
                            </div>
                            <div className='profile-edit_container_form_fullname_lastname'>
                                <label htmlFor="lastName">Last names</label>
                                <input type="text" id="lastName" defaultValue={userData?userData.lastName!=='undefined'?userData.lastName:null:null} onChange={(e)=>this.onType(e)} />
                            </div>
                        </div>
                        <div className='profile-edit_container_form_birth-sexo'>
                            <div className='profile-edit_container_form_birth-sexo_birth'>
                                <label htmlFor="birthday">Brithday</label>
                                <input type="date" id="birthday" defaultValue={userData?userData.birthday!=='undefined'?userData.birthday:null:null} onChange={(e)=>this.onType(e)} />
                            </div>
                            <div className='profile-edit_container_form_birth-sexo_sexo'>
                                <label>Sex</label>
                                <div className='profile-edit_container_form_birth-sexo_sexo_content'>
                                    {this.sexoOptions.map((text, i) => {
                                        return(
                                            <div className='profile-edit_container_form_birth-sexo_sexo_content_option' key={i} onClick={()=>this.changeSex(i)}>
                                                <div className='profile-edit_container_form_birth-sexo_sexo_content_option_icon'
                                                style={selectedSex===i?{backgroundColor: '#a3a3a2'}:null} />
                                                <span>{text}</span>
                                            </div>
                                        )
                                    })}
                                </div>
                            </div>
                        </div>
                        <div className='profile-edit_container_form_telf-email'>
                            <div className='profile-edit_container_form_telf-email_telf'>
                                <label htmlFor="phone">Phone</label>
                                <input type="tel" id="phone" defaultValue={userData?userData.phone:null} onChange={(e)=>this.onType(e)} />
                            </div>
                            <div className='profile-edit_container_form_telf-email_email'>
                                <label htmlFor="email">Email</label>
                                <input type="email" id="email" defaultValue={userData?userData.email:null} disabled />
                            </div>
                        </div>
                        <div className='profile-edit_container_form_states-postcode'>
                            <div className='profile-edit_container_form_states-postcode_state'>
                                <label htmlFor="id_state">State</label>
                                <select id="id_state" onChange={(e)=>this.onType(e)}>
                                    <option value="0" defaultChecked hidden>Select</option>
                                    {statesList.length>0?statesList.map((data, i) => {
                                        if(userData)
                                            if(userData.id_state){
                                                if(parseInt(userData.id_state) === data.id_state)
                                                    return( <option key={i} value={data.id_state} selected>{data.name}</option> )
                                                else
                                                    return( <option key={i} value={data.id_state}>{data.name}</option> )
                                            } else {
                                                return( <option key={i} value={data.id_state}>{data.name}</option> )
                                            }
                                                
                                    }):null}
                                </select>
                            </div>
                            <div className='profile-edit_container_form_states-postcode_postcode'>
                                <label htmlFor="postcode">Post code</label>
                                <input type="text" id="postcode" defaultValue={userData?userData.postcode?userData.postcode:null:null} onChange={(e)=>this.onType(e)} />
                            </div>
                        </div>
                        <button onClick={this.onSave}>SAVE</button>
                    </div>
                </div>
            </div>
        )
    }
}

export default ProfileEdit