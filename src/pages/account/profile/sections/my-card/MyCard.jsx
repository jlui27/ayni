import React from 'react'
import './MyCard.scss'
import { toCapitalize } from '../../../../../core/helpers/formats';
import $ from 'jquery'
import { api } from '../../../../../core/helpers/api';
import { alertConfirm } from '../../../../../core/constants/alert';

class MyCard extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            userData: null,
            yearList: [],
            monthList: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
            cardData: { cardNumber:null, expirationDate:null, editDate:null, editYear:null, code:null },
            message: null, messageColor: null
        }
        this.onType=this.onType.bind(this)
        this.saveCard=this.saveCard.bind(this)

        this.date = new Date()
        this.currentDate = this.date.getFullYear()
    }
    onType(e){
        var id = e.target.id
        var value = e.target.value
        const { cardData, monthList } = this.state
        if(id === 'editDate'){
            var pos = monthList.indexOf(toCapitalize(value)) + 1
            value = pos<10?'0'+pos:pos
        } else if(id === 'editYear') {
            value = value.substring(2,4)
        }
        this.setState({ cardData: { ...cardData, [id]: value } })
    }
    getUserData(){
        api.get('/users/me', '', res => {
            if(res.data){
                const data = res.data.data
                this.setState({ userData: data })
            }
        })
    }
    setYearList(){
        const  { yearList } = this.state
        const list = yearList
        for (let i = this.currentDate; i < this.currentDate + 20; i++) {
            list.push(i)
        }
        this.setState({ yearList: list })
    }
    saveCard(){
        const { cardNumber, code, editDate, editYear } = this.state.cardData
        const params = { cardNumber, code, expirationDate: editDate+editYear }
        const { userData } = this.state
        if(userData){
            if(userData.id_state){
                if(userData.id_state !== '0'){
                    if(cardNumber && code && editDate && editYear){
                        api.post('/payments/customers/me/card', params, (res) => {
                            if(res.data){
                                if(res.data.status === 'success')
                                    this.setState({ message: 'Credit card updated', messageColor: 'green' })
                            } else if(res.response.data.status === 'fail')
                                this.setState({ message: 'Invalid credit card', messageColor: 'red' })
                        })
                    } else {
                        this.setState({ message: 'Complete fields correctly', messageColor: 'red' })
                    }
                    $('html, body').animate({ scrollTop: 20 }, 'fast' );
                    setTimeout(() => { this.setState({ message: null, messageColor: null }) }, 5000);           
                } else {
                    alertConfirm('Please add your state and zip code.', res => {
                        if(res){window.location.pathname='/profile/edit'}}
                    )
                }
            }
        }
    }
    componentDidMount(){
        this.setYearList()
        this.getUserData()
    }
    render(){
        const  { yearList, monthList, message, messageColor } = this.state
        return(
            <div className='my-card'>
                <div className='my-card_container'>
                    <div className='my-card_container_title'>My card</div>
                    <div style={{color: messageColor}}>{message}</div>
                    <div className='my-card_container_body'>
                        <div className="my-card_container_body_form">
                            <span>We accept credit cards (Visa, MasterCard and American Express) and Debit (Visa).</span>
                            <div className="my-card_container_body_form_card">
                                <label htmlFor="cardNumber">Credit card or Debit</label>
                                <div className='my-card_container_body_form_card_textfield'>
                                    <input type="text" id='cardNumber' placeholder='1111 2222 3333 4444' onChange={(e)=>this.onType(e)} />
                                    <div/>
                                </div>
                            </div>
                            <div className='my-card_container_body_form_expired-date'>
                                <label htmlFor="edit-date">Expiración date</label>
                                <div className='my-card_container_body_form_expired-date_options'>
                                    <select id='editDate' onChange={(e)=>this.onType(e)}>
                                        <option value="0" defaultChecked hidden>Month</option>
                                        {monthList.map((month, i) => {
                                            return(
                                                <option key={i} value={month.toLowerCase()} >{month}</option>
                                            )
                                        })}
                                    </select>
                                    <select id='editYear' onChange={(e)=>this.onType(e)}>
                                        <option value="0" defaultChecked hidden>Year</option>
                                        {yearList.map((year, i) => {
                                            return(
                                                <option key={i} value={year}>{year}</option>
                                            )
                                        })}
                                    </select>
                                </div>
                            </div>
                            <div className="my-card_container_body_form_security-code">
                                <label htmlFor="code">Security code</label>
                                <div className='my-card_container_body_form_security-code_textfield'>
                                    <input type="text" id='code' placeholder='ccv' onChange={(e)=>this.onType(e)} />
                                    <div/>
                                </div>
                            </div>
                            <span id='aynis-obs' >Ayni will make a temporary authorization on your card to verify it. Your bank may inform you about this authorization.</span>
                            <button onClick={this.saveCard}>SAVE PAYMENT DATA</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default MyCard