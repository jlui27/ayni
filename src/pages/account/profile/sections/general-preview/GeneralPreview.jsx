import React from 'react'
import './GeneralPreview.scss'
import { api } from '../../../../../core/helpers/api';
import { connect } from 'react-redux';
import { changeSection } from '../../../../../core/store/actions/account';
import { withRouter } from 'react-router-dom';

class ProfileGeneralPreview extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            userData: null, cardData: null
        }
        this.labelData = ['Names', 'Lastnames', 'Birthday', 'Sex', 'Email']
        this.userData = ['Alexis', 'Lara', '2/02/94', 'Masculino', 'alexislara41@gmail.com']
        this.userAddress = ['Alexis Lara', 'casa', 'Av. Javier Prado Este', '7721', 'Dpto 201']
        this.sexOptions = ['Male', 'Female', 'Others']
    }
    getUserData(){
        api.get('/users/me', [], (res) => {
            if(res.data){
                const data = res.data.data
                this.setState({ userData: data })
            }
        })
    }
    getCard(){
        api.get('/payments/customers/me/card', '', (res) => {
            if(res.data)
                if(res.data.status === 'success')
                    this.setState({ cardData: res.data.data })
        })
    }
    onChangeSection(section, route){
        const { changeSection } = this.props
        changeSection(section)
        this.props.history.push(route)
    }

    componentDidMount(){
        this.getUserData()
        this.getCard()
    }
    render(){
        const { userData, cardData } = this.state
        const cardNumber = cardData?cardData.cardNumber?cardData.cardNumber.replace(/X/g,'*'):null:null
        const expirationDate = cardData?cardData.expirationDate?cardData.expirationDate:null:null
        //const code = cardData?cardData.code?cardData.code:null:null
        var profileData = []
        var addressData = []
        if(userData){
            profileData.push(userData.firstName, userData.lastName, userData.birthday, userData.id_gender, userData.email)
            addressData.push(userData.address?userData.address.replace(/\|/g, '-'):userData.data, userData.city, userData.reference)
        }
        return(
            <div className='profile-general-preview'>
                <div className='profile-general-preview_title'>General preview</div>
                <div className='profile-general-preview_container'>
                    <div className='profile-general-preview_container_left'>
                        <div className='profile-general-preview_container_left_data'>
                            <h4>Profile</h4>
                            {this.labelData.map((text, i) => {
                                return(
                                    <div className='profile-general-preview_container_left_data_content' key={i}>
                                        <label>{text}</label>
                                        <span>{text==='Sex'?this.sexOptions[profileData[i]] : profileData[i] !== 'undefined' ? 
                                            profileData[i] === '0000-00-00' ? '-' : profileData[i] : '-'}</span>
                                    </div>
                                )
                            })}
                            <button onClick={()=>this.onChangeSection(1, '/profile/edit')} >EDIT PROFILE</button>
                        </div>
                        <div className='profile-general-preview_container_left_breakup'/>
                        <div className='profile-general-preview_container_left_cupons'>
                            <div className='profile-general-preview_container_left_cupons_content'>
                                <div className='profile-general-preview_container_left_cupons_content_icon'/>
                                <div className='profile-general-preview_container_left_cupons_content_title'>You don't have coupons</div>
                                <span>As soon as you have them, you can consult them to apply them in your purchases.</span>
                            </div>
                        </div>
                    </div>
                    <div className='profile-general-preview_container_center' />
                    <div className='profile-general-preview_container_right'>
                        <div className='profile-general-preview_container_right_container'>
                            <div className='profile-general-preview_container_right_container_address'>
                                <h4>My address</h4>
                                <div className='profile-general-preview_container_right_container_address_data'>
                                    {addressData.map((text, i) => {
                                        return( <span key={i}>{text==='DATANULL'?'-':text}</span> )
                                    })}
                                </div>
                                <button onClick={()=>this.onChangeSection(2, '/profile/my-address')} >EDIT ADDRESS</button>
                            </div>
                            <div className='profile-general-preview_container_right_container_card'>
                                <h4>My card</h4>
                                {cardData ? 
                                <div className='profile-general-preview_container_right_container_card_data'>
                                    <div className='profile-general-preview_container_right_container_card_data_icon' />
                                    <div className='profile-general-preview_container_right_container_card_data_info'>
                                        <div className='profile-general-preview_container_right_container_card_data_info_name'>Visa {cardNumber}</div>
                                        <div className='profile-general-preview_container_right_container_card_data_info_expires'>Expiration: {expirationDate}</div>
                                    </div>
                                </div>
                                : <div style={{fontFamily: 'OpenSans-Regular'}}>Not found</div> }
                                <button onClick={()=>this.onChangeSection(3, '/profile/my-card')} >UPDATE</button>
                            </div>
                            <div className='profile-general-preview_container_right_container_cupons'>
                                <div className='profile-general-preview_container_right_container_cupons_content'>
                                    <div className='profile-general-preview_container_right_container_cupons_content_icon'/>
                                    <div className='profile-general-preview_container_right_container_cupons_content_title'>You don't have coupons</div>
                                    {/* <span>En cuanto los tengas, podrás consultarlos para aplicarlos en tus compras.</span> */}
                                    <span>As soon as you have them, you can consult them to apply them in your purchases.</span>
                                </div>
                            </div>
                            <div className='profile-general-preview_container_right_container_orders'>
                                <div className='profile-general-preview_container_right_container_orders_count'>
                                    <div className='profile-general-preview_container_right_container_orders_count_icon'/>
                                    <span>(0)</span>
                                </div>
                                <span>No orders placed</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        changeSection(section){
            dispatch(changeSection(section))
        }
    }
}

export default withRouter(connect(null, mapDispatchToProps)(ProfileGeneralPreview))