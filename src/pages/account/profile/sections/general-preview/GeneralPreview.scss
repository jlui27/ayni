@import '../../../../../styles/mainv2.scss';

.profile-general-preview {
    width: 100%; padding-top: 35px; display: flex; flex-direction: column;
    &_title{ 
        padding-left: 45px;
        @include letterProps(OpenSans-SemiBold, 35px, 600, normal, normal, .71, normal, left, #393939); 
    }
    &_container {
        width: 100%; height: 100%; padding-top: 45px;
        display: flex;
        &_left {
            width: 50%;
            display: flex; flex-direction: column;
            &_data {
                display: flex; flex-direction: column; padding-left: 75px;
                & h4 { @include letterProps(Raleway-SemiBold, 16px, 600, normal, normal, 1.13, .1px, left, #393939); }
                &_content {
                    display: flex; flex-direction: column; margin-bottom: 20px;
                    & label { @include letterProps(Raleway-Medium, 12px, 500, normal, normal, 1.17, normal, left, #a3a3a2); }
                    & span { @include letterProps(Raleway-Medium, 14px, 500, normal, normal, 1.14, normal, left, #393939); padding-top: 5px; }
                }
                & button{
                    @include profileButton(210px, 45px, auto, 20px);
                }
            }
            &_breakup { border: #8e8e8e 0.5px solid; width: 90%; margin-top: 40px; margin-left: 14px; }
            &_cupons{
                display: flex; justify-content: center; padding-left: 75px; padding-top: 30px;
                &_content{
                    display: flex; flex-direction: column; width: 240px; justify-content: center;
                    &_icon {
                        width: 70px; height: 40px; margin: auto;
                        background-repeat: no-repeat; background-position: center; background-size: contain; background-image: url('/images/icons/cupon2.png');
                    }
                    &_title { @include letterProps(Raleway-SemiBold, 18px, 600, normal, normal, 1.17, normal, center, #a3a3a2); margin-top: 12px; }
                    & span { @include letterProps(Raleway-Medium, 12px, 500, normal, normal, 1.33, normal, center, #393939); margin-top: 10px; }
                }
            }
        }
        &_center{
            border: 0.5px solid #8e8e8e; width: 1px;  height: 95%;
        }
        &_right {
            width: 50%;
            &_container {
                width: 90%; margin: auto;
                &_cupons{ display: none; }
                &_address {
                    display: flex; flex-direction: column; border-bottom: 1px solid #8e8e8e; padding-bottom: 21px;
                    & h4 { @include letterProps(Raleway-SemiBold, 16px, 600, normal, normal, 1.13, .1px, left, #393939); }
                    &_data {
                        display: flex; flex-direction: column;
                        & span {
                            @include letterProps(Raleway-Medium, 14px, 500, normal, normal, 1.14, normal, left, #393939);
                            padding-bottom: 5px;
                        }
                    }
                    & button { @include profileButton(210px, 45px, auto, 30px); }
                }
                &_card {
                    border-bottom: 1px #8e8e8e solid; padding-top: 20px; padding-bottom: 35px;
                    display: flex; flex-direction: column;
                    & h4 { @include letterProps(Raleway-SemiBold, 16px, 600, normal, normal, 1.13, .1px, left, #393939); }
                    & button { @include profileButton(210px, 45px, auto, 20px); }
                    &_data {
                        width: 375px; height: 87px; border: 1px solid #a3a3a2;
                        display: flex; align-items: center; margin-top: 20px;   
                        &_icon { @include backImage('/images/fixed/visa.png', 30px, 30px); margin-left: 46px; }
                        &_info {
                            display: flex; flex-direction: column; align-items: center; margin-left: 78px;
                            &_name {
                                @include letterProps(Raleway-Medium, 14px, 500, normal, normal, 1.14, .3, left, #393939);
                            }
                            &_expires {
                                @include letterProps(Raleway-Regular, 12px, normal, normal, normal, 1.17, normal, left, #393939);
                                padding-top: 2px;
                            }
                        }
                    }
                }
                &_orders {
                    width: 100%; padding-bottom: 25px;
                    &_count {
                        display: flex; justify-content: center; align-items: center; padding-top: 25px; padding-bottom: 10px;
                        &_icon { @include backImage('/images/icons/loan.png', 35px, 35px) }
                        & > span { @include letterProps(Raleway-Bold, 26px, bold, normal, normal, 1.15, normal, left, #a3a3a2); }
                    }
                    & > span { 
                        @include letterProps(Raleway-SemiBold, 18px, bold, normal, normal, 1.15, normal, left, #a3a3a2); 
                    }
                }
            }
        }
    }
}

@media screen and (max-width: $mobile-width){
    .profile-general-preview{
        width: 100%;
        &_title{
            //padding-left: 0;
            //font-family: Gotham-BOld; 
            font-size: 22px;
        }
        &_container{
            display: flex; flex-direction: column;
            &_left{
                width: 100%;
                &_data{
                    margin: auto; width: 90%;padding-left: 0; padding-top: 20px; padding-bottom: 30px;
                    border-top: 1px #707070 solid; border-bottom: 1px #707070 solid;
                    & h4{ padding-bottom: 10px; padding-left: 30px;  }
                    &_content{
                        margin-bottom: 10px; padding-left: 30px;
                    }
                    & button{ @include profileButton(210px, 45px, auto, 20px); }
                }
                &_breakup{ display: none; }
                &_cupons{ display: none; }
            }
            &_center{
                display: none;
            }
            &_right{
                width: 100%;
                &_container{
                    width: 90%;
                    &_address{
                        padding-top: 20px; padding-bottom: 30px;
                        & h4{ padding-left: 30px; }
                        &_data{
                            padding-left: 30px;
                            & span{ font-size: 14px; }
                        }
                    }
                    &_card{
                        & h4{ padding-left: 30px; }
                        &_data{
                            margin-left: 30px; width: 250px;
                            display: flex; justify-content: space-evenly;
                            &_icon{ margin-left: 0; }
                            &_info{
                                margin-left: 0;
                            }
                        }
                    }
                    &_cupons{
                        display: flex; justify-content: center; padding-top: 30px; padding-bottom: 25px;
                        border-bottom: 1px solid #707070;
                        &_content{
                            display: flex; flex-direction: column; width: 260px; justify-content: center;
                            &_icon {
                                width: 70px; height: 40px; margin: auto;
                                background-repeat: no-repeat; background-position: center; background-size: contain; background-image: url('/images/fixed/coupon.png');
                            }
                            &_title { @include letterProps(Gotham-Bold, 18px, 600, normal, normal, 1.17, normal, center, #1C1C1A); margin-top: 12px; }
                            & span { @include letterProps(Gotham-Medium, 12px, 500, normal, normal, 1.33, normal, center, #1C1C1A); margin-top: 10px; }
                        }
                    }
                    &_orders{
                        //background-color: antiquewhite;
                        padding-top: 15px; padding-bottom: 30px; border-bottom: 1px #707070 solid;
                        &_count{
                            &_icon{ @include backImage('/images/fixed/order-icon.png', 35px, 35px) }
                            & > span{ color: #1C1C1A; }
                        }
                        & > span{
                            //font-family: Gotham-Bold; 
                            color: #1C1C1A;
                        }
                    }
                }
            }
        }
    }
}