import React from 'react'
import './MyOrderRecords.scss'
import { api } from '../../../../../core/helpers/api';

class MyOrderRecords extends React.Component{
    getOrders(){
        api.get('/payments/customers/me/orders', '', (res) => {
            console.log('orders... ', res)
        })
    }
    componentDidMount(){
        this.getOrders()
    }
    render(){
        return(
            <div className='my-order-records'>
                <div className='my-order-records_container'>
                    <div className='my-order-records_container_title'>My orders (1)</div>
                    <div className='my-order-records_container_body'>
                        <div className='my-order-records_container_body_content'>
                            <div className='my-order-records_container_body_content_left'>
                                <div className='my-order-records_container_body_content_left_purchase-date'>
                                    <label>Purchase made:</label>
                                    <span>January 09, 2017</span>
                                </div>
                                <div className='my-order-records_container_body_content_left_product'>
                                    <img src="/images/test/productx.png" alt="" />
                                    <div className='my-order-records_container_body_content_left_product_detail'>
                                        <div className='my-order-records_container_body_content_left_product_detail_name'>Floral Off-the-Shoulder Romper</div>
                                        <div className='my-order-records_container_body_content_left_product_detail_others'>Rojo - S - 2 Uds</div>
                                        <div className='my-order-records_container_body_content_left_product_detail_price'>$ 120.00</div>
                                        <div className='my-order-records_container_body_content_left_product_detail_delivered'>Delivered January 10</div>
                                    </div>
                                </div>
                                <div className='my-order-records_container_body_content_left_delivered'>
                                    <label>Delivered January 10</label>
                                </div>
                            </div>
                            <div className='my-order-records_container_body_content_right'>
                                <button>Details</button>
                                <span>$ 120.oo</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default MyOrderRecords