import React from 'react'
import './MyCoupons.scss'

class MyCoupons extends React.Component{
    render(){
        return(
            <div className='my-coupons'>
                <div className="my-coupons_container">
                    <div className='my-coupons_container_title'>My Coupons (1)</div>
                    <div className='my-coupons_container_body'>
                        <div className='my-coupons_container_body_content'>
                            <div className='my-coupons_container_body_content_left'>
                                <div className='my-coupons_container_body_content_left_pic'></div>
                                <div className='my-coupons_container_body_content_left_detail'>
                                    <label>Coupon's name</label>
                                    <span className='coupon-name'>75% discount on your purchase</span>
                                    <span className='coupon-availability'>Available until April 30</span>
                                </div>
                            </div>
                            <div className='my-coupons_container_body_content_right'>
                                <div className='my-coupons_container_body_content_right_coupon'>FOODN89979</div>
                                <span>Available until April 30</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default MyCoupons