import React from 'react'
import './MyAddress.scss'
import { api } from '../../../../../core/helpers/api';

// source: https://www.npmjs.com/package/google-maps-react
import { Map, InfoWindow, Marker, GoogleApiWrapper } from 'google-maps-react'
import { alertConfirm } from '../../../../../core/constants/alert';
//import { withRouter } from 'react-router-dom';

class MyAddress extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            userData: null,
            address: {},
            reference: {},
            message:'', messageColor:''
        }

        this.onSave=this.onSave.bind(this)
        this.onType=this.onType.bind(this)
        this.setFinalData=this.setFinalData.bind(this)
    }
    getUserData(){
        api.get('/users/me', [], (res) => {
            if(res.data){
                const data = res.data.data
                const address = data.address?data.address!=='DATANULL'?data.address.split('|'):null:null
                this.setState({ userData: data, address: address?{refAddress: address[0], refInside: address[1]}:{} })
            }
        })
    }
    onType(e){
        var id = e.target.id
        var value = e.target.value
        if(value.split('').indexOf('|') < 0)
            this.setState({ address: { ...this.state.address, [id]: value } })
        else
            this.onRemove(e)
        
        this.setState({ message: '', messageColor:'' })
    }
    onRemove(e){
        var id = e.target.id
        var value = e.target.value
        document.getElementById(id).value = value.substr(0, value.length-1)
    }
    setFinalData(){
        //const { refAddress, refInside } = this.state.address
        const refAddress = document.getElementById('refAddress').value
        const refInside = document.getElementById('refInside').value
        if(refAddress && refInside){
            //const newAddress = { refAddress, refInside }
            const newAddress = `${refAddress.trim()} | ${refInside.trim()}`
            this.setState({ userData: { ...this.state.userData, address: newAddress } }, () => { this.onSave() })
        } else {
            this.setState({ message: 'Complete!', messageColor: 'red' })
        }
    }
    onSave(){
        const { userData } = this.state
        const { firstName, lastName, birthday, id_gender, phone, email, id_state, 
            postcode, reference, address, city, countryId, addressId } = userData
        const newUserData = { ...userData, state: userData.id_state }
        //console.log('Data to save: ', newUserData)
        if(firstName && lastName && birthday && id_gender && phone && email && id_state && postcode){
            api.put('/users/me', newUserData, (res) => {
                //console.log(res)
                if(res.data)
                    if(res.data.status === 'success')
                        this.setState({ message: 'The address was saved successfully', messageColor: 'green' })
                    else
                        this.setState({ message: 'Error, try again', messageColor: 'red' })
            })
            setTimeout(() => { this.setState({ message:'', messageColor:'' }) }, 5000);
        } else {
            alertConfirm('¡¡Hey!! you forgot to add your personal information, press OK to complete now.', (rescall) => {
                if(rescall)
                    window.location.href = '/profile/edit'
            })
        }
    }

    componentDidMount(){
        this.getUserData()
    }
    render(){
        const { userData, address, message, messageColor } = this.state
        var defAddress = ''
        var defInside = ''
        if(address)
            if(address!=='DATANULL'){
                const { refAddress, refInside } = address; 
                defAddress=refAddress; 
                defInside=refInside
            }

        console.log('userData from my address: ', userData, 'Address STATE: ', this.state)
        const mapStyle = {
            width: '456px', height: '463px', marginLeft: '10%'
        }
        return(
            <div className='my-address'>
                <div className='my-address_container'>
                    <div className='my-address_container_title'>My address</div>
                    <div className='my-address_container_body'>
                        <div className='my-address_container_body_form'>
                            <div style={{color: messageColor, paddingBottom: '30px', fontFamily: 'OpenSans-SemiBold'}}>{message}</div>
                            <div className='my-address_container_body_form_instructions'>
                            The address you are going to enter will be saved and used by default for the shipments of your purchases.
                            </div>
                            <div className='my-address_container_body_form_address'>
                                <label htmlFor="refAddress">Address</label>
                                <input type="text" id='refAddress' placeholder='Enter your address' defaultValue={defAddress} onChange={(e)=>this.onType(e)} />
                            </div>
                            <div className='my-address_container_body_form_dep'>
                                <label htmlFor="refInside">Inside</label>
                                <input type="text" id='refInside' placeholder='Example: Apartment 201' defaultValue={defInside} onChange={(e)=>this.onType(e)} />
                            </div>
                            <button onClick={this.setFinalData}>SAVE ADDRESS</button>
                        </div>
                        <div className='my-address_container_body_map'>
                            {/* <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d52486.97599642697!2d-77.02639877802015!3d-12.104085130286009!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses-419!2spe!4v1545834569596" 
                            width="456" height="463" frameBorder="0" style={{border:0}} allowFullScreen></iframe> */}
                            {/* <Map google={this.props.google} zoom={4} style={mapStyle} >
                                <Marker onClick={this.onMarkerClick} name={'Current location'} />
                                <InfoWindow onClose={this.onInfoWindowClose}>

                                </InfoWindow>
                            </Map> */}
                        </div>
                    </div>
                </div>
            </div>
        )
        
    }
}

export default GoogleApiWrapper({
    apiKey: ('AIzaSyChRBOC7e9Hvp_mJzQBSfxFcNg4VfvimbU')
})(MyAddress)

