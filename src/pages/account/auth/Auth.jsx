import React from 'react'
import './Auth.scss'
import { withRouter } from 'react-router-dom';
import { api } from '../../../core/helpers/api';
//import $ from 'jquery'
import NavSmall from '../../../components/menu/nav-small/NavSmall';
import FacebookLogin from 'react-facebook-login';
import GoogleLogin from 'react-google-login';
import $ from 'jquery'

class Auth extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            data: {em: '', ps: ''},
            message: { text: '', color: '' },
            btnLoginDisabled: false,
            remember: false
        }
        this.onCheck=this.onCheck.bind(this)
        this.onLogin=this.onLogin.bind(this)
        this.fbLogin=this.fbLogin.bind(this)
        this.gLogin=this.gLogin.bind(this)
        this.sendAccessToken=this.sendAccessToken.bind(this)
        this.sessionCreator=this.sessionCreator.bind(this)
    }
    
    ontype(e){
        var id = e.target.id
        var value = e.target.value
        this.setState({ data: { ...this.state.data, [id]: value }, message: {text:'', color:''} })
    }
    onCheck(){
        const { data } = this.state
        if(data.em === '' || data.ps === '')
            this.setState({ message: { text: 'Complete fields correctly', color: 'red' } })
        else
            this.onLogin()

        setTimeout(() => { this.setState({ message: {text:'', color:''} }) }, 3000);
    }
    onLogin(){
        const { data } = this.state
        const params = { email: data.em, password: data.ps  }
        this.setState({ btnLoginDisabled: true })
        api.post('/login', params, (res) => {
            //console.log('res: ', res)
            //alert(`Res: ${JSON.stringify(res)}`)
            if(res.data){
                this.setState({ message: { text: 'Correct successfulll', color: 'green' } })
                const data = res.data.data
                this.sessionCreator(data)
            }else {
                this.setState({ message: { text: 'Incorrect user or password', color: 'red' }, btnLoginDisabled: false })
            }
            //this.setState({ btnLoginDisabled: false })
        })
    }
    fbLogin(fbres){
        console.log('fb res: ', fbres)
        if(fbres){
            const accessToken = fbres.accessToken
            if(accessToken)
                this.sendAccessToken(accessToken, 'facebook')
        }
    }
    gLogin(gres){
        console.log('google res: ', gres)
        if(gres){
            const accessToken = gres.accessToken
            if(accessToken)
                this.sendAccessToken(accessToken, 'google')
        }
    }
    sendAccessToken(accessToken, type){
        var endpoint = type === 'facebook' ? '/users/signin-facebook' : '/users/signin-google'
        api.post(endpoint, {accessToken}, (res) => {
            console.log('res back: ', res)
            if(res.data){
                if(res.data.status === 'success'){
                    const data = res.data.data
                    this.sessionCreator(data)
                } else { alert('ERROR: Login error, try again') }
            }
        })
    }
    sessionCreator(data){
        const token = { 
            "X-Auth-Token": data.authToken ? data.authToken : data.token,
            "X-User-Id": data.userId
        }
        const localToken = JSON.stringify(token)
        localStorage.setItem(process.env.REACT_APP_LOCAL_TOKEN, localToken)
        localStorage.setItem(process.env.REACT_APP_LOCAL_UID, data.userId)
        setTimeout(() => {
            window.location.href = '/profile'
            //window.history.back()
            //this.props.history.push('/profile')
        }, 1000);
    }
    sessionChecker(){
        var userOn = localStorage.getItem(process.env.REACT_APP_LOCAL_TOKEN)
        if(userOn)
            this.props.history.push('/profile')
    }
    enableEnter(){
        $('.auth_form').keypress( (e) => {
            var key = e.which;
            if(key == 13){
                this.onCheck()
                return false;  
            }
        }); 
    }

    componentDidMount(){
        this.sessionChecker()
        this.enableEnter()
        //$('html, body').animate({ scrollTop: 100 }, );
    }
    render(){
        const { message, btnLoginDisabled, remember } = this.state
        //console.log(this.state)
        const navOptions = [{text:'Home', route:'/'}, {text:'Account', route:'/auth'}]
        return(
            <div className='auth'>
                <div className='auth_nav-small'><NavSmall options={navOptions} /></div>
                <div className='auth_form'>
                    <span style={{fontFamily: 'Gotham-Book', color: message.color }} >{message.text}</span>
                    <h2>LOG IN</h2>
                    <span className='auth_form_subtitle'>LOGIN WITH EMAIL</span>
                    <div className='auth_form_email'>
                        <label htmlFor='em'>EMAIL</label>
                        <input type="email" id='em' onChange={(e)=>this.ontype(e)} />
                    </div>
                    <div className='auth_form_password'>
                        <div className='auth_form_password_label'>
                            <label htmlFor='ps'>PASSWORD</label>
                            <span>FORGOT YOUR PASSWORD?</span>
                        </div>
                        <input type="password" id='ps' onChange={(e)=>this.ontype(e)} />
                    </div>
                    <div className='auth_form_access'>
                        <div className='auth_form_access_remember' onClick={()=>this.setState({ remember: !remember })}>
                            <div className='auth_form_access_remember_check'>{remember?<i className='material-icons'>done</i>:null}</div>
                            <span>REMEMBER ME</span>
                        </div>
                        <button id='login-btn' disabled={btnLoginDisabled} onClick={this.onCheck}>SIGN IN</button>
                    </div>
                    {/* <div className='auth_form_or'>
                        <div/>
                        <span>OR</span>
                        <div/>
                    </div>
                    <div className='auth_form_social-access'>
                        <button className='auth_form_social-access_facebook'/>
                        <button className='auth_form_social-access_google'/>
                    </div> */}
                    <div className='auth_form_social-login'>
                        <label>LOGIN WITH</label>
                        <div className='auth_form_social-login_buttons'>
                            <FacebookLogin
                                appId='1284225565049629'
                                autoLoad={false}
                                fields={'name,email,picture'}
                                callback={this.fbLogin}
                                cssClass='auth_form_social-login_buttons_facebook'
                                textButton=''
                            />
                            {/* <button className='auth_form_social-login_buttons_facebook'/> */}
                            <GoogleLogin
                                clientId="340779959209-012bva4ia2j5r0q6ia376np814bervg8.apps.googleusercontent.com"
                                buttonText="Login"
                                onSuccess={this.gLogin}
                                onFailure={this.gLogin}
                                cookiePolicy={'single_host_origin'}
                                render={renderProps => (
                                    <button className='auth_form_social-login_buttons_google'
                                        onClick={renderProps.onClick} disabled={renderProps.disabled}>
                                    </button>
                                )}
                            />
                            {/* <button className='auth_form_social-login_buttons_google'/> */}
                        </div>
                    </div>
                </div>
                <div className='auth_go-sign-up'>
                    {/* DON'T HAVE AN ACCOUNT? <b><Link to='/auth/register'>SIGN UP NOW</Link></b> */}
                    <h2>REGISTER</h2>
                    <span>ENJOY THE BENEFITS OF HAVING AN ACCOUNT</span>
                    <button onClick={()=>this.props.history.push('/auth/register')}>CREATE AN ACCOUNT</button>
                </div>
            </div>
        )
    }
}

export default withRouter(Auth)