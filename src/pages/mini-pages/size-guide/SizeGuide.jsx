import React, { Component } from 'react'
import './SizeGuide.scss'
import { api } from '../../../core/helpers/api';

class SizeGuide extends Component{
    getSizeGuide(){
        try {
            const conatainerID = 'size-guide_container'
            api.get('/footer?code=SIZE_GUIDE', '', (res) => {
                if(res)
                    if(res.data.status === 'success')
                        document.getElementById(conatainerID).innerHTML = res.data.data.content
                    else
                        document.getElementById(conatainerID).innerText = 'No content'
            })
        } catch (error) {
            
        }
    }
    componentDidMount(){
        this.getSizeGuide()
    }
    render(){
        return(
            <div className='size-guide'>
                <div className='size-guide_container' id='size-guide_container'></div>
            </div>
        )
    }
}

export default SizeGuide