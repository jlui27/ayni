import React, { Component } from 'react'
import './TermsConditions.scss'
import { api } from '../../../core/helpers/api';

class TermsConditions extends Component{
    getTerms(){
        try {
            const conatainerID = 'terms-conditions_container'
            api.get('/footer?code=TERMS_CONDITIONS', '', (res) => {
                if(res)
                    if(res.data.status === 'success')
                        document.getElementById(conatainerID).innerHTML = res.data.data.content
                    else
                        document.getElementById(conatainerID).innerText = 'No content'
            })
        } catch (error) {
            
        }
    }
    componentDidMount(){
        this.getTerms()
    }
    render(){
        return(
            <div className='terms-conditions_container' id='terms-conditions_container'></div>
        )
    }
}

export default TermsConditions