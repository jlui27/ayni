import React, { Component } from 'react'
import './PaymentMethods.scss'
import { api } from '../../../core/helpers/api';

class PaymentMethods extends Component{
    getMethods(){
        const containerID = 'payment-methods_container'
        api.get('/footer?code=PAYMENT_METHODS', '', (res) => {
            console.log(res)
            if(res)
                if(res.data.status === 'success')
                    document.getElementById(containerID).innerHTML = res.data.data.content
                else
                    document.getElementById(containerID).innerText = 'No content'
        })
    }
    componentDidMount(){
        this.getMethods()
    }
    render(){
        return(
            <div className='payment-methods'>
                <div className='payment-methods_container' id='payment-methods_container'></div>
            </div>
        )
    }
}

export default PaymentMethods