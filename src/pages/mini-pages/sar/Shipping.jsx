import React from 'react'
import './Shipping.scss'
import NavSmall from '../../../components/menu/nav-small/NavSmall';
import { api } from '../../../core/helpers/api';

class Shipping extends React.Component{
    getData(){
        api.get('/footer?code=', 'SHIPPING_RETURNS', (res) => {
            if(res.data){
                const data = res.data.data
                if(data){
                    document.getElementById('shipContent').innerHTML = data.content
                }
                    
            }
        })
    }

    componentDidMount(){
        this.getData()
    }
    render(){
        const options = [
            {text: 'Home', route: '/'},
            {text: 'Shipping & Returns', route: '/shipping-returns'}
        ]
        return(
            <div className='shipping'>
                <div className='shipping_nav-small'>
                    <NavSmall options={options} />
                </div>
                <div className='shipping_content' id='shipContent'>
                    
                </div>
            </div>
        )
    }
}

export default Shipping;