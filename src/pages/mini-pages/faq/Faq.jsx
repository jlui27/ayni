import React from 'react'
import './Faq.scss'
import NavSmall from '../../../components/menu/nav-small/NavSmall';
import { api } from '../../../core/helpers/api';

class Faq extends React.Component{
    constructor(){
        super()
        this.state = {
            showing: -1,
            faqs: []
        }
        this.onShow=this.onShow.bind(this)
    }
    onShow(i){
        const { showing} = this.state
        if(i === showing)
            this.setState({ showing: -1 })
        else
            this.setState({ showing: i })
    }
    getFaq(){
        api.get('/faqs', [], (res) => {
            if(res.data){
                const data = res.data.data
                if(data)
                    this.setState({ ...this.state, faqs: data })
            }  
        })
    }
    componentDidMount(){
        this.getFaq()
    }
    render(){
        const { showing, faqs } = this.state
        const plusButton = { backgroundImage: `url(${process.env.REACT_APP_URL_BASE}/images/icons/mat-icons/plus.png)`}
        const minusButton = { backgroundImage: `url(${process.env.REACT_APP_URL_BASE}/images/icons/mat-icons/minus.png)`}
        const faqsSize = faqs.length
        const options = [
            { text: 'Home', route: '/' },
            { text: 'FAQ', route: '/faq' }
        ]
        return(
            <div className='faq'>
                <div className='faq_nav-small'>
                    <NavSmall options={options} />
                </div>
                <div className='faq_sir'>
                    <div className='faq_sir_title'>Frequent Questions</div>
                    <div className='faq_sir_container'>
                        {faqs.map((data, i) => {
                            return(
                                <div className='faq_sir_container_content' key={i} style={i+1<faqsSize?{borderBottom: 'rgba(112, 112, 112, 0.4) 1px solid'}:null} >
                                    <div className='faq_sir_container_content_header' onClick={()=>this.onShow(i)}>
                                        <span>{data.question}</span>
                                        <button style={showing===i?minusButton:plusButton} />
                                    </div>
                                    <div className='faq_sir_container_content_body' >
                                        <hr style={showing!==i?{display:'none'}:null} />
                                        <p style={showing!==i?{fontSize:'0px'}:null}>{data.answer}</p>
                                    </div>
                                </div>
                            )
                        })}
                    </div>
                </div>
            </div>
        )
    }
}

export default Faq;