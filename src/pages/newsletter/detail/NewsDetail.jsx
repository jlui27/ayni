import React from 'react'
import './NewsDetail.scss'
import { api } from '../../../core/helpers/api';
import { connect } from 'react-redux';
import sharon from 'sharon'
import { onNewsModal } from '../../../core/store/actions/news';
import $ from 'jquery'
import { withRouter } from 'react-router-dom';

class NewsDetail extends React.Component{
    constructor(){
        super()
        this.state={
            newsData: []
        }
        this.closeModal=this.closeModal.bind(this)
        this.social = ['/images/icons/hearth.svg', '/images/icons/Facebook.svg', '/images/icons/twitter.svg']
    }
    closeModal(){
        //document.getElementById('id01').style.display='none'
        this.props.onSetNewsModal(null) // hide redux component
        //window.history.pushState("", document.title, `${process.env.REACT_APP_URL_BASE}/behind-the-brand`)
        this.props.history.push('/behind-the-brand')
    }
    getNewsData(id){
        if(id){
            api.get('/blogs/', id, (res) => {
                if(res.data.data){
                    this.setState({ newsData: res.data.data })
                    document.getElementById('news-data-body').innerHTML=res.data.data.content
                }
            })
        }
    }
    scrollTop(){
       $('.w3-modal').animate({ scrollTop: 0 }, );
    }
    sharing(title){
        // this function is not working for now
        sharon.twitter({
            title: title,
            hashtags: ['Moda', 'aynishop']
        })
    }
    componentDidMount(){
        const { id } = this.props
        const { newsData } = this.state
        console.log(id)
        if(newsData.id !== id)
            this.getNewsData(id)
       // console.log(id)
       // this.getNewsData(2)
    }
    /*
    componentDidUpdate(){
        const { id } = this.props
        const { newsData } = this.state
        console.log(id)
        if(newsData.id !== id)
            this.getNewsData(id)
    }*/
    render(){
        const { newsData } = this.state
        //console.log(newsData)
        return(
            <div className='news-detail' id='news-detail'>
                <div className="w3-container">
                {/* <h2>W3.CSS Modal</h2>
                <button onClick={this.openModal} className="w3-button w3-black">Open Modal</button> */}

                    <div id="id01" className="w3-modal">
                        <div className="w3-modal-content">
                            <div className="w3-container">
                                {/* <span onClick={this.closeModal} className="w3-button w3-display-topright">&times;</span> */}
                                <div className='w3-container_left'>
                                    <div className='w3-container_left_social' style={{display:'none'}}>
                                        {this.social.map((img, i) => {
                                            return(
                                                <button key={i} style={{backgroundImage: `url(${process.env.REACT_APP_URL_BASE}${img})`}} />
                                            )
                                        })} 
                                    </div>
                                    <div className='w3-container_left_arrow-up' onClick={this.scrollTop} />
                                </div>
                                <div className='w3-container_center'>
                                    <div className='w3-container_center_about'>Moda</div>
                                    <div className='w3-container_center_title'>{newsData.title}</div>
                                    <div className='w3-container_center_date'>{new Date(newsData.publicated).toDateString()}</div>
                                    <div className='w3-container_center_body' id='news-data-body'></div>
                                </div>
                                <div className='w3-container_right'>
                                    <button className='w3-container_right_close-btn' onClick={this.closeModal} />
                                    <button className='w3-container_right_to-up-btn' onClick={this.scrollTop} />
                                </div>
                            </div>
                            <div className='w3-modal-content_close'>
                                <button onClick={this.closeModal}>Cerrar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return { id: state.newsLetter.id }
}
const mapDispatchToProps = dispatch => {
    return{
        onSetNewsModal(component){
            dispatch(onNewsModal(component))
        }
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(NewsDetail))