import React from 'react'
import './NewsMain.scss'
import { api } from '../../../core/helpers/api';
import NewsDetail from '../detail/NewsDetail';
import { setId, onNewsModal } from '../../../core/store/actions/news';
import { connect } from 'react-redux';
import NavSmall from '../../../components/menu/nav-small/NavSmall';
import { withRouter } from 'react-router-dom';
import $ from 'jquery'
import { isNumber } from 'util';

class NewsMain extends React.Component{
    constructor(){
        super()
        this.state={
            selected: 0,
            newsList: [], newsDetComponenet: null
        }
        this.changeSection=this.changeSection.bind(this)
        //this.sections = ['ALL', 'NEWS', 'FASHION', 'CELEBRITIES', 'LETTER', 'TRAVEL', 'OTHERS']
        this.sections = ['ALL','SUSTAINABILITY','AYNI CERTIFY','MATERIALS','PRESS','NEWS']
        //this.sections = []
    }
    changeSection(i){
        const { selected } = this.state
        if(i!==selected)
            this.setState({ selected: i })
    }
    getNews(){
        api.get('/blogs', [], (res) => {
            if(res)
                if(res.data)
                    this.setState({ newsList: res.data.data })
        })
    }
    openModal(id){
        const { setNewsLetterId, onSetNewsModal } = this.props

        //this.setState({ newsDetComponenet: <NewsDetail/> })
        //document.getElementById('id01').style.display='block'
        
        this.props.history.push(`/behind-the-brand/${id}`)
        setNewsLetterId(id)
        onSetNewsModal( <NewsDetail/> ) // set component in redux
        //window.history.pushState("", document.title, `${process.env.REACT_APP_URL_BASE}/behind-the-brand/`+id)
    }
    enaDisaBehindScroll(){
        var news_id = parseInt(this.props.match.params.id)
        if(news_id)
            if(isNumber(news_id))
                $('html, body').css({ overflow: 'hidden', height: '100%' }) // disable
        else
            $('html').css({ overflow: 'auto', height: 'auto' }) // enable
    }

    componentDidMount(){
        this.getNews()
        const { match } = this.props
        if(match.params.id > 0)
            this.openModal(match.params.id)
    }
    componentDidUpdate(){
        this.enaDisaBehindScroll()
    }
    render(){
        const { selected, newsList } = this.state
        const testPortrait = 'https://i.imgur.com/Vc9adBM.jpg'
        const testDefaultImage = `${process.env.REACT_APP_URL_BASE}/images/fixed/blog-default-image.png`
        const selectedStyle = { borderBottom: '1.1px black solid', fontWeight: 'bold' }
        //console.log('News List: ', newsList)
        const navOptions = [{text:'Home', route:'/'}, {text:'Behind The Brand', route:'/behind-the-brand'}]
        return(
            <div className='news-main'>
                <div className='news-main_nav-small'><NavSmall options={navOptions}/></div>
                <div className='news-main_portrait' style={{backgroundImage: `url(${testPortrait})`}}>
                    <div className='news-main_portrait_layer'>
                        <div className='news-main_portrait_layer_content'>
                            <div className='news-main_portrait_layer_content_text'>MODA</div>
                            <div className='news-main_portrait_layer_content_title'>Superhype 2016</div>
                            <div className='news-main_portrait_layer_content_description'>
                                Una reseña increíble sobre un estilo completamente diferente y en tendencia . . .
                            </div>
                        </div>
                    </div>
                    
                </div>
                {/* Menu */}
                <div className='news-main_menu'>
                    {this.sections.map((text, i) => {
                        return(
                            <div key={i}>
                                <span style={selected===i?selectedStyle:null} onClick={()=>this.changeSection(i)}>{text}</span>
                            </div>
                        )
                    })}
                </div>
                {/* Content */}
                <div className='news-main_body'>
                    {newsList?newsList.map((data, i) => {
                        // Todas las categorias
                        if(selected === 0){
                            return(
                                <div className='news-main_body_content' key={i} onClick={()=>this.openModal(data.id)}>
                                    <img src={data.preview_url?data.preview_url:testDefaultImage} alt=""/>
                                    <div className='news-main_body_content_smalltext'>
                                        <div className='news-main_body_content_smalltext_about'>Moda</div>
                                        <div className='news-main_body_content_smalltext_date'>{new Date(data.publicated).toDateString()}</div>
                                    </div>
                                    <h1>{data.title}</h1>
                                    <span id='news-list-description'>{data.short_content}</span>
                                </div>
                            )
                        }
                        // Por categoría
                        else if((data.categories).toUpperCase()===this.sections[selected]){
                            console.log('DATA: ', data)
                            return(
                                <div className='news-main_body_content' key={i} onClick={()=>this.openModal(data.id)}>
                                    <img src={testDefaultImage} alt=""/>
                                    <div className='news-main_body_content_smalltext'>
                                        <div className='news-main_body_content_smalltext_about'>Moda</div>
                                        <div className='news-main_body_content_smalltext_date'>{new Date(data.publicated).toDateString()}</div>
                                    </div>
                                    <h1>{data.title}</h1>
                                    <span id='news-list-description'>{data.short_content}</span>
                                </div>
                            )
                        }
                    }):false}
                </div>
                {/* <NewsDetail/> */}
                {this.props.detailNewsComponent}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return{
        detailNewsComponent: state.newsLetter.detComponent
    }
}
const mapDispatchToProps = dispatch => {
    return{
        setNewsLetterId(id){
            dispatch(setId(id))
        },
        onSetNewsModal(component){
            dispatch(onNewsModal(component))
        }
    }
}
 
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(NewsMain))