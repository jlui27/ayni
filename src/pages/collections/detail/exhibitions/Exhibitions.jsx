import React from 'react'
import './Exhibitions.scss'
import Swiper from 'react-id-swiper';
import { getVideoId } from '../../../../core/helpers/formats';

class Exhibitions extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            collData: [], video: ''
        }
        this.swiperParams = {
            slidesPerView: 2,
            centeredSlides: true,
            loop: false,
            pagination: {
              el: '.swiper-pagination',
              type: 'bullets',
              clickable: true
            }
          }
    }

    componentDidMount(){
        const { collectionData } = this.props
        if(collectionData){
            this.setState({ collData: collectionData })
        }
    }
    render(){
        const { collData } = this.props
        var backstageContent = ''
        if(collData){
            for (let i = 0; i < collData.images.length; i++) {
                if(collData.images[i].type === 'BACKSTAGE'){
                    const element = collData.images[i].path;
                    const yt = element.split('/')[2] 
                    if(yt  === 'www.youtube.com'){
                        backstageContent = element
                    }
                }
            }
        }
            console.log('Coll Data: ', collData.images, 'video:', backstageContent)
            const testImages = [
                'http://shuttermike.com/wp-content/uploads/2010/02/Sedona-Vertical.jpg',
                'http://www.naturephotographysimplified.com/wp-content/uploads/2014/10/Nature-Photography-Landscape-Photogarphy-Vertical-Composition-Fall-Foliage-Road-To-Agate-Falls.jpg',
                'http://www.naturephotographysimplified.com/wp-content/uploads/2014/10/Nature-Photography-Simplified-Landscape-Photography-Vertical-Composition-Technique-Waterfalls-In-Blue-Ginger-Resort-Wayanad.jpg',
                'http://cdn3.craftsy.com/a/general/wp-content/uploads/2015/01/silver-cascade-autumn-2279.jpg'
            ]
        return(
            <div className='exhibitions'>
                <div className='exhibitions_lookbook'>
                    {collData.images?collData.images.map((data, i) => {
                        if(data.type==='EXHIBITION')
                            return(
                                <div className='exhibitions_lookbook_content' key={i}>
                                    <img src={data.path} alt=""/>
                                </div>
                            )
                    }):null}
                </div>
                {backstageContent?
                    <div className='exhibitions_video'>
                        {/* <iframe width="100%" height="100%" src={"https://www.youtube.com/embed/"+this.state.videoID} frameBorder="0" allow="autoplay; encrypted-media" allowFullScreen></iframe> */}
                        <iframe width="100%" height="100%" src={`https://www.youtube.com/embed/${getVideoId(backstageContent)}`} frameBorder="0" allow="autoplay; encrypted-media" allowFullScreen></iframe>
                    </div>
                :null}
                <div className='exhibitions_backstage'>
                    <div className='exhibitions_backstage_title'>FASHION SHOW BACKSTAGE</div>
                    <div className='exhibitions_backstage_container'>
                        {collData?
                        <Swiper {...this.swiperParams} ref={(node) => { if (node) this.swiper = node.swiper; }}>
                            { collData.images && collData.images.length > 0?collData.images.map( (data,index)=>{
                                //const image = data.substring(0,5) !== 'https' ? data : 'https://previews.123rf.com/images/elen/elen1111/elen111100108/11121687-beautiful-nature-panoramic-scenery-small-river-and-evening.jpg'
                                if(data.type === 'BACKSTAGE'){
                                    const pathName = data.path.split('/')[2]
                                    if(pathName === '167.99.236.225'){
                                        //console.log('.............',data.path)
                                        return (
                                            <div className='exhibitions_backstage_container_content'>
                                                <img className='exhibitions_backstage_container_content_image' key={index} src={data.path} alt=""/>
                                            </div>
                                        )
                                    }
                                }
                            }) : null }
                        </Swiper>
                        : null}
                    </div>
                </div>
            </div>
        )
    }
}

export default Exhibitions