import React from 'react'
import './Campaign.scss'

class Campaign extends React.Component{
    setContent(){
        try {
            const { campaignData } = this.props
            if(campaignData !== null || campaignData === ''){
                document.getElementById('camp-content').innerHTML = campaignData
            }
        } catch (error) {
            console.log(error)
        }
    }
    componentDidUpdate(){
        
    }
    render(){
        const { campaignData } = this.props
        setTimeout(() => {
            this.setContent()
        }, 1500); 
        //console.log('Campaign data from campaign', campaignData)
        return(
            <div className='campaign'>
                {campaignData?
                <div className='campaign_content' id='camp-content'></div>
                :<div className='campaign_nodata'>No content</div>}
            </div>
        )
    }
}

export default Campaign