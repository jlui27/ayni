import React from 'react'
import './DetailCollections.scss'
import { api } from '../../../core/helpers/api';
import Lookbook from './lookbook/Lookbook';
import Campaign from './campaign/Campaign';
import Exhibitions from './exhibitions/Exhibitions';
import $ from 'jquery'
import NavSmall from '../../../components/menu/nav-small/NavSmall';
import { setLookbookData } from '../../../core/store/actions/header';
import { connect } from 'react-redux';

class DetailCollection extends React.Component{
    constructor(){
        super()
        this.state = {
            detailData: [],
            onSection: 0,
            exhibitionsList: []
        }
        this.changeSection=this.changeSection.bind(this)
        this.sections = ['CAMPAIGN', 'LOOKBOOK', 'NYFW']
        this.sectionsNotExhibitions = ['CAMPAIGN', 'LOOKBOOK']
    }
    getDetail(){
        const { match, setLookbookModalData } = this.props
        api.get('/collections/', match.params.id, (res) => {
            if(res.data){
                const data = res.data.data
                this.setState({ detailData: data, onSection: 0 })
                this.checkExhibitions(data)
                if(data.images)
                    setLookbookModalData(data.images) // set to modal trought redux
            }
        })
    }
    changeSection(i){
        const { onSection } = this.state
        if(onSection!==i){
            this.setState({ onSection: i })
            $('html, body').animate({ scrollTop: 100 }, );
        }  
    }
    showSection(){
        const { detailData, onSection } = this.state
        switch(onSection){
            case 0: return <Campaign campaignData={detailData?detailData.campaign_content:null} />; break;
            case 1: return <Lookbook dataList={detailData?detailData.images:null} />; break;
            case 2: return <Exhibitions collData={detailData?detailData:null} />; break;
            default: break;
        }
    }
    checkExhibitions(collection){
        //console.log('Current collections: ', collection.images.type)
        var exhibitionsList = []
        for (let i = 0; i < collection.images.length; i++) {
            const element = collection.images[i].type;
            exhibitionsList.push(element)
        }
        this.setState({ exhibitionsList: exhibitionsList })
    }

    componentDidMount(){
        this.getDetail()
        //this.checkExhibitions()
    }
    componentDidUpdate(){
        const urlId = parseInt(this.props.match.params.id)
        if(urlId !== this.state.detailData.id)
            this.getDetail()
    }
    render(){
        const { detailData, onSection, exhibitionsList } = this.state
        const selected = { fontWeight: 'bold' }
        //console.log('collection data: ', detailData, 'Exhibitions list: ', exhibitionsList)
        const navOptions = [{text: 'Home', route: '/'}, {text: 'Collections', route: ''}]
        return(
            <div className='detail-collection' id='collsection'>
                <div className='detail-collection_nav-small'><NavSmall options={navOptions} /></div>
                <div className='detail-collection_title'>{detailData?detailData.name:'Not found'}</div>
                <div className='detail-collection_sections'>
                    <div className='detail-collection_sections_container'>
                        {exhibitionsList.indexOf('EXHIBITION') > -1 ? this.sections.map((name, i) => {
                            return(
                                <div className='detail-collection_sections_container_content' key={i}>
                                    <span style={onSection===i?selected:null} onClick={()=>this.changeSection(i)}>{name}</span>
                                </div>
                            )
                        }) :
                            this.sectionsNotExhibitions.map((name, i) => {
                                return(
                                    <div className='detail-collection_sections_container_content' key={i}>
                                        <span style={onSection===i?selected:null} onClick={()=>this.changeSection(i)}>{name}</span>
                                    </div>
                                )
                            })
                        }
                    </div>
                </div>
                {/* Body */}
                {/* <Lookbook dataList={detailData?detailData.images:null} /> */}
                    {this.showSection()}
                {/* End body */}
                
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return{
        setLookbookModalData(data){
            dispatch(setLookbookData(data))
        }
    }
}

export default connect(null, mapDispatchToProps)(DetailCollection)