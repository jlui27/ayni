import React, { Fragment } from 'react'
import './Lookbook.scss'
import { displayLookbookModal, setLookbookIndex } from '../../../../core/store/actions/header';
import { connect } from 'react-redux';
import Lightbox from 'react-image-lightbox';
import 'react-image-lightbox/style.css'; 

const title =()=>{

    return(
        <div>Hello</div>
    )
}

class Lookbook extends React.Component{
    constructor(){
        super()
        this.state = {
            isMobile: false,
            img:'',
            title_img : '',
            photoIndex: 0,
            isOpen: false,
            boxImage:null
        }
        this.title1='';
        this.title2='';
        this.title3='';
        this.title4='';
        this.title = [];
        this.containerImg=[];
        this.mediaQueryExecute=this.mediaQueryExecute.bind(this);
        this.modalImg=this.modalImg.bind(this);
        this.nextImg = this.nextImg.bind(this);
    }

    nextImg(photoIndex){
        this.title1 = this.containerImg[photoIndex].name
       return this.containerImg[(photoIndex + 1) % this.containerImg.length].path
    }
    onModal(exec, index){
        const { onDisplayModal, onModalIndex } = this.props
        onDisplayModal(exec)
        onModalIndex(index)
    }
    mediaQueryExecute(x) {
        if (x.matches) { // If media query matches
            this.setState({ isMobile: true, isOpen:false})
        } else {
            this.setState({ isMobile: false })
        }
    }
    mediaQueryChecker(){
        var x = window.matchMedia("(max-width: 800px)")
        this.mediaQueryExecute(x) // Call listener function at run time
        x.addListener(this.mediaQueryExecute) // Attach listener function on state changes
    }
    modalImg(){
        console.log('click')
    }

    openModal(i,name) {
        this.setState({ isOpen: true ,photoIndex:i,title_img:name})
        //console.log('data received: ', name)
      }
     
      afterOpenModal() {
        // this.subtitle.style.color = '#f00';
      }
     
      closeModal() {
        this.setState({modalIsOpen: false});
      }

    componentDidMount(){
        this.mediaQueryChecker();
        this.props.dataList.map((val,index)=>{
            if(val.type==='LOOKBOOK'){
                            this.containerImg.push({path:val.path,name:val.name})
            }
        })
        //this.setState({boxImage:this.containerImg})
    }
    render(){
        this.title = [] 
        const { isMobile,photoIndex, isOpen } = this.state
        if( this.containerImg.length > 0){
            (this.containerImg[photoIndex].name).split('/').map((val,key)=>{
                this.title[key]=val
            })
        }
      
        if(this.title.length > 0){
            var reactElementUl = React.createElement(
                'div', {
                    className: 'myList'
                },
                    React.createElement('div', {id: 'div1'},
                    React.createElement('div', {id: 'title'},this.title[0]?this.title[0].split(" ")[0]:''),
                    React.createElement('div', {id: 'txt'},this.title[0]?this.title[0].split(" ")[1]:''),
                    ),
                    React.createElement('div', {id: 'div2'},
                    React.createElement('div', {id: 'title'},this.title[1]?this.title[1].split(" ")[1]:''),
                    React.createElement('div', {id: 'txt'},this.title[1]?this.title[1].split(" ")[2]:''),
                    ),
                    React.createElement('div', {id: 'div3'},
                    React.createElement('div', {id: 'title'},this.title[2]?this.title[2].split(" ")[1]:''),
                    React.createElement('div', {id: 'txt'},this.title[2]?this.title[2].split(" ")[2]:''),
                    )
    
            );
    
        
        }
       
        
        return(
            
            <div className='lookbook'>
                {this.containerImg.length > 0?this.containerImg.map((data, i) => {
                    if(this.containerImg !=null)
                        /*return(
                            
                            <div className='lookbook_content'  onClick={()=>!isMobile?  this.openModal(i,data.name) : this.onModal(true, i) }key={i}>
                                <img src={data.path} alt="" onClick={()=>this.modalImg()}/>
                            </div>
                        )*/
                        return(
                            <div className='lookbook_content' onClick={()=>this.openModal(i, data.name)} key={i}>
                                <img src={data.path} alt=""/>
                            </div>
                        )
                }):<div className='lookbook_nodata'>No content</div>}
 
        {isOpen && (
          <Lightbox
            enableZoom={false}
            imagePadding={42}
            imageCaption={reactElementUl}
            mainSrc={this.containerImg[photoIndex].path}
            nextSrc={this.nextImg(this.state.photoIndex)}
            prevSrc={this.containerImg[(photoIndex + this.containerImg.length - 1) % this.containerImg.length].path}
            onCloseRequest={() => this.setState({ isOpen: false })}
            onMovePrevRequest={() =>
              this.setState({
                photoIndex: (photoIndex + this.containerImg.length - 1) % this.containerImg.length,
              })
            }
            onMoveNextRequest={() =>
              this.setState({
                photoIndex: (photoIndex + 1) % this.containerImg.length,
              })
            }
          />
        )}
      
    </div>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return{
        onDisplayModal(exec){ dispatch(displayLookbookModal(exec)) },
        onModalIndex(index){ dispatch(setLookbookIndex(index)) }
    }
}

export default connect(null, mapDispatchToProps)(Lookbook)
