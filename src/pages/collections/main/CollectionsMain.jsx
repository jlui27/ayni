import React from 'react'
import './CollectionsMain.scss'

class CollectionsMain extends React.Component{
    render(){
        const list = [
            {name: 'NYFW / 19', image: 'https://cdn.glamour.mx/uploads/images/thumbs/mx/glam/2/s/2017/19/olanes_en_la_moda_893158530_1800x1200.jpg'},
            {name: 'SPRING SUMMER / 18', image: 'https://image.afcdn.com/story/20180108/-1141060_w767h767c1cx396cy260.jpg'},
            {name: 'FALL WINTER / 18', image: 'http://thepiltrafastv.com/wp-content/uploads/2016/03/moda.jpg'},
            {name: 'RESORT / 18', image: 'https://www.istitutomarangoni.com/wp-content/uploads/2017/01/1-GABRIELLA-ABULEAC-2015.jpg'},
        ]
        return(
            <div className='collections-main'>
                <div className='collections-main_container'>
                    {list.map((data, i) => {
                        return(
                            <div className='collections-main_container_content' key={i} style={{backgroundImage: `url(${data.image})`}}>
                                <span>{data.name}</span>
                            </div>
                        )
                    })}
                </div>
            </div>
        )
    }
}

export default CollectionsMain