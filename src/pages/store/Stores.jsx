import React from 'react'
import './Stores.scss'

import { Map, InfoWindow, Marker, GoogleApiWrapper } from 'google-maps-react'
import StoresMobile from './mobile/StoresMobile';

class Stores extends React.Component{
    constructor(){
        super()
        this.state={
            storesData: [],
            selected: 0,
            showingInfoWindow: false,
            activeMarker: {},
            selectedPlace: {},
        }
        this.changeStore=this.changeStore.bind(this)
        /*
        this.locations = [
            {name: 'JOCKEY PLAZA', address: 'Primer Nivel - Barrio Jockey, Tienda N° 63 CC JOCKEY PLAZA',
            location: `https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4639.534938581695!2d-76.97782774842294!3d-12.
            086919908202471!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105c7b26eae302b%3A0x8b6249a5b3a9f027!2sAYNI
            +-+Jockey+Plaza!5e0!3m2!1ses-419!2spe!4v1542934438272`},

            {name: 'MIRAFLORES', address: 'Jr. Gonzales Prada Nro. 355 Dpto. 204 (Costado del Teatro Marsano)',
            location: `https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2758.3815874926213!2d-77.02669279528664!3d-12.
            116213315291933!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105c810d8335d6d%3A0xbc884dd93678607f!2sDpto.
            +204%2C+Jr.+Gonzales+Prada+355%2C+Distrito+de+Lima+15074!5e0!3m2!1ses-419!2spe!4v1543016502294`},
        ]
        */
    }
    async getStores(){
        const reqStores = await fetch('http://167.99.236.225:30/api/stores')
        const resData = await reqStores.json()
        if(resData)
            this.setState({ storesData: resData })
    }
    changeStore(i){
        const { storesData } = this.state
        const { preview_type, url } = storesData.data[i]
        this.setState({ selected: i, showingInfoWindow: false })
        if(preview_type === 'IMAGE')
            this.goToWebsite(url)
    }
    onMarkerClick = (props, marker, e) => {
        this.setState({
            selectedPlace: props,
            activeMarker: marker,
            showingInfoWindow: true
          })
    }
    onMapClicked = (props) => {
        if (this.state.showingInfoWindow) {
            this.setState({
              showingInfoWindow: false,
              activeMarker: null
            })
        }
    }
    goToWebsite(url){
        const goLink = !url.match(/^[a-zA-Z]+:\/\//)?`http://${url}`:url
        window.open(goLink)
    }

    componentDidMount(){
        this.getStores()
    }
    render(){
        const { storesData, selected, showingInfoWindow, activeMarker, selectedPlace } = this.state
        //console.log('store data: ', storesData)
        //console.log('seleccionado ahora: ', selected)
        const selStyle = { backgroundColor: '#686260', color: '#dfdede' }
        const base = process.env.REACT_APP_URL_BASE
        return(
            <div className='stores'>
                <h2>Stores</h2>
                <div className='stores_container'>
                    <div className="stores_container_left">
                        {/* <div className='stores_container_left_storein'>STORES IN <b>LIMA, PERÚ</b></div> */}
                        <div className='stores_container_left_locations'>
                            {storesData?storesData.status==='success'?(storesData.data).map((data, i) => {
                                const selectedStyle = selected === i ? selStyle : null
                                const icon = data.preview_type==='MAP'?`${base}/images/icons/location.png`:`${base}/images/icons/www.png`
                                return(
                                    <div className={`stores_container_left_locations_content`} style={selectedStyle} onClick={()=>this.changeStore(i)} key={i} >
                                        <div className={`stores_container_left_locations_content_logo`}>
                                            <div className={`stores_container_left_locations_content_logo_icon`} style={{backgroundImage: `url(${icon})`}}/>
                                        </div>
                                        <div className={`stores_container_left_locations_content_info`} style={selectedStyle} >
                                            <div className={`stores_container_left_locations_content_info_name`} style={selectedStyle}>{data.name}</div>
                                            <div className={`stores_container_left_locations_content_info_address`} style={selectedStyle}>{data.preview_type==='MAP'?data.url:null}</div>
                                            {/* <span onClick={()=>document.getElementById(`schedule${i}`).style.display = 'none'}>+ VER HORARIO</span> */}
                                            <div id={`schedule`} style={selectedStyle}>{data.schedule?data.schedule:null}</div>
                                        </div>
                                    </div>
                                )
                            }):null:null}
                        </div>
                    </div>
                    <div className="stores_container_right">
                        {/* <iframe src={currentLocation} title='Google maps'
                        frameBorder="0" style={{border: '0', width: '95%', height: '100%'}} ></iframe> */}
                        {storesData?storesData.status==='success'?storesData.data[selected].preview_type === 'MAP'?
                            <div className='stores_container_right_content'>
                                <Map google={this.props.google} zoom={15} initialCenter={{
                                    lat: storesData.data[selected].preview_latitude,
                                    lng: storesData.data[selected].preview_longitude
                                }} center={{
                                    lat: storesData.data[selected].preview_latitude,
                                    lng: storesData.data[selected].preview_longitude
                                }}
                                    onClick={this.onMapClicked}
                                >
                                    <Marker onClick={this.onMarkerClick}
                                        title={`Ayni's store`} 
                                        name={storesData.data[selected].name}
                                        position={{
                                            lat: storesData.data[selected].preview_latitude,
                                            lng: storesData.data[selected].preview_longitude
                                        }} />
                                    <InfoWindow onClose={this.onInfoWindowClose} 
                                        marker={activeMarker} 
                                        visible={showingInfoWindow}
                                    >
                                    <div>
                                        <h1>{selectedPlace.name}</h1>
                                    </div>
                                    </InfoWindow>
                                </Map>
                            </div>
                        :
                            <img src={storesData.data[selected].preview_image_path} 
                            onClick={()=>this.goToWebsite(storesData.data[selected].url)}  />
                        :null:null}
                    </div>
                </div>
                <StoresMobile storesData={storesData} />
            </div>
        )
    }
}

export default GoogleApiWrapper({
    apiKey: ('AIzaSyAbHOaFI5djya6RJoSrbJC6zB7e17KTETk')
})(Stores)


// source: https://www.npmjs.com/package/google-maps-react