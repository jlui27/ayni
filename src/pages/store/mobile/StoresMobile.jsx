import React, { Component} from 'react';
import './StoresMobile.scss'
import { Map, InfoWindow, Marker, GoogleApiWrapper } from 'google-maps-react'

class StoresMobile extends Component{
    constructor(){
        super()
        this.state = {
            storesData: [],
            selected: 0,
            showingInfoWindow: false,
            activeMarker: {},
            selectedPlace: {},
        }
        this.changeStore=this.changeStore.bind(this)
    }
    changeStore(i){
        const { storesData } = this.state
        const { preview_type, url } = storesData.data[i]
        this.setState({ selected: i, showingInfoWindow: false })
        if(preview_type === 'IMAGE')
            this.goToWebsite(url)
    }
    onMarkerClick = (props, marker, e) => {
        this.setState({
            selectedPlace: props,
            activeMarker: marker,
            showingInfoWindow: true
          })
    }
    onMapClicked = (props) => {
        if (this.state.showingInfoWindow) {
            this.setState({
              showingInfoWindow: false,
              activeMarker: null
            })
        }
    }
    goToWebsite(url){
        const goLink = !url.match(/^[a-zA-Z]+:\/\//)?`http://${url}`:url
        window.open(goLink)
    }

    componentWillReceiveProps(nextProps){
        const { storesData } = nextProps
        if(storesData)
            this.setState({ storesData })
    }
    render(){
        const { storesData, selected, showingInfoWindow, activeMarker, selectedPlace } = this.state
        const selStyle = { backgroundColor: '#686260', color: '#dfdede' }
        const prevActive = { display: 'block' }
        const base = process.env.REACT_APP_URL_BASE
        console.log('mobile store state: ', this.state)
        return(
            <div className='stores-mobile'>
                <div className='stores-mobile_container'>
                    {storesData?storesData.status==='success'?(storesData.data).map((data, i) => {
                        const selectedStyle = selected === i ? selStyle : null
                        const icon = data.preview_type==='MAP'?`${base}/images/icons/location.png`:`${base}/images/icons/www.png`
                        return(
                            <div className='stores-mobile_container_father' key={i}>
                                <div className={`stores-mobile_container_father_content`} style={selectedStyle} onClick={()=>this.changeStore(i)} >
                                    <div className={`stores-mobile_container_father_content_logo`}>
                                        <div className={`stores-mobile_container_father_content_logo_icon`} style={{backgroundImage: `url(${icon})`}}/>
                                    </div>
                                    <div className={`stores-mobile_container_father_content_info`} style={selectedStyle} >
                                        <div className={`stores-mobile_container_father_content_info_name`} style={selectedStyle}>{data.name}</div>
                                        <div className={`stores-mobile_container_father_content_info_address`} style={selectedStyle}>{data.preview_type==='MAP'?data.url:null}</div>
                                        {/* <span onClick={()=>document.getElementById(`schedule${i}`).style.display = 'none'}>+ VER HORARIO</span> */}
                                        <div id={`schedule`} style={selectedStyle}>{data.schedule?data.schedule:null}</div>
                                    </div>
                                </div>
                                <div className={`stores-mobile_container_father_preview .mstore-preview-${i}`} style={i===selected?prevActive:null}>
                                    {storesData?storesData.status==='success'?storesData.data[selected].preview_type === 'MAP'?
                                        <div className='stores-mobile_container_father_preview_map'>
                                            <Map google={this.props.google} zoom={15} initialCenter={{
                                                lat: storesData.data[selected].preview_latitude,
                                                lng: storesData.data[selected].preview_longitude
                                            }} center={{
                                                lat: storesData.data[selected].preview_latitude,
                                                lng: storesData.data[selected].preview_longitude
                                            }}
                                                onClick={this.onMapClicked}
                                            >
                                                <Marker onClick={this.onMarkerClick}
                                                    title={`Ayni's store`} 
                                                    name={storesData.data[selected].name}
                                                    position={{
                                                        lat: storesData.data[selected].preview_latitude,
                                                        lng: storesData.data[selected].preview_longitude
                                                    }} />
                                                <InfoWindow onClose={this.onInfoWindowClose} 
                                                    marker={activeMarker} 
                                                    visible={showingInfoWindow}
                                                >
                                                <div>
                                                    <h1>{selectedPlace.name}</h1>
                                                </div>
                                                </InfoWindow>
                                            </Map>
                                        </div>
                                    :
                                        <img src={storesData.data[selected].preview_image_path} 
                                        onClick={()=>this.goToWebsite(storesData.data[selected].url)}  />
                                    :null:null}
                                </div>
                            </div>
                                )
                        }):null:null}
                </div>
            </div>
        )
    }
}

export default GoogleApiWrapper({
    apiKey: ('AIzaSyAbHOaFI5djya6RJoSrbJC6zB7e17KTETk')
})(StoresMobile)