import React, {Component} from 'react'
import './Shop.scss'
import { api } from '../../../core/helpers/api';
import { withRouter } from 'react-router-dom';
import { toMoney } from '../../../core/helpers/formats';
import { connect } from 'react-redux';
import NavSmall from '../../../components/menu/nav-small/NavSmall';
import $ from 'jquery'
import { setShopFilter } from '../../../core/store/actions/header';
class Shop extends Component{
    constructor(props){
        super(props)
        this.state={
            selectors: {category: -1, collection: -1, color: -1},
            dropState: {category: true, collection: false, color: true},
            categories: [],
            collections: [],
            colors: [],
            products: []
        }
        this.productFilter=this.productFilter.bind(this)
    }
    onDropDown(key){
        const { dropState } = this.state
        switch(key){
            case 'category': this.setState({ dropState: { ...dropState, category: !dropState.category } }); break;
            case 'collection': this.setState({ dropState: { ...dropState, collection: !dropState.collection } }); break;
            case 'color': this.setState({ dropState: { ...dropState, color: !dropState.color } }); break;
            default: break;
        }
    }
    qParams(){
        // check params from menu
        const { filterParams } = this.props
        if(filterParams)
            this.onCheck('category', filterParams.id, filterParams.name)
        else
            this.productFilter()
    }
    onCheck(key, id, name){
        const { appIsMobile } = this.props
        const { selectors } = this.state
        if(selectors[key] !== id) // Check
            this.setState({ selectors: { ...selectors, [key]: key==='category'&&name==='ALL'?-1:id } }, () => this.productFilter())
        else // UnCheck
            this.setState({ selectors: { ...selectors, [key]: -1 } }, () => this.productFilter())
        
        if(!appIsMobile)
            $('html, body').animate({ scrollTop: 100 }, );
    }
    getColors(route, state){
        api.get(route, [], (res) => {
            if(res.data){
                const data = res.data.data
                    if(data)
                        this.setState({ ...this.state, [state]: data })
            }
        })
    }
    productFilter(){
        const { selectors } = this.state
        const category = selectors.category
        const collection = selectors.collection
        const color = selectors.color
        if(category < 0 && collection < 0 && color < 0)
            var route = '/products'
        else
            var route = `/products?${category>-1?'category='+category:false}&${collection>-1?'collection='+collection:false}&${color>-1?'color='+color:false}`
        api.get(route, [], (res) => {
            console.log('Catalog content: ', res)
            if(res.data){
                const data = res.data.data
                    if(data)
                        this.setState({ ...this.state, products: data })
            }
        })
    }
    componentDidMount(){
        const routes = ['/categories', '/collections', '/colors'] // /products
        const states = ['categories', 'collections', 'colors'] // products
        var i = 0
        while (i < routes.length){
            this.getColors(routes[i], states[i])
            i++
        }
        //this.productFilter()
        this.qParams()
    }
    goDetail(id){
        this.props.history.push(`/product/${id}`)
    }
    componentWillUnmount(){
        this.props.setShopFilterParams(null)
    }
    componentWillReceiveProps(nextProps){
        // Listen changes over shop filter dropdown menu
        const { filterParams } = nextProps
        if(filterParams)
            this.onCheck('category', filterParams.id, filterParams.name)
    }
    render(){
        const { selectors, categories, collections, colors, products, dropState } = this.state
        const display = { display: 'none' }
        const selectedStyle = { fontWeight: 'bold' }
        const iconSelected = { backgroundColor: '#A6A6A6', backgroundImage: `url(${process.env.REACT_APP_URL_BASE}/images/icons/mat-icons/check-negative.png)` }
        const navOptions = [{text: 'Home', route: '/'}, {text: 'Shop', route: '/shop'}]

        //console.log('current State: ', this.state)
        //console.log('filter redux params: ', this.props.filterParams)
        return(
            <div className="shop">
                <div className='shop_nav-small'><NavSmall options={navOptions} /></div>
                <div className='shop_container'>
                    <div className='shop_container_options'>
                        <div className='shop_container_options_content'>
                            {/* <ShopDropdown title='CATEGORIES' options={categories} select={0} /> */}
                            {/* <ShopDropdown title='COLLECTIONS' options={collections} /> */}
                            {/* <ShopDropdown title='COLORS' options={colors} /> */}
                            {/*  */}
                            <div className='shop-dropdown' style={{paddingBottom: '40px'}}>
                                <div className='shop-dropdown_title' onClick={()=>this.onDropDown('category')}>
                                    <div className='shop-dropdown_title_text'>{'CATEGORY'}</div>
                                    <i className='shop-dropdown_title_icon material-icons'>{dropState.category?'keyboard_arrow_up':'keyboard_arrow_down'}</i>
                                </div>
                                <div className='shop-dropdown_options' style={dropState.category===false?display:null} >
                                    {categories.map((data, i) => {
                                        return(
                                            <div className='shop-dropdown_options_content' key={i}>
                                                <div className='shop-dropdown_options_content_check' style={selectors.category===data.id?iconSelected:selectors.category===-1&&data.name==='ALL'?iconSelected:null} />
                                                <div className='shop-dropdown_options_content_text' style={selectors.category===data.id?selectedStyle:selectors.category===-1&&data.name==='ALL'?selectedStyle:null}
                                                onClick={()=>this.onCheck('category', data.id, data.name)}>{data.name}</div>
                                            </div>
                                        )
                                    })}
                                </div>
                            </div>
                            {/*  */}
                            {/* <div className='shop-dropdown' style={{paddingBottom: '40px'}}>
                                <div className='shop-dropdown_title' onClick={()=>this.onDropDown('collection')}>
                                    <div className='shop-dropdown_title_text'>{'COLLECTION'}</div>
                                    <i className='shop-dropdown_title_icon material-icons'>{dropState.collection?'keyboard_arrow_up':'keyboard_arrow_down'}</i>
                                </div>
                                <div className='shop-dropdown_options' style={dropState.collection===false?display:null} >
                                    {collections.map((data, i) => {
                                        return(
                                            <div className='shop-dropdown_options_content' key={i}>
                                                <div className='shop-dropdown_options_content_check' style={selectors.collection===i?iconSelected:null} />
                                                <div className='shop-dropdown_options_content_text' style={selectors.collection===i?selectedStyle:null}
                                                onClick={()=>this.onCheck('collection', i)}>{data.name}</div>
                                            </div>
                                        )
                                    })}
                                </div>
                            </div> */}
                            {/*  */}
                            <div className='shop-dropdown'>
                                <div className='shop-dropdown_title' onClick={()=>this.onDropDown('color')}>
                                    <div className='shop-dropdown_title_text'>{'COLOR'}</div>
                                    <i className='shop-dropdown_title_icon material-icons'>{dropState.color?'keyboard_arrow_up':'keyboard_arrow_down'}</i>
                                </div>
                                <div className='shop-dropdown_options' style={dropState.color===false?display:null} >
                                    {colors.map((data, i) => {
                                        return(
                                            <div className='shop-dropdown_options_content' key={i}>
                                                <div className='shop-dropdown_options_content_check' style={selectors.color===data.id?iconSelected:null} />
                                                <div className='shop-dropdown_options_content_text' style={selectors.color===data.id?selectedStyle:null}
                                                onClick={()=>this.onCheck('color', data.id, data.name)}>{data.name}</div>
                                            </div>
                                        )
                                    })}
                                </div>
                            </div>
                            {/*  */}
                        </div>
                    </div>
                    <div className='shop_container_products'>
                        <div className='shop_container_products_body'>
                            {products.length > 0?products.map((data, i) => {
                                return(
                                    <div className='shop_container_products_body_content' key={i}>
                                        <img src={data.images[0]} alt="" onClick={()=>this.goDetail(data.id)}/>
                                        <span className='shop_container_products_body_content_name'>{data.title}</span>
                                        <span className='shop_container_products_body_content_price'>$ {toMoney(data.price)}</span>
                                    </div>
                                )
                            }):<div style={{paddingTop: '30px', color: 'darkgray', fontFamily: 'Gotham-Book'}}>Empty</div>}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return{
        appIsMobile: state.isMobile,
        filterParams: state.shopFilterBy
    }
}
const mapDispatchToProps = dispatch => {
    return {
        setShopFilterParams(params){
            dispatch(setShopFilter(params))
        }
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Shop));