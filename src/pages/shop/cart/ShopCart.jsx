import React from 'react'
import './ShopCart.scss'
import { toMoney } from '../../../core/helpers/formats';
import { withRouter, Link } from 'react-router-dom';
import { updateCartCounter, setSessionCart } from '../../../core/store/actions/cart';
import { connect } from 'react-redux';
import { notify } from 'react-notify-toast'
import swal from 'sweetalert2'
import NavSmall from '../../../components/menu/nav-small/NavSmall';
import { api } from '../../../core/helpers/api';
import $ from 'jquery'

const init_state = { cartData: [], total: 0 }

class ShopCart extends React.Component{
    constructor(props){
        super(props)
        this.state = init_state
        this.labels = ['PRODUCT','PRICE','QUANTITY','TOTAL']
        this.onUpdate=this.onUpdate.bind(this)
        this.getLocalCart=this.getLocalCart.bind(this)
    }
    onUpdate(){
        notify.show(`Updating...`, 'custom', 1000, {background: 'rgba(255,200,255,.2)', text: '#393939'})
        $('html, body').animate({ scrollTop: 200 }, );
        this.setState(init_state)
        setTimeout(() => { this.getLocalCart() }, 500);
    }
    getLocalCart(){
        const uid = localStorage.getItem(process.env.REACT_APP_LOCAL_UID)
        if(uid){
            const { sessionCart } = this.props
            this.interfacePrinter(sessionCart)
        } else {
            const localCart = localStorage.getItem(process.env.REACT_APP_LOCAL_CART)
            if(localCart){
                const cart = JSON.parse(localCart)
                this.interfacePrinter(cart)
            }
        }
    }
    interfacePrinter(cart){
        // calcular total y setear datos del carrito al estado local
        var total = 0
        for (let i = 0; i < cart.length; i++) {
            const iTotal = cart[i].total;
            total = total + iTotal
        }
        this.setState({ cartData: cart, total: total })
    }
    onDelete(position){
        const { onUpdateCartCounter, counter, sessionCart, onUpdateSessionCart } = this.props
        const uid = localStorage.getItem(process.env.REACT_APP_LOCAL_UID)
        const localCart = localStorage.getItem(process.env.REACT_APP_LOCAL_CART)
        var cart = uid ? sessionCart : JSON.parse(localCart)
        cart.splice(position, 1)
        if(!uid){
            var newCart = JSON.stringify(cart)
            localStorage.setItem(process.env.REACT_APP_LOCAL_CART, newCart)
            onUpdateCartCounter(counter - 1) // Update Redux counter cart
            notify.show(`The product was removed successfully`, 'custom', 3000, {background: 'rgba(0,200,0,.1)', text: '#393939'})
        } else {
            var params = { shoppingCartJson: cart }
            api.post(`/users/${uid}/shoppingCart`, params, (res) => {
                //console.log(`Res for set cart test in shop CART: `, res)
                if(res.data){
                    if(res.data.status === 'success'){
                        onUpdateSessionCart(cart)
                        onUpdateCartCounter(counter - 1) // Update Redux counter cart
                        notify.show(`The product was removed successfully`, 'custom', 3000, {background: 'rgba(0,200,0,.1)', text: '#393939'})
                    }else { alert('Error: An error occurred while removing') } 
                } else { alert('Error: An error occurred while removing') }
            })
        }
        this.getLocalCart() // Re render Local Cart
    }
    confirmDelete(position, name, color, size, quantity){
        swal.fire({ 
            html: `<div style='font-family: Gotham-Light; text-align: center; font-size:16px;'><b>Are you sure to remove?</b><br><br>
            <div style='font-size: 14px; text-align: left;'>
            <b>Product:</b> ${name}<br> 
            <b>Color:</b> ${color} <br>
            <b>Size:</b> ${size}<br>
            <b>Quantity:</b> ${quantity}
            </div></div>`, 
            customClass: 'customContSWDEL', customContainerClass: "customSWDEL", 
            allowEscapeKey: false,
            confirmButtonColor: '#393939',
            showCancelButton: true,
            animation: false, allowOutsideClick: false,
            focusConfirm: false, focusCancel:  false,
            backdrop: 'rgba(103, 103, 7, .4)'
        }).then((res) => {
            if(res.value)
                this.onDelete(position)
        })
    }
    goProduct(id, position){
        this.props.history.push(`/product/${id}?edit=${btoa(position)}`)
    }

    componentDidMount(){
        this.getLocalCart()
    }
    componentWillReceiveProps(nextProps){
        if(localStorage.getItem(process.env.REACT_APP_LOCAL_UID))
            this.interfacePrinter(nextProps.sessionCart)
    }
    render(){
        const { cartData, total } = this.state
        console.log(this.state)
        //const testImg = { backgroundImage: `url(http://167.99.236.225/prestashop/api/images/products/8/63?ws_key=Y5ZIKF5AQXZW9IZUM7C4DQ793VMRFQ9B)` }
        const navOptions = [{text:'Home', route: '/'}, {text:'Bag', route: '/cart'}]
        return(
            <div className='shop-cart'>
                <div className='shop-cart_nav-small'><NavSmall options={navOptions} /></div>
                <div className='shop-cart_title'>AYNI SHOP</div>
                <div className='shop-cart_title-mobile'>SHOPPING BAG</div>
                <div className='shop-cart_subtitle'>SHOPPING BAG</div>
                <div className='shop-cart_container'>
                    <div className='shop-cart_container_labels'>
                        {this.labels.map((text, i) => {
                            return(
                                <span key={i} className={`shop-cart_container_labels_${text}`}>{text}</span>
                            )
                        })}
                    </div>
                    <div className='shop-cart_container_labels-mobile'>PRODUCT</div> {/* mobile */}
                    <div className='shop-cart_container_body'>
                        {cartData.length>0?cartData.map((data, i) => {
                            const img = { backgroundImage: `url(${data.image})` }
                            const id = data.id
                            const name = data.name
                            const color = data.color
                            const size = data.size
                            const quantity = data.quantity
                            const price = data.price
                            const total = data.total
                            return(
                                <div className='shop-cart_container_body_content' key={i}>
                                    <div className='shop-cart_container_body_content_product'>
                                        <div className='shop-cart_container_body_content_product_img' style={img}/>
                                        <div className='shop-cart_container_body_content_product_detail'>
                                            <span className='shop-cart_container_body_content_product_detail_name'>{name}</span>
                                            <span className='shop-cart_container_body_content_product_detail_color'>COLOR: {color}</span>
                                            <span className='shop-cart_container_body_content_product_detail_size'><span>SIZE:</span> {size}</span>
                                            <span className='shop-cart_container_body_content_product_detail_size'><span>PRICE:</span> {price}</span> {/* mobile */}
                                            <span className='shop-cart_container_body_content_product_detail_size'><span>QUANTITY:</span> {quantity}</span> {/* mobile */}
                                            <div className='shop-cart_container_body_content_product_detail_buttons'>
                                                <button onClick={()=>this.goProduct(id, i)}>EDIT</button>
                                                <button onClick={()=>this.confirmDelete(i, name, color, size, quantity)}>DELETE</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div className='shop-cart_container_body_content_price'>$ {toMoney(price)}</div>
                                    <div className='shop-cart_container_body_content_quantity'>{quantity}</div>
                                    <div className='shop-cart_container_body_content_total'>$ {toMoney(total)}</div>
                                    <div className='shop-cart_container_body_content_buttons'>
                                        <button onClick={()=>this.goProduct(id, i)}>EDIT</button>
                                        <button onClick={()=>this.confirmDelete(i, name, color, size, quantity)}>DELETE</button>
                                    </div>
                                </div>
                            )
                        }):<div className='shop-cart_container_body_cart-empty'>Bag is empty</div>}
                    </div>

                    <div className='shop-cart_container_footer'>
                        <div className='shop-cart_container_footer_total'>
                            <label>TOTAL:</label>
                            <span>$ {toMoney(total)}</span>
                        </div>
                        <div className='shop-cart_container_footer_buttons'>
                            <Link to='/shop' className='shop-cart_container_footer_buttons_goshop'><button>CONTINUE SHOPPING</button></Link>
                            <button className='shop-cart_container_footer_buttons_update' onClick={this.onUpdate}>UPDATE</button>
                            <Link to='/checkout' className='shop-cart_container_footer_buttons_gocheckout'>
                                <button style={cartData.length>0?null:{cursor:'not-allowed'}} disabled={cartData.length>0?false:true}>CHECKOUT</button>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        counter: state.cart.counter,
        sessionCart: state.cart.data
    }
}
const mapDispatchToProps = dispatch => {
    return {
        onUpdateCartCounter(count){
            dispatch(updateCartCounter(count))
        },
        onUpdateSessionCart(data){
            dispatch(setSessionCart(data))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ShopCart))