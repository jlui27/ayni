import React, { Component } from 'react'
import './FooterForDetail.scss'
import { Link, withRouter } from 'react-router-dom';
import Subscription from '../../../../components/subscription/Subscription';
import NavMain from '../../../../components/menu/nav-main/NavMain';
import NavFooter from '../../../../components/menu/nav-footer/NavFooter';
import { socialNetworking } from '../../../../core/constants/menu';
import StickyFooter from 'react-sticky-footer';
import NavMainV2 from '../../../../components/menu/nav-main-v2/NavMainV2';

class Footer extends Component{
    constructor(){
        super()
        this.deniedSubscribeFor = ['/our-universe']
        this.deniedNavFor = ['/', '/faq', '/stores', '/shipping-returns', '/checkout', '/our-universe']
        this.deniedNavFooterFor = ['/our-universe']
    }
    render(){
        const logo1 = { backgroundImage: `url(${process.env.REACT_APP_URL_BASE}/images/fixed/logoAY.png)`}
        const logo2 = { backgroundImage: `url(${process.env.REACT_APP_URL_BASE}/images/fixed/logotipo.jpg)` }
        return(
            <div className='footer'>
                <div className='footer_container'>
                    {/* {this.deniedNavFooterFor.indexOf(window.location.pathname) < 0 ? <Subscription/>: null} */}
                    {/* {this.deniedNavFor.indexOf(window.location.pathname) < 0 ? <NavMain/> : null} */}
                    {this.deniedNavFor.indexOf(window.location.pathname) < 0 ? 
                    <StickyFooter
                    bottomThreshold={407}
                    normalStyles={{
                        with: '100%',
                    }}
                    stickyStyles={{
                    backgroundColor: "rgba(255,255,255,.8)",
                    width: '100%',
                    height: '50px',
                    }}
                > <NavMainV2/> </StickyFooter>
                    : null}
                    <div className='footer_container_line'></div>
                    {/* {this.deniedNavFooterFor.indexOf(window.location.pathname) < 0 ? <NavFooter/>: null} */}
                    <div className='footer_container_logo1' style={logo1} onClick={()=>this.props.history.push('/')} />
                    <div className='footer_container_social'>
                        <strong>FOLLOW US</strong>
                        {window.location.pathname==='/'?
                        <div className='footer_container_social_icons'>
                            {socialNetworking.map((data, i) => {
                                return(
                                    <Link to={data.url} target='_blank' key={i}>
                                        <div style={{backgroundImage: `url(${process.env.REACT_APP_URL_BASE}${data.icon}`}} />
                                    </Link>
                                )
                            })}
                        </div>
                        :
                        <div className='footer_container_social_option'>
                            {socialNetworking.map((data, i) => {
                                return(
                                    <Link to={data.url} target='_blank' key={i}>{data.text}</Link>
                                )
                            })}    
                        </div>
                        }
                    </div>
                    <div className='footer_container_logo2' style={logo2} onClick={()=>this.props.history.push('/')} />
                    <div className='footer_container_copyright'>© 2019 AYNI</div>
                </div>
            </div>
        )
    }
}

export default withRouter(Footer)