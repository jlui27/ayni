import React, { Component } from 'react'
import './GoShopButton.scss'
import { withRouter } from 'react-router-dom';

class GoShopButton extends Component{
    constructor(props){
        super(props)
        this.state = {
            showText: false
        }
    }
    onOver(){ document.getElementById("go-shop-button_txt").style.opacity = "1"; }
    onOut(){ document.getElementById("go-shop-button_txt").style.opacity = "0"; }
    render(){
        const { showText } = this.state
        return(
            <div className='go-shop-button'>
                <button onMouseOver={this.onOver} onMouseOut={this.onOut} onClick={() => this.props.history.push('/shop')}
                className='go-shop-button_btn'></button>
                <span className='go-shop-button_txt' id='go-shop-button_txt'>Back to shop</span>
            </div>
        )
    }
}

export default withRouter(GoShopButton)