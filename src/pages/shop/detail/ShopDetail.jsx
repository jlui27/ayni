import React from 'react'
import './ShopDetail.scss'
import { api } from '../../../core/helpers/api';
import ProductInfo from './info/ProducfInfo';
import RelatedProducts from './related/RelatedProducts';
import FooterForDetail from './detail-footer/FooterForDetail';
import GoShopButton from './go-shop-button/GoShopButton';
import Swiper from 'react-id-swiper'
import $ from 'jquery'

class ShopDetail extends React.Component{
    constructor(){
        super()
        this.state={
            detailData: []
        }
        this.swiperParams = {
            slidesPerView: 1,
            spaceBetween: 10,
            initialSlide: 0,
            pagination: {
              el: '.swiper-pagination',
              type: 'bullets',
              clickable: true
            }
        }
    }
    getProduct(id){
        api.get('/products/', id, (res) => {
            if(res.data)
                this.setState({ detailData: res.data.data })
        })
    }
    componentDidMount(){
        const { match } = this.props
        var id = parseInt(match.params.id)
        this.getProduct(id)
        //$('html, body').animate({ scrollTop: 100 }, );
    }
    render(){
        const { detailData } = this.state
        if(detailData)
            var images = detailData.images
            
        const testImages = [
            'http://shuttermike.com/wp-content/uploads/2010/02/Sedona-Vertical.jpg',
            'http://www.naturephotographysimplified.com/wp-content/uploads/2014/10/Nature-Photography-Landscape-Photogarphy-Vertical-Composition-Fall-Foliage-Road-To-Agate-Falls.jpg',
            'http://www.naturephotographysimplified.com/wp-content/uploads/2014/10/Nature-Photography-Simplified-Landscape-Photography-Vertical-Composition-Technique-Waterfalls-In-Blue-Ginger-Resort-Wayanad.jpg',
            'http://cdn3.craftsy.com/a/general/wp-content/uploads/2015/01/silver-cascade-autumn-2279.jpg'
        ]
        console.log(detailData)
        return(
            <div className='shop-detail'>
                {detailData?
                <div className='shop-detail_container'>
                    <div className='shop-detail_container_left'>
                        <div className='shop-detail_container_left_images'>
                            {images?
                            <Swiper {...this.swiperParams} >
                                {images.length>0?images.map((data, i) => {
                                    return(
                                        <div className='shop-detail_container_left_images_content' key={i}>
                                            <img className='shop-detail_container_left_images_content_data' src={data} />
                                        </div>
                                    )
                                }): null}
                            </Swiper>
                            :null}
                        </div>
                        <ProductInfo product={detailData} />
                    </div>
                    {/* <div className='shop-detail_container_right'>
                        <div className='shop-detail_container_right_content' style={images?images.length>1?{overflowY: 'scroll'}:null:null}>
                            {images?images.map((data , i) => {
                                return(
                                    <div className='shop-detail_container_right_content_data' key={i} style={{backgroundImage: `url(${data})`}} />
                                )
                            }):null}
                        </div>
                    </div> */}
                    <div className='shop-detail_container_right'>
                        <div className='shop-detail_container_right_content'>
                            {images?images.map((data, i) => {
                                return(
                                    //<div className='shop-detail_container_right_content_data' key={i} style={{backgroundImage: `url(${data})`}} />
                                    <img src={data} alt="" key={i} />
                                )
                            }):null}
                        </div>
                        <div className='shop-detail_container_right_back-btn'>
                            <GoShopButton/>
                        </div>
                    </div>
                </div>
                :<div className='shop-detail_no-found'>Product detail not found</div>}
                <RelatedProducts/>
                <FooterForDetail/>
            </div>
        )
    }
}

export default ShopDetail