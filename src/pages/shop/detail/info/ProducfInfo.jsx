import React from 'react'
import './ProductInfo.scss'
import { updateCartCounter, setSessionCart } from '../../../../core/store/actions/cart';
import { connect } from 'react-redux';
import { toMoney, arrayRemove, removeHTMLTags } from '../../../../core/helpers/formats';
import { notify } from 'react-notify-toast'
import swal from 'sweetalert2'
import { withRouter } from 'react-router-dom'
import { api } from '../../../../core/helpers/api';

class ProductInfo extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            iColor: false, iSize: false,
            errColor: false, errSize: false,
            localPositionToEdit: -1,
            productData: {
                id: 0, name: null, description: null, image: null, color: null, size: null, price: null, quantity: 1, total: 0
            },
            allColors: [], allColorsName: []
        }

        this.addToBag=this.addToBag.bind(this)
        this.getAndSetProductData=this.getAndSetProductData.bind(this)
        this.onChooseFeatures=this.onChooseFeatures.bind(this)
        this.getAllColors=this.getAllColors.bind(this)
    }
    confirmDuplicateProduct(callback){
        swal.fire({ 
            html: `<span style='font-family:Gotham-Book;'>This product and its combination already exist in bag. Do you want to modify?</span>`, 
            allowEscapeKey: false,
            confirmButtonColor: '#393939',
            showCancelButton: true,
            animation: false, allowOutsideClick: false,
            focusConfirm: false, focusCancel:  false
        }).then((res) => {
            callback(res.value)
        })
    }
    addToBag(){
        const { productData, localPositionToEdit } = this.state
        const { cartCounter, updateCartCounter, updateSessionCart, sesCartData } = this.props
        const execute = () => {
            const uid = localStorage.getItem(process.env.REACT_APP_LOCAL_UID)
            const localCart = localStorage.getItem(process.env.REACT_APP_LOCAL_CART)
            var cart = uid ? sesCartData : JSON.parse(localCart)
            const successAlert = (message) => {
                notify.show(message, 'custom', 4000, {background: '#C0EAA4', text: '#393939'})
                setTimeout(() => { this.props.history.push('/cart') }, 1000) // redirect to bag
            }
            const addinToBag = (message) => {
                cart.push(productData)
                if(!uid){ // si no existe sesion
                    const newCart = JSON.stringify(cart)
                    localStorage.setItem(process.env.REACT_APP_LOCAL_CART, newCart)
                    successAlert(message)
                } else {
                    // actualizar cart storage y redux
                    var params = { shoppingCartJson: cart }
                    api.post(`/users/${uid}/shoppingCart`, params, (res) => {
                        //console.log(`Res for set cart test in shop CART: `, res)
                        if(res.data){
                            if(res.data.status === 'success'){
                                updateSessionCart(cart) // actualizar session cart (redux)
                                successAlert(message)
                            } else { alert('Error: Error when removing, try again') }
                        } else { alert('Error: Error when removing, try again') }
                    })
                }
            }
            if(!localCart && !uid){ // when bag/cart is empty & user not exist
                var initCart = []
                initCart.push(productData)
                const newInitCart = JSON.stringify(initCart)
                localStorage.setItem(process.env.REACT_APP_LOCAL_CART, newInitCart)
                updateCartCounter(cartCounter + 1)
                successAlert(`Product "${productData.name}" added to bag`)
            } else { //when bag has data or user exists
                const checkCombinationAndSave = () => {
                    var currentCart = cart
                    var localCombinations = []
                    for(let i=0; i<currentCart.length; i++){
                        let combination = currentCart[i].id + currentCart[i].color + currentCart[i].size
                        localCombinations.push(combination)
                    }
                    const newCombination = productData.id + productData.color + productData.size
                    const position = localCombinations.indexOf(newCombination)
                    if(position > -1){
                        if(localPositionToEdit > -1) {
                            // Edit from cart / bag
                            if(position === localPositionToEdit){
                                cart.splice(localPositionToEdit, 1)
                                addinToBag(`Product "${productData.name}" was modified`)
                            } else {
                                // No edit from Shop
                                this.confirmDuplicateProduct((confirm) => {
                                    if(confirm){
                                        // Esta parte esta pendiente, no borra el ultimo item ni el duplicado cuando se esta modificando
                                        cart.splice(position, 1)
                                        cart.splice(localPositionToEdit, 1)
                                        updateCartCounter(cartCounter - 1)
                                        addinToBag(`Product "${productData.name}" was modified`)
                                    }
                                })
                            }
                        } else {
                            this.confirmDuplicateProduct((confirm) => {
                                if(confirm){
                                    cart.splice(position, 1)
                                    addinToBag(`Product "${productData.name}" was modified`)
                                }
                            })
                        }
                    } else {// si hay posicion para editar, eliminar esa posicion 
                        if(localPositionToEdit > -1){
                            cart.splice(localPositionToEdit, 1)
                        } else {
                            updateCartCounter(cartCounter + 1)
                        }
                        addinToBag(`Product "${productData.name}" added to bag`)
                    }
                }
                
                checkCombinationAndSave()
            }
        }
        if(productData.color === null && productData.size === null)
            this.setState({ errColor: true, errSize: true })
        else if(productData.color === null)
            this.setState({ errColor: true })
        else if(productData.size === null)
            this.setState({ errSize: true })
        else
            execute()
    }
    setBucle(obj){
        const array = []
        for (let i = 0; i < obj.length; i++) {
            var parameter = obj[i].attribute
            const subarray = []
            for (let j = 0; j < parameter.length; j++) {
                const element = parameter[j];
                const subobj = element.split(':')
                var key = subobj[0]
                var value = subobj[1]
                subarray.push({[key] : value})
            }
            array.push(subarray)
        }
        return array
    }
    getAndSetProductData(){
        const  { productData } = this.state
        const { product } = this.props
        if(product.id){
            const id = product.id
            const name = product.title
            const image = product.images[0]
            const price = toMoney(product.price)
            const total = price * productData.quantity
            const description = removeHTMLTags(product.description)
            this.setState({ productData: { ...this.state.productData, id, name, description, image, price, total } })
        }
        //console.log('Product got' , product)
    }
    onChooseFeatures(type, data, i){
        switch(type){
            case 'color': this.setState({ iColor: i, errColor: false, productData: { ...this.state.productData, color: data } }); break;
            case 'size': this.setState({ iSize: i, errSize: false, productData: { ...this.state.productData, size: data } }); break;
            case 'quantity':
                const { productData } = this.state 
                const quantity = productData.quantity
                const price = productData.price
                if(data === 'minus'){
                    if(quantity > 1)
                        this.setState({ productData: { ...this.state.productData, quantity: quantity-1, total: price * (quantity-1) } })
                } else if(data === 'plus') {
                    if(quantity < 10)
                        this.setState({ productData: { ...this.state.productData, quantity: quantity+1, total: price * (quantity+1) } })
                }
                break;
            default: break;
        }
    }
    GetEditParams(){
        try {
            const urlParas = new URLSearchParams(window.location.search) // create URL instance
            const edit = parseInt(atob(urlParas.get('edit'))) // url.get , key = edit y parseado a entero
            if(!isNaN(edit))
                this.setState({ localPositionToEdit: edit })
        } catch (error) {
            swal.fire({ html: `<span style='font-family: Gotham-Book;'><b>ERROR:</b> Edit param failed, press Ok to retry from bag</span>`, 
            confirmButtonColor: 'black', animation: false, allowOutsideClick: false, backdrop: 'rgba(200,0,0,.1)'  })
            .then((res) => {
                if(res.value)
                    window.location.href = '/cart'
            })
        }
    }
    getAllColors(){
        api.get('/colors', '', (res) => {
            if(res)
                if(res.data.status === 'success'){
                    var colorsData = res.data.data
                    var colorsName = [], counter = 0
                    while(counter < colorsData.length){
                        colorsName.push(colorsData[counter].name)
                        counter++
                    }
                    this.setState({ allColors: colorsData, allColorsName: colorsName })
                }
        })
    }
    
    componentDidMount(){
        setTimeout(() => {
            this.getAndSetProductData()
        }, 2000);
        this.GetEditParams()
        this.getAllColors()
    }
    componentDidCatch(){
        alert('ERROR')
    }
    componentDidUpdate(){
        const { product } = this.props
        if(product.id)
            document.getElementById('description').innerHTML = product.description
    }
    render(){
        const { iColor, iSize, errColor, errSize, productData, localPositionToEdit, allColors, allColorsName } = this.state //
        //console.log('Product Data State:' , productData, 'and localPositionToEdit: ', localPositionToEdit) //
        const colorSelected = { paddingBottom: '5px', borderBottom:'1px #1d1d1b solid', width:'30px', marginRight:'8px' }
        const sizeSelected = { borderBottom: '1px black solid', fontWeight: 'bold' }
        const { product } = this.props
        const combs = product.combinations
        var subdata
        var colorList = []
        var sizeList = []
        console.log('produc data', productData)
            if(combs){
                //console.log('COMBS: ',product.combinations.length)
                subdata = this.setBucle(product.combinations)
                for(let i=0; i<subdata.length; i++){
                    //console.log(subdata[i].length)
                    const item = subdata[i]
                    for (let j = 0; j < item.length; j++) {
                        const element = item[j];
                        var jcolor = element.Color
                        var jsize = element.Size
                        if(jcolor){
                            if(colorList.indexOf(jcolor) < 0)
                                colorList.push(jcolor)
                        } else if(jsize){
                            if(sizeList.indexOf(jsize) < 0)
                                sizeList.push(jsize)
                        }  
                    }
                }
                /*
                    document.getElementById('description').innerHTML = product.description
                */
                return(
                    <div className='product-info'>
                        <div className='product-info_title'>{product.title}</div>
                        <div className='product-info_price'>$ {(Math.round(product.price*Math.pow(10,2))/Math.pow(10,2)).toFixed(2)}</div>
                        <div className='product-info_container'>
                            <span style={errColor?{color: '#ec3c3c', fontWeight:'bold'}:null}>COLOR</span>
                            <div className='product-info_container_content'>
                                {subdata ?colorList.map((data , i) => {
                                    const findColor = allColorsName.indexOf(data)
                                    const printColor = findColor > -1 ? allColors[findColor].color : 'white'
                                    //console.log('color data got: ', data, 'colors to print: ', printColor, 'all colors: ', allColors)
                                    return(
                                        <div key={i} style={iColor===i?colorSelected:null}>
                                            <div key={i} className='product-info_container_content_item' id='color_item'
                                             /*style={data.toLowerCase()==='#ffffff'||data.toLowerCase()==='white'?{backgroundColor: data, border: '1px solid black'}:{backgroundColor: printColor}}*/ title={data} onClick={()=>this.onChooseFeatures('color', data, i)} 
                                             style={{backgroundColor: printColor, border: '1px solid black'}} />
                                        </div>
                                    )
                                }):null}
                                {errColor?<div className='product-info_container_content_error'>You missed to choose a&nbsp;<b>color</b></div>:false}
                            </div>
                        </div>
                        <div className='product-info_container'>
                            <span style={errSize?{color: '#ec3c3c', fontWeight:'bold'}:null}>SIZE</span>
                            <div className='product-info_container_content'>
                                {sizeList?sizeList.map((data , i) => {
                                    return(
                                        <div key={i} className='product-info_container_content_item' id='size_item' style={iSize===i?sizeSelected:null}
                                        onClick={()=>this.onChooseFeatures('size', data, i)}>{data}</div>
                                    )
                                }):null}
                                {errSize?<div className='product-info_container_content_error'>You missed to choose a&nbsp;<b>size</b></div>:false}
                            </div>
                        </div>
                        <div className='product-info_container'>
                                <span>QUANTITY</span>
                                <div style={{width: '70px', display:'flex', justifyContent: 'space-between'}}>
                                    <button onClick={()=>this.onChooseFeatures('quantity','minus', null)}>-</button>
                                    <span style={{paddingLeft:'5px'}}>{productData.quantity}</span>
                                    <button onClick={()=>this.onChooseFeatures('quantity','plus', null)}>+</button>
                                </div>
                        </div>
                        <div className='product-info_container'>
                            <span>DESCRIPTION</span>
                            <div id='description'></div>
                        </div>
                        <div className='product-info_container'>
                            <span>COMPOSITION</span>
                            <div>{product.composition}</div>
                        </div>
                        <div className='product-info_container'>
                            <span>MADE IN</span>
                            <div>Perú</div>
                        </div>
                        <div className='product-info_container'>
                            <span>CARE</span>
                            <div>{product.care}</div>
                        </div>
                        <button onClick={this.addToBag}>ADD TO SHOPPING BAG</button>
                    </div>
                )
            } else {
                return(
                    <b>Loading info ...</b>
                )
            }
    }
}

const mapStateToProps = state => {
    return {
        cartCounter: state.cart.counter,
        sesCartData: state.cart.data
    }
}
const mapDispatchToProps = dispatch => {
    return {
        updateCartCounter(count){
            dispatch(updateCartCounter(count))
        },
        updateSessionCart(data){
            dispatch(setSessionCart(data))
        }
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ProductInfo))