import React from 'react'
import './RelatedProducts.scss'
import { api } from '../../../../core/helpers/api';
import ProdPreview from '../../../../components/prod-preview/ProdPreview';

class RelatedProducts extends React.Component{
    constructor(){
        super()
        this.state={
            relatedData: []
        }
        this.getRelated=this.getRelated.bind(this)
    }
    getRelated(){
        const urlId = window.location.pathname
        const productId = parseInt(urlId.split('/')[2])
        api.get(`/products/${productId}/recommended`, [], (res) => {
            if(res.data){
                const data = res.data.data
                    if(data)
                        this.setState({ relatedData: data })
            }
        })
    }
    componentDidMount(){
        this.getRelated()
    }
    render(){
        const { relatedData } = this.state
        console.log('renderDATA: ', relatedData)
        return(
            <div className='related-products'>
                <div className='related-products_title'>RELATED PRODUCTS</div>
                <div className='related-products_titlev2'>COMPLETE THE LOOK</div>
                <div className='related-products_body'>
                    <ProdPreview products={relatedData} width='300px' limit={3} />
                </div>
            </div>
        )
    }
}

export default RelatedProducts