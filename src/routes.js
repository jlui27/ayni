import React from 'react'
import { Route, Switch } from 'react-router-dom'
import App from './App';
import Home from './pages/home/Home';
//import Shop from './pages/shop/Shop';
import Shop from './pages/shop/catalog/Shop'
import Page404 from './pages/page404/Page404';
import Faq from './pages/mini-pages/faq/Faq';
import Shipping from './pages/mini-pages/sar/Shipping';
import Stores from './pages/store/Stores';
import ShopDetail from './pages/shop/detail/ShopDetail';
import OurUniverse from './pages/our-universe/OurUniverse';
import DetailCollection from './pages/collections/detail/DetailCollections';
import NewsMain from './pages/newsletter/main/NewsMain';
import CollectionsMain from './pages/collections/main/CollectionsMain';
import Auth from './pages/account/auth/Auth';
//import Account from './pages/account/Account';
import Profile from './pages/account/profile/Profile';
import SignUp from './pages/account/sign-up/SignUp';
import ShopCart from './pages/shop/cart/ShopCart';
import Checkout from './pages/account/checkout/Checkout';
import SizeGuide from './pages/mini-pages/size-guide/SizeGuide';
import PaymentMethods from './pages/mini-pages/payment-methods/PaymentMethods';
import TermsConditions from './pages/mini-pages/terms/TermsConditions';

export const AppRoutes = () =>
    <App>
        <Route>
            <Switch>
                <Route exact path='/' component={Home}/>
                <Route exact path='/our-universe' component={OurUniverse} />
                <Route exact path='/shop' component={Shop}/>
                <Route exact path='/product/:id' component={ShopDetail} />
                <Route exact path='/cart' component={ShopCart} />
                <Route exact path='/collections' component={CollectionsMain} />
                <Route exact path='/collection/:id' component={DetailCollection} />
                <Route exact path='/behind-the-brand' component={NewsMain} />
                <Route exact path='/behind-the-brand/:id' component={NewsMain} />
                <Route exact path='/auth' component={Auth} />
                <Route exact path='/auth/register' component={SignUp} />
                <Route exact path='/profile' component={Profile} />
                <Route exact path='/profile/:section' component={Profile} />
                <Route exact path='/checkout' component={Checkout} />
                <Route exact path='/faq' component={Faq} />
                <Route exact path='/shipping-returns' component={Shipping} />
                <Route exact path='/stores' component={Stores} />
                <Route exact path='/size-guide' component={SizeGuide} />
                <Route exact path='/payment-methods' component={PaymentMethods} />
                <Route exact path='/terms' component={TermsConditions} />
                <Route component={Page404}/>
            </Switch>
        </Route>
    </App>;