import { createStore } from 'redux'
import { reducer, init_state } from './reducers';
export default createStore(reducer, init_state)