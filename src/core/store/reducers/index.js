
export let init_state = {
    collections: [], lastCollection: 0,
    shopCategories: [], shopFilterBy: null,
    lookModal: { display: false, data: [], init: 0 },
    newsLetter: { id:0, detComponent: null },
    account: { sectionSelected: 0 },
    cart: { counter: 0, data: [] },
    checkout: { currentStep: 0, success: false },
    isMobile: false,
}

export const reducer = (state, action) => {
    switch(action.type){
        case 'SET_COLLECTIONS':
            return { ...state, collections: action.data }
        case 'SET_LAST_COLLECTION':
            return { ...state, lastCollection: action.id }
        case 'SET_SHOP_CATEGORIES':
            return { ...state, shopCategories: action.data }
        case 'SET_SHOP_FILTER':
            return { ...state, shopFilterBy: action.params }
        case 'DISPLAY_LOOKBOOK_MODAL':
            return { ...state, lookModal: { ...state.lookModal, display: action.exec } }
        case 'SET_LOOKBOOK_DATA':
            return { ...state, lookModal: { ...state.lookModal, data: action.data } }
        case 'SET_LOOKBOOK_INIT':
            return { ...state, lookModal: { ...state.lookModal, init: action.index } }
        case 'SET_NEWSLETTER_ID':
            return { ...state, newsLetter:{ ...state.newsLetter, id: action.id } }
        case 'ON_MODAL_NEWS':
            return { ...state, newsLetter:{ ...state.newsLetter, detComponent: action.component } }
        case 'CHANGE_PROFILE_SECTION':
            return { ...state, account:{ ...state.account, sectionSelected: action.section } }
        case 'UPDATE_CART_COUNTER':
            return { ...state, cart:{ ...state.cart, counter: action.count } }
        case 'UPDATE_SESSION_CART':
            return { ...state, cart:{ ...state.cart, data: action.data } }
        case 'UPDATE_CHECKOUT_STEP':
            return { ...state, checkout: { ...state.checkout, currentStep: action.step } }
        case 'UPDATE_CHECKOUT_SUCCESS':
            return { ...state, checkout: { ...state.checkout, success: action.bool } }
        case 'UPDATE_IS_MOBILE':
            return { ...state, isMobile: action.bool }
        default:
            break
    }
    return state
}