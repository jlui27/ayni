export const setCollections = data => {
    return {
        type: 'SET_COLLECTIONS',
        data
    }
}

export const setLastCollection = id => {
    return{
        type: 'SET_LAST_COLLECTION',
        id
    }
}

export const setLookbookData = data => {
    return{
        type: 'SET_LOOKBOOK_DATA',
        data
    }
}

export const displayLookbookModal = exec => {
    return{
        type: 'DISPLAY_LOOKBOOK_MODAL',
        exec
    }
}

export const setLookbookIndex = index => {
    return{
        type: 'SET_LOOKBOOK_INIT',
        index
    }
}

export const updateAppIsMobile = bool => {
    return{
        type: 'UPDATE_IS_MOBILE',
        bool
    }
}

export const setShopCategories = data => {
    return{
        type: 'SET_SHOP_CATEGORIES',
        data
    }
}

export const setShopFilter = params => {
    return{
        type: 'SET_SHOP_FILTER',
        params
    }
}