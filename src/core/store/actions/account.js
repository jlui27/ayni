export const changeSection = section => {
    return {
        type: 'CHANGE_PROFILE_SECTION',
        section
    }
}