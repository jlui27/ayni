export const setId = id => {
    return {
        type: 'SET_NEWSLETTER_ID',
        id
    }
}
export const onNewsModal = component => {
    return {
        type: 'ON_MODAL_NEWS',
        component
    }
}