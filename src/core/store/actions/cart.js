export const updateCartCounter = count => {
    return {
        type: 'UPDATE_CART_COUNTER',
        count
    }
}

export const setSessionCart = data => {
    return{
        type: 'UPDATE_SESSION_CART',
        data
    }
}