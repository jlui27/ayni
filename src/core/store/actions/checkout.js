export const changeCheckoutStep = step => {
    return {
        type: 'UPDATE_CHECKOUT_STEP',
        step
    }
}
export const updateCheckoutSuccess = bool => {
    return {
        type: 'UPDATE_CHECKOUT_SUCCESS',
        bool
    }
}