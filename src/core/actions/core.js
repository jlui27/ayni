export const ACTION_TYPES = {
    setModule: 'SET_MODULE',
    changeMode: 'CHANGE_MODE'
};

export const setModule = module => ({
    type: ACTION_TYPES.setModule,
    payload: {
        module
    }
});

export const changeMode = (mode,id) => ({
    type: ACTION_TYPES.changeMode,
    payload: {
        mode,
        id
    }
});