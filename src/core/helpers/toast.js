import iziToast from 'izitoast';
import 'izitoast/dist/css/iziToast.css';

const toast = {
    success: (message,title) => {
        iziToast.success({
            message,
            title,
            position: 'topCenter'
        });
    },
    error: (message,title) => {
        iziToast.error({
            message,
            title,
            position: 'topCenter'
        });
    }
};

export default toast;