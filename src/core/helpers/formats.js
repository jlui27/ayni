export function toMoney(data){
    return (Math.round(data*Math.pow(10,2))/Math.pow(10,2)).toFixed(2)
}

export const getVideoId = function(url){
    var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
    var match = url.match(regExp);
    if (match && match[2].length == 11)
        return match[2];
    else 
        return null
}

export function arrayRemove(arr, value) {
    return arr.filter(function(ele){
        return ele != value
    })
}

export function toCapitalize(s){
    if (typeof s !== 'string') return ''
    return s.charAt(0).toUpperCase() + s.slice(1)
}

export function removeHTMLTags(string){
    return string.replace(/(<([^>]+)>)/ig, "")
}