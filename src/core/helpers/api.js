import axios from "axios";

class Api{
    constructor(baseUrl, tokens = ''){
        this.baseUrl = baseUrl
        this.timeout = 60000
        //this.header = new Headers()
        //this.headers = this.header.append('Authorization',`Token ${token}`);
        //this.instance = axios.create(this.headers)
        this.instance = axios.create({ headers: tokens })
    }
    
    get(url, params, callback){
        this.instance.get(`${this.baseUrl}${url}${params}`, {timeout: this.timeout})
        .then((res) => { callback(res) })
        .catch((err) => { callback(err) })
    }
    post(url, params, callback){
        this.instance.post(`${this.baseUrl}${url}`, params, {timeout: this.timeout})
        .then((res) => { callback(res) })
        .catch((err) => { callback(err) })
    }
    put(url, params, callback){
        this.instance.put(`${this.baseUrl}${url}`, params, {timeout: this.timeout})
        .then((res) => { callback(res) })
        .catch((err) => { callback(err) })
    }
    delete(url, params, callback){
        this.instance.delete(`${this.baseUrl}${url}`, params)
        .then((res) => { callback(res) })
        .catch((err) => { callback(err) })
    }
}

export const api = new Api(process.env.REACT_APP_API_URL, JSON.parse(localStorage.getItem(process.env.REACT_APP_LOCAL_TOKEN)))