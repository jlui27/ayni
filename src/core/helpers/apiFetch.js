export class Api{
    constructor(baseUrl, token = ''){
        this.baseUrl = baseUrl;
        this.token = token;
    }

    setToken(token){
        this.token = token;
    }

    getAuthHeader(header = new Headers()){
        header.append('Authorization',`Token ${this.token}`);
        return header;
    }

    get(url, queryParams = {}){
        return fetch(
            this.setQueryParams(
                `${this.baseUrl}${url}`,
                queryParams
            ),{
            headers: this.getAuthHeader()
        }).then( resp => resp.json() );
    }

    post(url, body={}, queryParams = {}){        
        return fetch(this.setQueryParams(
            `${this.baseUrl}${url}`,
            queryParams
        ),{
            body: body instanceof FormData ? body : JSON.stringify(body),
            headers: this.getAuthHeader(),
            method: 'POST'
        }).then( resp => resp.json() );
    }

    put(url, body={}, queryParams = {}){
        return fetch(this.setQueryParams(
            `${this.baseUrl}${url}`,
            queryParams
        ),{
            body: JSON.stringify(body),
            headers: this.getAuthHeader(),
            method: 'PUT'
        }).then( resp => resp.json() );
    }

    delete(url, queryParams = {}){
        return fetch(this.setQueryParams(
            `${this.baseUrl}${url}`,
            queryParams
        ),{
            headers: this.getAuthHeader(),
            method: 'DELETE'
        }).then( resp => resp.json() );
    }

    setQueryParams(url, queryParams = {}){
        let queryString = Object.keys(queryParams).map( key => {
            return `${key}=${queryParams[key]}`;
        }).join('&');
        return `${url}?${queryString}`;
    }
}

var api = new Api(
    process.env.REACT_APP_API_URL,
    'xxx');

export default api; 
