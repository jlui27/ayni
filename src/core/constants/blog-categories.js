export const blogCategories = [
    {code: 'RECENTS', display: 'Recents'},
    {code: 'FASHION', display: 'Fashion'},
    {code: 'CELEBRITIES', display: 'Celebrities'},
    {code: 'PRESS', display: 'Press'},
    {code: 'TRAVELS', display: 'Travels'},
    {Code: 'OTHERS', display: 'Others'}
];