import swal from 'sweetalert2'

export const alertConfirm = (text, callback) => {
    swal.fire({
        //title: `<h2 style='font-family: Gotham-Light'>AYNI</h2>`,
        html: `<div style='font-family: Gotham-Light; text-align: center; font-size:16px;'><b>${text}</b></div>`, 
            customClass: 'customContSWDEL', customContainerClass: "customSWDEL", 
            allowEscapeKey: false,
            confirmButtonColor: '#393939',
            showCancelButton: true,
            animation: false, allowOutsideClick: false,
            focusConfirm: false, focusCancel:  false,
            backdrop: 'rgba(103, 103, 7, .4)'
    }) .then((res) => {
        if(res.value)
            callback(res)
    })
}

export const alertError = (text) => {
    swal.fire({
        html: `<div style='font-family: Gotham-Light; text-align: center; font-size:16px; color:red;'><b>${text}</b></div>`,
        allowEnterKey: false, imageWidth: 30, imageHeight: 30,
        imageUrl: '/images/icons/error-icon.png',
        confirmButtonColor: '#B7170F',
        animation: false, allowOutsideClick: false,
        backdrop: 'rgba(200,0,0,.2)'
    })
}