export const topMenu = [
    {text: 'ACCOUNT', url: '/auth'},
    {text: 'BAG', url: '/cart'}
]

export const mainMenu = [
    {text: 'HOME', url: '/'},
    {text: 'OUR UNIVERSE', url: '/our-universe'},
    {text: 'SHOP', url: '/shop'},
    {text: 'COLLECTIONS', url: '/collections' /*url: '/collections'*/},
    {text: 'BEHIND THE BRAND', url: '/behind-the-brand'},
]

export const footerMenu = [
    /*{text: 'FAQ', url: '/faq'},
    {text: 'STOCKIST', url: '/stores'},
    {text: 'SHIPPING & RETURNS', url: '/shipping-returns'}*/
    {text: 'SIZE GUIDE & GARMENT CARE', url: '/size-guide'},
    //{text: 'GARMENT CARE', url: '/garment-care'},
    {text: 'PAYMENT METHODS', url: '/payment-methods'},
    {text: 'STORES', url: '/stores'},
    {text: 'TERMS & CONDITIONS', url: '/terms'}
]

export const socialNetworking = [
    {text: 'INSTAGRAM', url: '//www.instagram.com/ayniuniverse/', icon: '/images/icons/insta.png'},
    {text: 'FACEBOOK', url: '//www.facebook.com/ayniuniverse/', icon: '/images/icons/fb.png'}
]

// for mobile
export const socialNetworkingForMobile = [
    {text: 'INSTAGRAM', url: '//www.instagram.com/ayniuniverse/'},
    {text: 'FACEBOOK', url: '//www.facebook.com/ayniuniverse/'},
    {text: 'YOUTUBE', url: ''}
]