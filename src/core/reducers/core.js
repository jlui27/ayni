import { ACTION_TYPES } from "../actions/core";
import { defaultState } from "../state";

console.log(ACTION_TYPES);
console.log(defaultState);

export const coreReducer = (state = defaultState, action)=> {
    switch(action.type){
        case ACTION_TYPES.setModule:
            return {
                ...state,
                module: action.payload.module
            };
            break;
        case ACTION_TYPES.changeMode:
            return {
                ...state,
                mode: action.payload.mode,
                detailId: action.payload.id
            };
            break;
        default: 
            return state;
            break;
    }
};