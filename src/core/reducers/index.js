import { combineReducers } from 'redux';
import { coreReducer } from './core';

export const rootReducer = combineReducers({
    core: coreReducer
});