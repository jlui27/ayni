import React, { Component } from 'react';
//import logo from './logo.svg';
import './App.scss';
import Footer from './components/shell/footer/Footer';
import Header from './components/shell/header/Header';
import PropTypes from 'prop-types'
import SubscriptionV2 from './components/subscription/SubscriptionV2';
import swal from 'sweetalert2'

class App extends Component {
  static propTypes = {
    children: PropTypes.object.isRequired
  }

  lastUpate(){
    if (window.location.pathname === '/'){
        swal.fire({
          title: 'Ultimos cambios 14-02-2019',
          html: `<ul style='text-align: left;'>
                <li>Notificaciones y alertas agregado</li>
                <li>Backstage contenido agregado</li>
                <li>Bug editar/eliminar productos del BAG corregido</li>  
                </ul>
                <p style='text-align:left;'>Cualquier punto de vista anotarlas al Trello.<br>
                Este mensaje es temporal y será eliminado antes de la presentaciòn con AYNI.</p>`,
          allowOutsideClick: false,
          confirmButtonColor: 'black',
          type: 'info'
        })
    }
    
  }
  componentDidMount(){
    //this.lastUpate()
  }
  render() {
    console.log(process.env)
    const { children } = this.props
    return (
      <div className="App">
          <Header/>
          {children}
          <Footer/>
          <SubscriptionV2/>
      </div>
    );
  }
}

export default App;
