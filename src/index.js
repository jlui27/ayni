import React from 'react';
import { render } from 'react-dom';
import './index.scss';
//import App from './App';
import * as serviceWorker from './serviceWorker';

import { BrowserRouter as Router } from 'react-router-dom'
//import store from './core/reducers/'
import { Provider } from 'react-redux'
import { AppRoutes } from './routes';
import ScrollToTop from './features/top-scroll/ScrollToTop';
import store from './core/store';

render(
    <Provider store={store}>
        <Router>
            <ScrollToTop>
                <AppRoutes/>
            </ScrollToTop>
        </Router>
    </Provider>,
     document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
