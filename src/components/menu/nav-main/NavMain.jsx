import React, { Component } from 'react'
import './NavMain.scss'
import { mainMenu } from '../../../core/constants/menu';
import { Link } from 'react-router-dom'
import { api } from '../../../core/helpers/api';
import CollectionList from '../dropdowns/collection-list/CollectionList';
import ShopCategoryList from '../dropdowns/shop-category-list/ShopCategoryList';
import OurUniverseList from '../dropdowns/our-universe-list/OurUniverseList';

class NavMain extends Component{
    constructor(props){
        super(props)
        this.state = {
            lastCollection: 0
        }
        this.optionsWithDropDown = ['OUR UNIVERSE', 'SHOP', 'COLLECTIONS']
        this.containersID = ['our-universe-list', 'shop-category-list', 'collection-list']
    }
    //onOver(){ document.getElementById("collection-list").style.display = "block"; }
    //onOut(){ document.getElementById("collection-list").style.display = "none"; }

    onOver(execFor){
        switch(execFor){
            case 'OUR UNIVERSE': document.getElementById(this.containersID[0]).style.display = "block"; break;
            case 'SHOP': document.getElementById(this.containersID[1]).style.display = "block"; break;
            case 'COLLECTIONS': document.getElementById(this.containersID[2]).style.display = "block"; break;
            default: break;
        }
    }
    onOut(execFor){
        switch(execFor){
            case 'OUR UNIVERSE': document.getElementById(this.containersID[0]).style.display = "none"; break;
            case 'SHOP': document.getElementById(this.containersID[1]).style.display = "none"; break;
            case 'COLLECTIONS': document.getElementById(this.containersID[2]).style.display = "none"; break;
            default: break;
        }
    }

    getLastCollection(){
        api.get('/collections', [], (res) => {
            if(res){
                if(res.data){
                    const collections = res.data.data
                    this.setState({ lastCollection: collections[0].id })
                }
            }
        })
    }
    
    componentDidMount(){
        this.getLastCollection()
    }
    render(){
        const { lastCollection } = this.state
        const routesWithoutMain = ['/','/faq','/stores','/shipping-returns'] // Without NavMain v1
        var path = window.location.pathname
        return(
            <div className='nav-main' style={routesWithoutMain.indexOf(path) < 0 ?{borderBottom:'none',width:'90%'}:null}>
                <div className='nav-main_container'>
                    <div className='nav-main_container_content'>
                        {mainMenu.map((data, i) => {
                            return(
                                <div key={i} className='nav-main_container_content_option'
                                    //onMouseOver={data.text==='COLLECTIONS'?this.onOver:null} 
                                    //onMouseOut={data.text==='COLLECTIONS'?this.onOut:null}
                                    onMouseOver={this.optionsWithDropDown.indexOf(data.text)>-1?()=>this.onOver(data.text):null} 
                                    onMouseOut={this.optionsWithDropDown.indexOf(data.text)>-1?()=>this.onOut(data.text):null}
                                >    
                                    <Link to={data.url==='/collections'?`/collection/${lastCollection}`: data.url}>
                                        <span style={path===data.url?{borderBottom:'1.5px #393939 solid', fontWeight:'bold'}:null}>{data.text}</span>
                                    </Link>
                                    {data.text==='COLLECTIONS'?
                                        <div id='collection-list'>
                                            <CollectionList />
                                        </div>:
                                    data.text==='SHOP'?
                                        <div id='shop-category-list'>
                                            <ShopCategoryList/>
                                        </div>:
                                    data.text==='OUR UNIVERSE'?
                                        <div id='our-universe-list'>
                                            <OurUniverseList/>
                                        </div>:
                                    null}
                                </div>
                            )
                        })}
                    </div>
                </div>
            </div>
        )
    }
}

export default NavMain