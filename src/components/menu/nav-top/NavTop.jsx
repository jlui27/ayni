import React, { Component } from 'react'
import './NavTop.scss'
import { topMenu } from '../../../core/constants/menu';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

class NavTop extends Component{
    goTo(route){
        if(route !== '/auth')
            this.props.history.push(route)
        else
            window.location.href = route
    }
    render(){
        const { cartCounter } = this.props
        return(
            <div className='nav-top'>
                <div className='nav-top_content'>
                    {topMenu.map((data, i) => {
                        return(
                            <div className={`nav-top_content_option_${data.text}`} key={i} onClick={()=>this.goTo(data.url)}>
                                {data.text}
                                {data.text==='BAG'?
                                <div>{cartCounter}</div>:null}
                            </div>
                        )
                    })}
                </div>
                {window.location.pathname==='/'?
                <div className='nav-top_logo' onClick={()=>this.props.history.push('/')} />
                :<div className='nav-top_logotipo' onClick={()=>this.props.history.push('/')} />}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        cartCounter: state.cart.counter
    }
}

export default withRouter(connect(mapStateToProps)(NavTop))