import React, { Component  } from 'react'
import './OurUniverseList.scss';
import { withRouter } from 'react-router-dom';


class OurUniverseList extends Component{
    constructor(){
        super()
        this.goWapperDiv= this.goWapperDiv.bind(this);
    }
    goWapperDiv(ref){
       if(this.props.location.pathname != '/our-universe'){
            this.props.history.push('/our-universe')
       }
        setTimeout(()=>{
            if(ref === 'nav_1'){
                const element = document.getElementById(`${ref}`)
                element.scrollIntoView()
            }else if(ref === 'nav_2'){
                const element = document.getElementById(`${ref}`)
                element.scrollIntoView()
            }     
        },500)
       
    }
    render(){
        const options = [
            {elementId: '#', name: 'OUR STORY',ref:'nav_1'},
            {elementId: '#', name: 'FOUNDERS',ref:'nav_2'}
        ]
        return(
            <div className='our-universe-list'>
                {options.map((data, i) => {
                    return(
                        <div className='our-universe-list_content' key={i}>
                            <span onClick={()=>this.goWapperDiv(data.ref)}>{data.name}</span>
                        </div>
                    )
                })}
            </div>
        )
    }
}

export default withRouter(OurUniverseList)