import React, { Component  } from 'react'
import './ShopCategoryList.scss'
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { setShopFilter } from '../../../../core/store/actions/header';

class ShopCategoryList extends Component{
    goShopFilter(id, name){
        //const { push } = this.props.history
        //push(`/shop/${id}/${name}`) // Solo para activar filtro, quise evitar el tesioso redux
        this.props.setShopFilterParams({id, name})
        this.props.history.push('/shop')
    }
    render(){
        const { exec, list } = this.props
        return(
            <div className='shop-category-list'>
                {list?list.map((data, i) => {
                    const listStyle = exec === 'mobile' && i < list.length - 1 ? { paddingBottom: '30px', textAlign: 'center' } : null
                    return(
                        <div className='shop-category-list_content' style={listStyle} key={i}>
                            <span onClick={()=>this.goShopFilter(data.id, data.name)}>{data.name}</span>
                        </div>
                    )
                }):null}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        list: state.shopCategories
    }
}
const mapDispatchToProps = dispatch => {
    return {
        setShopFilterParams(params){
            dispatch(setShopFilter(params))
        }
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ShopCategoryList))