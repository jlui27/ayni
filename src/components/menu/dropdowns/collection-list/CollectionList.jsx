import React from 'react'
import './CollectionList.scss'
//import { api } from '../../../core/helpers/api';
import { Link } from 'react-router-dom'
import { connect } from 'react-redux';

class CollectionList extends React.Component{
    render(){
        const { exec, list } = this.props
        return(
            <div className='collection-list'>
                {list?list.map((data, i) => {
                    const listStyle = exec === 'mobile' && i < list.length - 1 ? { paddingBottom: '30px', textAlign: 'center' } : null
                    return(
                        <div className='collection-list_content' style={listStyle} key={i}>
                            <span><Link to={'/collection/'+data.id}>{data.name}</Link></span>
                        </div>
                    )
                }):null}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return{
        list: state.collections
    }
}

export default connect(mapStateToProps)(CollectionList)