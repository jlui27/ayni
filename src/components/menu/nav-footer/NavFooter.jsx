import React, {Component} from 'react'
import './NavFooter.scss'
import { footerMenu } from '../../../core/constants/menu';
import { Link } from 'react-router-dom'

class NavFooter extends Component{
    render(){
        const selected = { fontWeight: 'bold' }
        const currentPath = window.location.pathname
        return(
            <div className='nav-footer'>
                <div className='nav-footer_content'>
                    {currentPath === '/shop' ? footerMenu.map((data, i) => {
                        return(
                            <div key={i} className='nav-footer_content_option'><Link to={data.url} style={currentPath===data.url?selected:null}>{data.text}</Link></div>
                        )
                    }) : null}
                </div>
            </div>
        )
    }
}

export default NavFooter;