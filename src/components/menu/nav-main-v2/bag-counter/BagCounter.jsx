import React, { Component } from 'react'
import './BagCounter.scss'
import { connect } from 'react-redux';
import { topMenu } from '../../../../core/constants/menu';
import { withRouter, Link } from 'react-router-dom'
import { updateCartCounter } from '../../../../core/store/actions/cart';
import { setLastCollection } from '../../../../core/store/actions/header';

/* CART OR BAG ARE THE SAME */

class BagCounter extends Component{
    setLastCollectionIntoRedux(props){
        // En observacion
        const { lastCollection, lastCollectionSetter } = props
        if(lastCollection)
            lastCollectionSetter(lastCollection)
    }

    componentWillReceiveProps(props){
        // En observacion
        this.setLastCollectionIntoRedux(props)
    }
    render(){
        const { cartCounter } = this.props
        //console.log('last collection id: ', lastCollectionId)
        return(
            <div className='bag-counter'>
                {topMenu.map((data, i) => {
                    return(
                        <span className={`bag-counter_${data.text}`} key={i}>
                            {window.location.pathname.substring(0,8) === '/profile' || data.url === '/cart' ? // temporal
                                <Link to={data.url}>
                                    {data.text}
                                    {data.text==='BAG'?
                                    <div>{cartCounter}</div>:null}
                                </Link>
                            :
                                <a href={data.url}>
                                    {data.text}
                                    {data.text==='BAG'?
                                    <div>{cartCounter}</div>:null}
                                </a>
                            }
                        </span>
                        )
                })}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        cartCounter: state.cart.counter,
        lastCollectionId: state.lastCollection
    }
}
const mapDispatchToProps = dispatch => {
    return {
        lastCollectionSetter(id){
            dispatch(setLastCollection(id))
        }
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(BagCounter)