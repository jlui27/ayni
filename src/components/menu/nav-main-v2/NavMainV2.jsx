import React from 'react'
import './NavMainV2.scss'
import { mainMenu } from '../../../core/constants/menu';
import { Link } from 'react-router-dom'
import CollectionList from '../dropdowns/collection-list/CollectionList';
import BagCounter from './bag-counter/BagCounter';
import { api } from '../../../core/helpers/api';
import ShopCategoryList from '../dropdowns/shop-category-list/ShopCategoryList';
import OurUniverseList from '../dropdowns/our-universe-list/OurUniverseList';

class NavMainV2 extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            lastCollection: 0
        }
        this.optionsWithDropDown = ['OUR UNIVERSE', 'SHOP', 'COLLECTIONS']
        this.containersID = ['our-universe-list', 'shop-category-list', 'collection-list']
    }
    //onOver(){ document.getElementById("collection-list").style.display = "block"; }
    //onOut(){ document.getElementById("collection-list").style.display = "none"; }
    onOver(execFor){
        switch(execFor){
            case 'OUR UNIVERSE': document.getElementById(this.containersID[0]).style.display = "block"; break;
            case 'SHOP': document.getElementById(this.containersID[1]).style.display = "block"; break;
            case 'COLLECTIONS': document.getElementById(this.containersID[2]).style.display = "block"; break;
            default: break;
        }
    }
    onOut(execFor){
        switch(execFor){
            case 'OUR UNIVERSE': document.getElementById(this.containersID[0]).style.display = "none"; break;
            case 'SHOP': document.getElementById(this.containersID[1]).style.display = "none"; break;
            case 'COLLECTIONS': document.getElementById(this.containersID[2]).style.display = "none"; break;
            default: break;
        }
    }
    getLastCollectionList(){
        api.get('/collections', [], (res) => {
            if(res){
                if(res.data){
                    const collections = res.data.data
                    this.setState({ lastCollection: collections[0].id })
                }
            }
        })
    }
    componentDidMount(){
        this.getLastCollectionList()
    }
    render(){
        const { lastCollection } = this.state
        var currentPath = window.location.pathname
        const menuSelectedStyle = { borderBottom: '1.5px #1c1c1a solid', fontWeight: 'bold' }
        return(
            <div className='nav-main-v2'>
                <Link to='/' className='nav-main-v2_a'><div className="nav-main-v2_logo" /></Link>
                <div className="nav-main-v2_menu">
                    {mainMenu.map((data, i) => {
                        return(
                            <span key={i} 
                            //onMouseOver={data.text==='COLLECTIONS'?this.onOver:null} 
                            //onMouseOut={data.text==='COLLECTIONS'?this.onOut:null}
                            onMouseOver={this.optionsWithDropDown.indexOf(data.text)>-1?()=>this.onOver(data.text):null} 
                            onMouseOut={this.optionsWithDropDown.indexOf(data.text)>-1?()=>this.onOut(data.text):null}
                            >
                                <Link style={window.location.pathname===data.url?menuSelectedStyle:null} 
                                to={data.url==='/collections'?`/collection/${lastCollection}`:data.url}>
                                    {data.text}
                                </Link>
                                {data.text==='COLLECTIONS'?
                                    <div id='collection-list'>
                                    <CollectionList />
                                    </div>:
                                data.text==='SHOP'?
                                    <div id='shop-category-list'>
                                        <ShopCategoryList/>
                                    </div>:
                                data.text==='OUR UNIVERSE'?
                                    <div id='our-universe-list'>
                                        <OurUniverseList/>
                                    </div>:
                                null}
                            </span>
                        )
                    })}
                </div>
                <div className="nav-main-v2_sm-menu">
                    <BagCounter lastCollection={lastCollection} />
                </div>
            </div>
        )
    }
}

export default NavMainV2 //No puede tener redux o pierde efectos de seleccion