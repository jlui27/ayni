import React, { Component } from 'react'
import './NavTopMobile.scss'
import { topMenu, mainMenu } from '../../../core/constants/menu';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import $ from 'jquery'
import ModalCollectionLookbook from '../../modal-collection-lookbook/ModalCollectionLookbook';
import { setShopFilter } from '../../../core/store/actions/header';

const initState = {
    isDown: false, collectionIsDown: false,
    isDownOption: { universe: false, shop: false, collection: false }
}

class NavTopMobile extends Component{
    constructor(){
        super()
        this.state = initState
        this.dropDown = this.dropDown.bind(this)
        this.go=this.go.bind(this)
        this.goToCollection=this.goToCollection.bind(this)
        this.onCollDropDown=this.onCollDropDown.bind(this)
        this.goWapperDiv=this.goWapperDiv.bind(this)

        this.dropdowns = ['OUR UNIVERSE', 'SHOP', 'COLLECTIONS']
    }

    goWapperDiv(ref){
        if(this.props.location.pathname != '/our-universe'){
             this.props.history.push('/our-universe')
        }
         setTimeout(()=>{
            this.setState({isDown:false})
             if(ref === 'nav_1'){
                 const element = document.getElementById(`${ref}`)
                 element.scrollIntoView()
             }else if(ref === 'nav_2'){
                 const element = document.getElementById(`${ref}`)
                 element.scrollIntoView()
             }     
         },500)
        
     }

    dropDown(){
        const { isDown } = this.state
        this.setState({ isDown: !isDown, collectionIsDown: false, isDownOption: { universe:false, shop:false, collection:false } })
        
    }
    closeMenuOutside(){
        document.addEventListener("click", (evt) => {
            const flyoutElement = document.getElementById("nav-top-mobile_container");
            let targetElement = evt.target; // clicked element
        
            do {
                if (targetElement == flyoutElement) {
                    // Do nothing, just return.
                    // clicked inside
                    return;
                }
                // Go up the DOM.
                targetElement = targetElement.parentNode;
            } while (targetElement);
            // Do something useful here.
            // clicked outside 
            const { isDown } = this.state
            if(isDown)
                this.setState({ isDown: false })
        });
    }
    go(route){
        this.setState( initState )
        this.props.history.push(route)
    }
    setShop(params){
        // Go to shop with redux shop filter
        this.props.setShopFilterParams(params)
        this.props.history.push('/shop')
        this.setState(initState)
    }

    onCollDropDown(to){
        //const { collectionIsDown } = this.state
        //this.setState({ collectionIsDown: !collectionIsDown })
        const { isDownOption } = this.state
        switch(to){
            case 0: this.setState({ isDownOption: { universe: !isDownOption.universe, shop: false, collection: false } }); break;
            case 1: this.setState({ isDownOption: { universe: false, shop: !isDownOption.shop, collection: false } }); break;
            case 2: this.setState({ isDownOption: { universe: false, shop: false, collection: !isDownOption.collection } }); break;
            default: break;
        }
    }
    goToCollection(id){
        this.props.history.push(`/collection/${id}`)
        this.setState(initState)
    }
    disableScroll(){
        $('html').css({
            overflow: 'hidden',
            height: '100%'
        })
    }
    enableScroll(){
        $('html').css({
            overflow: 'auto',
            height: 'auto'
        })
    }

    componentDidMount(){
        this.closeMenuOutside()
    }
    componentDidUpdate(){
        const { isDown } = this.state
        if(isDown)
            this.disableScroll()
        else
            this.enableScroll()
    }
    render(){
        const { cartCounter, collectionList, shopCategories, setShopFilterParams } = this.props
        const { isDown, collectionIsDown, isDownOption } = this.state
        const currentPath = window.location.pathname
        const disableBackground = isDown ? { height: '100%', backgroundColor: 'rgba(0,0,0,.2)' } : null
        const dropDownStyle = isDown ? { transform: 'translateY(0)', paddingTop: '15px' } : null
        const visible = { visibility: 'hidden', maxHeight: 0 }
        const showCollectionList = isDownOption.collection ? null : visible
        const showUniverseList = isDownOption.universe ? null : visible
        const showShopCategory = isDownOption.shop ? null : visible
        const padTop = { paddingTop: '15px' }
        const options = [
            {name: 'OUR STORY',ref:'nav_1'},
            {name: 'FOUNDERS',ref:'nav_2'}
        ]
        //console.log('shop categories content.. : ', shopCategories)
        return(
            <div className='nav-top-mobile' style={disableBackground}>
                {currentPath.substring(0,8) !== '/product' ?
                <div className='nav-top-mobile_container' id='nav-top-mobile_container'>
                    <div className='nav-top-mobile_container_header'>
                        <button onClick={this.dropDown}><i className='material-icons'>{isDown?'close':'menu'}</i></button>
                        <div className='nav-top-mobile_container_header_smenu'>
                            {topMenu.map((data, i) => {
                                return(
                                    <div className={`nav-top-mobile_container_header_smenu_option_${data.text}`} key={i} onClick={()=>this.go(data.url)}>
                                        {data.text}
                                        {data.text==='BAG'?
                                        <div>{cartCounter}</div>:null}
                                    </div>
                                )
                            })}
                        </div>
                    </div>
                    <div className='nav-top-mobile_container_body' style={dropDownStyle}>
                        {mainMenu.map((data, i) => {
                            const selected = window.location.pathname === data.url ? { fontWeight: '600', textDecoration: 'underline' } : null
                            return(
                                <div key={i} className='nav-top-mobile_container_body_option'>
                                    { this.dropdowns.indexOf(data.text) < 0 ? 
                                        <div onClick={()=>this.go(data.url)} style={selected}>{data.text}</div> :
                                    data.text === 'OUR UNIVERSE' ?
                                        <div className='nav-top-mobile_container_body_option_universe'>
                                            <div className='nav-top-mobile_container_body_option_universe_label' onClick={()=>this.onCollDropDown(0)}>
                                                {data.text} <i className='material-icons'>{isDownOption.universe?'keyboard_arrow_up':'keyboard_arrow_down'}</i>
                                            </div>
                                            <div className='nav-top-mobile_container_body_option_universe_list' style={showUniverseList}>
                                                {options.map((data, i) => {
                                                    return(
                                                        <div className='nav-top-mobile_container_body_option_universe_list_content' key={i} style={padTop}>
                                                            <span onClick={()=>this.goWapperDiv(data.ref)}>{data.name}</span>
                                                        </div>
                                                    )
                                                })}
                                            </div>
                                        </div>:
                                    data.text === 'SHOP' ?
                                        <div className='nav-top-mobile_container_body_option_shop'>
                                            <div className='nav-top-mobile_container_body_option_shop_label' onClick={()=>this.onCollDropDown(1)}>
                                                {data.text} <i className='material-icons'>{isDownOption.shop?'keyboard_arrow_up':'keyboard_arrow_down'}</i>
                                            </div>
                                            <div className='nav-top-mobile_container_body_option_shop_list' style={showShopCategory}>
                                                {shopCategories || shopCategories.length > 0 ? shopCategories.map((data, i) => {
                                                    const { id, name } = data
                                                    return(
                                                        <div className='nav-top-mobile_container_body_option_shop_list_content' key={i} style={padTop}>
                                                            <span onClick={()=>this.setShop({id, name})}>{name}</span>
                                                        </div>
                                                    )
                                                }) : null}
                                            </div>
                                        </div>:
                                    data.text === 'COLLECTIONS' ?
                                    <div className='nav-top-mobile_container_body_option_collection'>
                                        <div className='nav-top-mobile_container_body_option_collection_label' onClick={()=>this.onCollDropDown(2)}>
                                            {data.text} <i className='material-icons'>{isDownOption.collection?'keyboard_arrow_up':'keyboard_arrow_down'}</i>
                                        </div>
                                        <div className='nav-top-mobile_container_body_option_collection_list' style={showCollectionList}>
                                            {collectionList || collectionList.length > 0 ? collectionList.map((data, i) => {
                                                //id = data.id
                                                return(
                                                    <div className='nav-top-mobile_container_body_option_collection_list_content' key={i} style={padTop} >
                                                        <span onClick={()=>this.goToCollection(data.id)}>{data.name}</span>
                                                    </div>
                                                )
                                            }) : null}
                                        </div>
                                    </div> : null }
                                </div>
                            )
                        })}
                    </div>
                </div>
                :
                <div className='nav-top-mobile_goback' onClick={()=>this.props.history.push('/shop')}>
                    <div className='nav-top-mobile_goback_image'/>
                </div> 
                }

                {currentPath.substring(0,11) === '/collection' ? <ModalCollectionLookbook/> : null }
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        cartCounter: state.cart.counter,
        collectionList: state.collections,
        shopCategories: state.shopCategories
    }
}
const mapDispatchToProps = dispatch => {
    return {
        setShopFilterParams(params){
            dispatch(setShopFilter(params))
        }
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(NavTopMobile))