import React from 'react'
import './NavSmall.scss'
import { withRouter } from 'react-router-dom';

class NavSmall extends React.Component{
    constructor(){
        super()
        this.state={
            optionSelected: 0, selectedStyle: ''
        }
        this.onSelect=this.onSelect.bind(this)
        this.mediaQexecute=this.mediaQexecute.bind(this)
    }
    onSelect(i, route){
        this.setState({ optionSelected: i })
        if(route==='/profile/order-records')
            window.location.href = route
        else
            this.props.history.push(route)
    }
    mediaQexecute(x) {
        if (x.matches) { // If media query matches
            this.setState({ selectedStyle: { fontFamily: 'OpenSans-Light' } })
        } else {
            this.setState({ selectedStyle: { textDecoration: 'underline' } })
        }
    }
    mediaQChecker(){
        var x = window.matchMedia("(max-width: 800px)")
        this.mediaQexecute(x) // Call listener function at run time
        x.addListener(this.mediaQexecute) // Attach listener function on state changes
    }
    componentDidMount(){
        this.setState({ optionSelected: this.props.options.length - 1 })
        this.mediaQChecker()
    }
    render(){
        const { options } = this.props
        const { optionSelected, selectedStyle } = this.state
        // styles
        //const underLine = { textDecoration: 'underline' }
        const selected = selectedStyle ? selectedStyle : null
        return(
            <div className='nav-small'>
                <div className='nav-small_content'>
                    {options.map((data, i) => {
                        return(
                            <div className='nav-small_content_option' key={i}>
                                <span style={optionSelected===i?selected:null} onClick={optionSelected!==i?()=>this.onSelect(i, data.route):null}>{data.text}</span>
                                {i!==this.props.options.length-1?
                                <div><hr/></div>:null}
                            </div>
                        )
                    })}
                </div>
            </div>
        )
    }
}


export default withRouter(NavSmall)