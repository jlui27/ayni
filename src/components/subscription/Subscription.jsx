import React, { Component } from 'react'
import './Subscription.scss'

class Subscription extends Component{
    constructor(){
        super()
        this.state={
            validEmail: false
        }
        
        this.onType=this.onType.bind(this)
        this.sendMail=this.sendMail.bind(this)
    }
    onType(e){
        const { validEmail } = this.state
        const email = e.target.value
        const message = document.getElementById('subscription_message')
        var emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
        if(email){
            if (emailRegex.test(email)) {
                message.innerText = "Your email is valid";
                message.style.color='green'
                if(validEmail!==true)
                    this.setState({ validEmail: true })
            } else {
                message.innerText = "Invalid email";
                message.style.color='red'
                if(validEmail!==false)
                    this.setState({ validEmail: false })
            }
        } else {
            message.innerText = ''
            if(validEmail!==false)
                this.setState({ validEmail: false })
        }
    }
    sendMail(){
        const { validEmail } = this.state
        const email = document.getElementById('email').value
        const message = document.getElementById('subscription_message')
        if(email){
            if(validEmail===true){
                message.innerText='Subscribed ✔'
                message.style.color='green'
                /* Actiar en backend */
            } else {
                message.innerText = "Invalid email, please check it";
                message.style.color='red'
            }
        } else {
            message.innerText='Complete!!'
            message.style.color='red'
            setTimeout(() => { message.innerText='' }, 1500);
        }
    }
    render(){
        return(
            <div className='subscription'>
                <h2>SUSCRIBE</h2>
                <div className='subscription_field'>
                    <input type="email" id="email" placeholder='Enter your email' onChange={(e)=>this.onType(e)} />
                    <button onClick={this.sendMail} />
                </div>
                <i id='subscription_message'></i>
                <span>Be the first to receive our latest trends.</span>
            </div>
        )
    }
}

export default Subscription;