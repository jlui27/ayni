import React, { Component } from 'react'
import './SubscriptionV2.scss'
import $ from 'jquery'
import { api } from '../../core/helpers/api';

class SubscriptionV2 extends Component{
    constructor(props){
        super(props)
        this.state = {
            modal: false, validEmail: false
        }
        this.onType=this.onType.bind(this)
        this.sendMail=this.sendMail.bind(this)
    }
    onType(e){
        const { validEmail } = this.state
        const email = e.target.value
        const message = document.getElementById('subscription_message_v2')
        var emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
        if(email){
            if (emailRegex.test(email)) {
                message.innerText = "Your email is valid";
                message.style.color='green'
                if(validEmail!==true)
                    this.setState({ validEmail: true })
            } else {
                message.innerText = "Invalid email";
                message.style.color='red'
                if(validEmail!==false)
                    this.setState({ validEmail: false })
            }
        } else {
            message.innerText = ''
            if(validEmail!==false)
                this.setState({ validEmail: false })
        }
    }
    sendMail(){
        const { validEmail } = this.state
        const email = document.getElementById('emailv2').value
        const message = document.getElementById('subscription_message_v2')
        if(email){
            if(validEmail===true){
                api.post('/mail-subscription', {email}, (res) => {
                    if(res.data){
                        if(res.data.status === 'success'){
                            message.innerText='Subscribed ✔'
                            message.style.color='green'
                            /* Actiar en backend */
                        }
                    }
                })
            } else {
                message.innerText = "Invalid email, please check it";
                message.style.color='red'
            }
        } else {
            message.innerText='Complete!!'
            message.style.color='red'
            setTimeout(() => { message.innerText='' }, 1500);
        }
    }
    catchEscapeKey(){
        $(document).keydown((e) => {
            var code = e.keyCode || e.which;
            //alert(code);
            if(code === 27){
                this.setState({ modal: false })
            }
        })
    }

    componentDidMount(){
        if(window.location.pathname==='/'){
            setTimeout(() => { this.setState({ modal: true }) }, 3000);
        }
        this.catchEscapeKey()
    }
    render(){
        const { modal } = this.state
        const modalActive = modal===false?{display: 'none'}:null
        return(
            <div className='subscription-v2' style={modalActive}>
                <div className='subscription-v2_container'>
                    <div className='subscription-v2_container_content'>
                        <div className='subscription-v2_container_content_close'><button onClick={()=>this.setState({modal:false})}>X</button></div>
                        <div className='subscription-v2_container_content_logo'></div>
                        <div className='subscription-v2_container_content_title'>JOIN OUR UNIVERSE</div>
                        <label>Be the first to receive our latest trends,<br/>newest styles and our special offers</label>
                        <i id='subscription_message_v2'></i>
                        <input type="email" id="emailv2" placeholder='ENTER YOUR EMAIL' onChange={(e)=>this.onType(e)} />
                        <button className='subscription-v2_container_content_suscribe'  onClick={this.sendMail}>SUBSCRIBE</button>
                        <div className='subscription-v2_container_content_footer'>
                            <span className='subscription-v2_container_content_footer_t1'>We respect your privacy.</span>
                            <span className='subscription-v2_container_content_footer_t2'>Your information is safe and will never be shared.</span>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default SubscriptionV2