import React, { Fragment, createContext } from 'react'
import './ProdPreview.scss'
import { toMoney } from '../../core/helpers/formats';
import Swiper from 'react-id-swiper';

class ProdPreview extends React.Component{
    constructor(props){
        super(props);
        this.nextSli=this.nextSli.bind(this);
        this.swiper =  React.createRef();
    }
    backSli(){ if (this.swiper) this.swiper.current.swiper.slidePrev() }
    nextSli(){ if (this.swiper) this.swiper.current.swiper.slideNext() }
    
    render(){
        const { products, width, limit } = this.props
        const swiperParams = {
            slidesPerView: limit,
            spaceBetween: 50,
            pagination: {
              el: '.swiper-pagination',
              type: 'bullets',
              clickable: true
            },
            breakpoints: {
                // mobile
                800: {
                    slidesPerView: 2,
                    spaceBetween: 20
                }
            },
          }
        return(   
                    !!products && products.length > 0 ?
                    <Fragment>
                    <Swiper {...swiperParams} ref={this.swiper}>
                        {products.map((data, i) => {
                                return(
                                    <div key={i}>
                                        <div className='prod-preview_content_product' key={i}>
                                            <a href={`/product/${data.id}`}>
                                                <img src={data.images[0]} alt=""/>
                                            </a>
                                            <div className='prod-preview_content_product_text'>
                                                <span>{data.name?data.name:data.title}</span>
                                                <b>$ {toMoney(data.price)}</b>
                                            </div>
                                        </div>
                                    </div>
                                );
                        })}
                    </Swiper>
                    <div className='container_buttons'>
                        <button onClick={()=>this.backSli()}>{'<'}</button>
                        <button onClick={()=>this.nextSli()}>{'>'}</button>
                    </div>
                    </Fragment> : <div style={{paddingTop: '40px', color: 'darkgray', fontFamily: 'Gotham-Book'}}>Empty</div>
                    
        )
    }
}

export default ProdPreview