import React from 'react'
import './Header.scss'
import NavTop from '../../menu/nav-top/NavTop';
import NavMainV2 from '../../menu/nav-main-v2/NavMainV2';
import Notifications from 'react-notify-toast';
import NavTopMobile from '../../menu/nav-top-mobile/NavTopMobile';
import HeaderStarter from './HeaderStarter';

class Header extends React.Component{
    constructor(props){
        super(props)
        this.onlyFor = ['/profile']
        this.excluded = ['/product']
    }
    render(){
        const currentPath = window.location.pathname
        const stickyFor = this.onlyFor.indexOf(currentPath.substring(0,8)) > -1 ? { position:'sticky', top:'0', backgroundColor: 'white' } : null
        return(
            <div className='header' style={stickyFor}>
            <HeaderStarter/>
            <Notifications/>
                <div className='header_desktop'>
                    { this.excluded.indexOf(window.location.pathname.substring(0,8)) < 0 ?
                    this.onlyFor.indexOf(currentPath.substring(0,8)) < 0 ? <NavTop/> : <NavMainV2/> 
                    : null }
                </div>
                <div className='header_mobile'>
                    <NavTopMobile/>
                    {this.excluded.indexOf(currentPath.substring(0,8)) < 0 && this.onlyFor.indexOf(currentPath.substring(0,8)) < 0 ? 
                        <div className='header_mobile_logo'>
                            <div className='header_mobile_logo_image'/>
                        </div>
                    : null}
                </div>
            </div>
        )
    }
}

export default Header