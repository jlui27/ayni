import { Component } from 'react'
import { connect } from 'react-redux';
import { api } from '../../../core/helpers/api';
import { updateCartCounter, setSessionCart } from '../../../core/store/actions/cart';
import { setCollections, updateAppIsMobile, setShopCategories } from '../../../core/store/actions/header';
import $ from 'jquery'

const MOBILE_RESOLUTION = '800px'

const collListTest = [
    {name: 'FIRST COLLECTION'}, {name: 'SECOND COLLECTION'}, {name: 'THIRD COLLECTION'}, {name: 'THIRD COLLECTION'},
    {name: 'FIRST COLLECTION'}, {name: 'SECOND COLLECTION'}, {name: 'THIRD COLLECTION'}, {name: 'THIRD COLLECTION'}
]

class HeaderStarter extends Component{
    constructor(){
        super()
        this.mediaQueryExecute=this.mediaQueryExecute.bind(this)
    }
    checkCartCounter(){
        const localCart = localStorage.getItem(process.env.REACT_APP_LOCAL_CART)
        const uid = localStorage.getItem(process.env.REACT_APP_LOCAL_UID)
        if(uid)
            this.setSessionCartCounter()
        else
            this.setLocalCartCounter(localCart)
    }
    setLocalCartCounter(localCart){
        if(localCart){
            var cart = JSON.parse(localCart)
            var count = cart.length
            const { updateCartCounter } = this.props
            updateCartCounter(count)
        }
    }
    setSessionCartCounter(){
        const { updateCartCounter, updateSessionCart } = this.props
        api.get('/carts/me', '', (res) => {
            if(res.data){
                var cartString = res.data.data?res.data.data.shoppingCartJson?res.data.data.shoppingCartJson:null:null
                var cart = cartString?JSON.parse(cartString):[]
                updateCartCounter(cart.length)
                updateSessionCart(cart)
                this.concatCartData(cart)
            }
        })
    }
    concatCartData(cart){
        const { updateCartCounter, updateSessionCart } = this.props
        const local = localStorage.getItem(process.env.REACT_APP_LOCAL_CART)
        const uid = localStorage.getItem(process.env.REACT_APP_LOCAL_UID)
        const localCart = local ? JSON.parse(local) : []
        if(localCart.length>0){
            var newCart = cart.concat(localCart)
            api.post(`/users/${uid}/shoppingCart`, { shoppingCartJson: newCart }, (res) => {
                console.log('ses cart concat', res)
                if(res.data){
                    if(res.data.status === 'success'){
                        updateCartCounter(newCart.length)
                        updateSessionCart(newCart)
                        localStorage.removeItem(process.env.REACT_APP_LOCAL_CART)
                    } else {
                        alert('Error: Cart was not updated')
                    }
                }
            })
        }
    }
    getCollections(){
        const { setCollectionList } = this.props
        api.get('/collections', '', (res) => {
            if(res.data)
                setCollectionList(res.data.data)
                //setCollectionList(collListTest)
        })
    }
    getShopCategories(){
        api.get('/categories', '', (res) => {
            if(res)
                if(res.data)
                    if(res.data.status === 'success')
                        this.props.setShopCategoryList(res.data.data)
        })
    }
    // Media Query
    mediaQueryExecute(x) {
        const { setAppIsMobile } = this.props
        if (x.matches) { // If media query matches
            setAppIsMobile(true)
        } else {
            setAppIsMobile(false)
        }
    }
    mediaQueryChecker(){
        var x = window.matchMedia(`(max-width: ${MOBILE_RESOLUTION})`)
        this.mediaQueryExecute(x) // Call listener function at run time
        x.addListener(this.mediaQueryExecute) // Attach listener function on state changes
    }
    //disableScroll(){ $('html').css({ overflow: 'hidden', height: '100%' }) }
    //enableScroll(){ $('html').css({ overflow: 'auto', height: 'auto' }) }
    
    componentDidMount(){
        this.checkCartCounter()
        this.getCollections()
        this.getShopCategories()
        this.mediaQueryChecker()
    }
    render(){ return false }
}

const mapDispatchToProps = dispatch => {
    return {
        updateCartCounter(count){
            dispatch(updateCartCounter(count))
        },
        updateSessionCart(data){
            dispatch(setSessionCart(data))
        },
        setCollectionList(data){
            dispatch(setCollections(data))
        },
        setShopCategoryList(data){
            dispatch(setShopCategories(data))
        },
        setAppIsMobile(bool){
            dispatch(updateAppIsMobile(bool))
        }
    }
}

export default connect(null, mapDispatchToProps)(HeaderStarter)