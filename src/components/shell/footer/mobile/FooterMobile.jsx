import React, { Component } from 'react'
import './FooterMobile.scss'
import { withRouter } from 'react-router-dom';
import { mainMenu, socialNetworkingForMobile, footerMenu } from '../../../../core/constants/menu';
import { connect } from 'react-redux';
import { api } from '../../../../core/helpers/api';

class FooterMobile extends Component{
    constructor(){
        super()
        this.state={ validEmail: false }
        this.onType=this.onType.bind(this)
        this.sendMail=this.sendMail.bind(this)
    }
    onType(e){
        const { validEmail } = this.state
        const email = e.target.value
        const message = document.getElementById('subscription_message_footer')
        var emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
        if(email){
            if (emailRegex.test(email)) {
                message.innerText = "Your email is valid";
                message.style.color='green'
                if(validEmail!==true)
                    this.setState({ validEmail: true })
            } else {
                message.innerText = "Invalid email";
                message.style.color='red'
                if(validEmail!==false)
                    this.setState({ validEmail: false })
            }
        } else {
            message.innerText = ''
            if(validEmail!==false)
                this.setState({ validEmail: false })
        }
    }
    sendMail(){
        const { validEmail } = this.state
        const email = document.getElementById('email_footer').value
        const message = document.getElementById('subscription_message_footer')
        if(email){
            if(validEmail===true){
                api.post('/mail-subscription', {email}, (res) => {
                    if(res.data){
                        if(res.data.status === 'success'){
                            message.innerText='Subscribed ✔'
                            message.style.color='green'
                            /* Actiar en backend */
                        }
                    }
                })
            } else {
                message.innerText = "Invalid email, please check it";
                message.style.color='red'
            }
        } else {
            message.innerText='Complete!!'
            message.style.color='red'
            setTimeout(() => { message.innerText='' }, 1500);
        }
    }
    render(){
        const { history, lastCollectionId } = this.props
        return(
            <div className='footer-mobile'>
                <div id='subscription_message_footer'></div>
                <div className='footer-mobile_subscription'>
                    <input type="email" id="email_footer" placeholder='ENTER YOUR EMAIL ADDRESS TO SUBSCRIBE' onChange={(e)=>this.onType(e)} />
                    <button onClick={this.sendMail}></button>
                </div>
                <div className='footer-mobile_social'>
                    {socialNetworkingForMobile.map((data, i) => {
                        return(
                            <span onClick={()=>window.open(data.url)} key={i}>{data.text}</span>
                        )
                    })}
                </div>
                <div className='footer-mobile_routes'>
                    {mainMenu.map((data, i) => {
                        const route = data.url !== '/collections' ? data.url : `/collection/${lastCollectionId}`
                        if(data.url !== '/')
                            return( <div key={i}><span onClick={()=>history.push(route)}>{data.text}</span></div> )
                    })}
                </div>
                <div className='footer-mobile_sroutes'>
                    {window.location.pathname === '/shop' ? footerMenu.map((data, i) => {
                        return( <div className={`footer-mobile_sroutes_${i}`} key={i}><span onClick={()=>history.push(data.url)}>{data.text}</span></div> )
                    }) : null }
                </div>
                <div className='footer-mobile_copyright'>© 2019 AYNI</div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return{ lastCollectionId: state.lastCollection }
}

export default withRouter(connect(mapStateToProps)(FooterMobile))