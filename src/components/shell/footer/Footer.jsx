import React, { Component } from 'react'
import './Footer.scss'
import NavFooter from '../../menu/nav-footer/NavFooter';
import { socialNetworking, mainMenu } from '../../../core/constants/menu';
import { Link, withRouter } from 'react-router-dom';
import Subscription from '../../subscription/Subscription';
import NavMain from '../../menu/nav-main/NavMain';
import StickyFooter from 'react-sticky-footer'
import NavMainV2 from '../../menu/nav-main-v2/NavMainV2';
import $ from 'jquery'
import FooterMobile from './mobile/FooterMobile';

class Footer extends Component{
    constructor(){
        super()
        this.state = {
            isMobile: false
        }
        this.mediaQueryExecute=this.mediaQueryExecute.bind(this)
        // global
        this.routeOptions = [
            {text: 'INSTAGRAM', url: ''},
            {text: 'FACEBOOK', url: ''},
            {text: 'YOUTUBE', url: ''}
        ]
        // to evaluate
        this.deniedNavFor = ['/' /*, '/faq', '/stores', '/shipping-returns', '/checkout'*/ ]
        this.deniedNavStickyForUser = ['/profile']
        this.allowNavFooterFor = ['/our-universe', '/']
    }
    scrollerChecker(){
        this.forceUpdate()
        if(this.deniedNavStickyForUser.indexOf(window.location.pathname.substring(0,8)) < 0){
            $(window).scroll(function() {
                var scroll = $(document).height() - $(window).height() - $(window).scrollTop()
                if(scroll > 450){  
                    $('.nav-main-v2_logo').css({'opacity': '1'})
                    $('.nav-main-v2_a').css({'pointer-events':'painted'})
                    $('.nav-main-v2_sm-menu').css({'opacity': '1', 'pointer-events':'painted'})
                } else {
                    $('.nav-main-v2_logo').css({'opacity': '0', 'pointer-events': 'none' })
                    $('.nav-main-v2_a').css({'pointer-events':'none'})
                    $('.nav-main-v2_sm-menu').css({'opacity': '0', 'pointer-events': 'none' })
                }
            })
        }
    }
    mediaQueryExecute(x) {
        if (x.matches) { // If media query matches
            this.setState({ isMobile: true })
        } else {
            this.setState({ isMobile: false })
        }
    }
    mediaQueryChecker(){
        var x = window.matchMedia("(max-width: 800px)")
        this.mediaQueryExecute(x) // Call listener function at run time
        x.addListener(this.mediaQueryExecute) // Attach listener function on state changes
    }
    componentDidMount(){
        this.scrollerChecker()
        this.mediaQueryChecker()
        this.props.history.listen(() => this.scrollerChecker() )
    }
    render(){
        const { isMobile } = this.state
        const logo1 = { backgroundImage: `url(${process.env.REACT_APP_URL_BASE}/images/fixed/logoAY.png)`}
        const logo2 = { backgroundImage: `url(${process.env.REACT_APP_URL_BASE}/images/fixed/logotipo.jpg)` }
        //console.log('is mobile from ffooteR: ', isMobile)
        const currentPath = window.location.pathname
        return(
            <div className='footer'>
                {this.props.location.pathname.substring(0,8) === '/product'? null :
                <div className='footer_container' style={window.location.pathname==='/'?{paddingTop: '120px'}:null}>
                    {/* {this.allowNavFooterFor.indexOf(window.location.pathname) < 0 ? <Subscription/>: null} */}
                    {this.deniedNavFor.indexOf(window.location.pathname) < 0 && this.deniedNavStickyForUser.indexOf(currentPath.substring(0,8)) < 0 ?
                        
                    <StickyFooter
                    bottomThreshold={440}
                    normalStyles={{
                        with: '100%',
                    }}
                    stickyStyles={{
                    backgroundColor: "rgba(255,255,255,.8)",
                    width: '100%',
                    height: `${isMobile?'0px':'50px'}`,
                    zIndex: '1'
                    }}
                > <NavMainV2/> </StickyFooter>  

                    
                    : null}
                    {this.deniedNavFor.indexOf(window.location.pathname) < 0 ? <div className='footer_container_line'/> : null}
                    <NavFooter/>
                    <div className='footer_container_logo1' style={logo1} onClick={()=>this.props.history.push('/')} />
                    <div className='footer_container_social'>
                        <strong>{window.location.pathname==='/'?'FOLLOW OUR FASHION JOURNEY':'FOLLOW US'}</strong>
                        {window.location.pathname==='/'?
                        <div className='footer_container_social_icons'>
                            {socialNetworking.map((data, i) => {
                                return(
                                    <Link to={data.url} target='_blank' key={i}>
                                        <div style={{backgroundImage: `url(${process.env.REACT_APP_URL_BASE}${data.icon}`}} />
                                    </Link>
                                )
                            })}
                        </div>
                        :
                        <div className='footer_container_social_option'>
                            {socialNetworking.map((data, i) => {
                                return(
                                    <Link to={data.url} target='_blank' key={i}>{data.text}</Link>
                                )
                            })}    
                        </div>
                        }
                    </div>
                    <div className='footer_container_logo2' style={logo2} onClick={()=>this.props.history.push('/')} />
                    <div className='footer_container_copyright'>© 2019 AYNI</div>
                </div>
                }
                <FooterMobile/>
            </div>
        )
    }
}

export default withRouter(Footer)