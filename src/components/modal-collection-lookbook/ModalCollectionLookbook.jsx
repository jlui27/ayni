import React, { Component } from 'react'
import './ModalCollectionLookbook.scss'
import { connect } from 'react-redux';
import { displayLookbookModal } from '../../core/store/actions/header';
import Swiper from 'react-id-swiper';

class ModalCollectionLookbook extends Component{
    constructor(){
        super()
    }
    render(){
        const { displayModal, onDisplayModal, modalData, modalIndex } = this.props
        const display = displayModal ? { display: 'block' } : null
        const swiperParams = {
            slidesPerView: 1,
            spaceBetween: 10,
            initialSlide: modalIndex,
            pagination: {
              el: '.swiper-pagination',
              type: 'bullets',
              clickable: true
            }
        }
        //console.log('All props: ', displayModal, modalData, modalIndex)
        return(
            <div className='modal-collection-preview' style={display}>
                <div className='modal-collection-preview_header'>
                    <button onClick={()=>onDisplayModal(false)}>×</button>
                </div>
                <div className='modal-collection-preview_container'>
                    {modalData ?
                    <Swiper { ...swiperParams } ref={(node) => { if (node) this.swiper = node.swiper; }} >
                        {modalData.length>0?modalData.map((data, i) => {
                            if(data.type === 'LOOKBOOK'){
                                return(
                                    <div className='modal-collection-preview_container_content' key={i}>
                                        <img src={data.path} alt=""/>
                                        <span>{data.name.replace(/_/g, ' ')}</span>
                                </div>
                                )
                            }
                        }) : null}
                    </Swiper>
                    :null}
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return{
        displayModal: state.lookModal.display,
        modalData: state.lookModal.data,
        modalIndex: state.lookModal.init
    }
}

const mapDispatchToProps = dispatch => {
    return{
        onDisplayModal(exec){
            dispatch(displayLookbookModal(exec))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ModalCollectionLookbook)